//
//  Constants.h
//  NineConnections
//
//  Created by Floris van der Grinten on 26-05-14.
//
//
#import <Foundation/Foundation.h>
#ifndef NineConnections_Constants_h
#define NineConnections_Constants_h

typedef NS_ENUM(NSInteger, SocialNetwork) {
    SocialNetworkTwitter,
    SocialNetworkLinkedIn,
    SocialNetworkBuffer
};

typedef NS_ENUM(NSInteger, FeedType) {
    FeedTypeChannel,
    FeedTypeNewsroom,
    FeedTypeTwitterList,
    FeedTypeAito
};

typedef NS_ENUM(NSInteger, AlertBoxType) {
    AlertBoxTypeConnectionErrorTwitter,
    AlertBoxTypePostingMessage
};

#define NO_STATS_MESSAGE =  NSLocalizedString(@"NO_STATS_MESSAGE", "")
#define NO_STATS_SUBMESSAGE NSLocalizedString(@"NO_STATS_SUBMESSAGE", nil)
#define INBOX_ZERO_MOTIVATIONAL_MESSAGE NSLocalizedString(@"INBOX_ZERO_MOTIVATIONAL_MESSAGE", nil)
#define SETTINGS_USE_AVATAR NSLocalizedString(@"SETTINGS_USE_AVATAR", nil)
#define ALERT_NO_ACCOUNT_TITLE NSLocalizedString(@"ALERT_NO_ACCOUNT_TITLE", nil)
#define ALERT_NO_ACCOUNT_MESSAGE NSLocalizedString(@"ALERT_NO_ACCOUNT_MESSAGE", nil)
#define ALERT_NO_ACCOUNT_ACCESS_MESSAGE NSLocalizedString(@"ALERT_NO_ACCOUNT_ACCESS_MESSAGE", nil)
#define INSTANT_SHARE_WARNING NSLocalizedString(@"INSTANT_SHARE_WARNING", nil)
#define NO_FEEDS_FOUND_MESSAGE NSLocalizedString(@"NO_FEEDS_FOUND_MESSAGE", "")
#define MAIN_NAV_BAR_SEEN_TITLE NSLocalizedString(@"MAIN_NAV_BAR_SEEN_TITLE", nil)
#define MAIN_NAV_BAR_SHARED_TITLE NSLocalizedString(@"MAIN_NAV_BAR_SHARED_TITLE", nil)
#define MAIN_NAV_BAR_SMART_SHARED_TITLE NSLocalizedString(@"MAIN_NAV_BAR_SMART_SHARED_TITLE", nil)
#define MAIN_NAV_BAR_BOOKMARKED_TITLE NSLocalizedString(@"MAIN_NAV_BAR_BOOKMARKED_TITLE", nil)

#define SEG_App_Opens @"App opened"
#define SEG_Login_Is_Shown @"Login screen shown"
#define SEG_Article_Shared @"Article shared"
#define SEG_Article_Seen @"Article seen"
#define SEG_Article_Bookmarked @"Article bookmarked"
#define SEG_Social_Stat_Retrieved @"Social stat retrieved"
#define SEG_Share_Method_Key @"Share method"
#define SEG_Share_Method_Drag @"Instant drag-to-share"
#define SEG_Share_Method_Modal @"Conventional modal popup"
#define SEG_Web_Page_Viewed @"Article web page view"
#define SEG_Full_Article_Viewed @"Viewed full article"
#define SEG_Connected_To_Buffer @"Connected to Buffer"
#define SEG_Share_Option_Selected @"Share option selected"

#define TWITTER_ALERT_DUPLICATE_TITLE NSLocalizedString(@"TWITTER_ALERT_DUPLICATE_TITLE", nil)
#define TWITTER_ALERT_DUPLICATE_TEXT NSLocalizedString(@"TWITTER_ALERT_DUPLICATE_TEXT", nil)
#define TWITTER_ALERT_RENEW_TITLE NSLocalizedString(@"TWITTER_ALERT_RENEW_TITLE", nil)
#define TWITTER_ALERT_RENEW_TEXT NSLocalizedString(@"TWITTER_ALERT_RENEW_TEXT", nil)
#define TWITTER_ALERT_MAX_CHARS_TITLE NSLocalizedString(@"TWITTER_ALERT_MAX_CHARS_TITLE", nil)
#define TWITTER_ALERT_MAX_CHARS_TEXT NSLocalizedString(@"TWITTER_ALERT_MAX_CHARS_TEXT", nil)
#define TWITTER_ALERT_UNKNOWN_TITLE NSLocalizedString(@"TWITTER_ALERT_UNKNOWN_TITLE", nil)
#define TWITTER_ALERT_UNKNOWN_TEXT NSLocalizedString(@"TWITTER_ALERT_UNKNOWN_TEXT", nil)
#define TWITTER_ALERT_OVERCAPACITY_TITLE NSLocalizedString(@"TWITTER_ALERT_OVERCAPACITY_TITLE", nil)
#define TWITTER_ALERT_OVERCAPACITY_TEXT NSLocalizedString(@"TWITTER_ALERT_OVERCAPACITY_TEXT", nil)

#define BUFFER_ALERT_INSTRUCTIONS_TITLE NSLocalizedString(@"BUFFER_ALERT_INSTRUCTIONS_TITLE", nil)
#define BUFFER_ALERT_INSTRUCTIONS_TEXT NSLocalizedString(@"BUFFER_ALERT_INSTRUCTIONS_TEXT", nil)
#define BUFFER_ALERT_INSTRUCTIONS_BUTTON_TEXT NSLocalizedString(@"BUFFER_ALERT_INSTRUCTIONS_BUTTON_TEXT", nil)

#define BUFFER_ALERT_FREE_LIMIT_TITLE NSLocalizedString(@"BUFFER_ALERT_FREE_LIMIT_TITLE", nil)
#define BUFFER_ALERT_FREE_LIMIT_TEXT NSLocalizedString(@"BUFFER_ALERT_FREE_LIMIT_TEXT", nil)
#define BUFFER_ALERT_DUPLICATE_POST_TITLE NSLocalizedString(@"BUFFER_ALERT_DUPLICATE_POST_TITLE", nil)
#define BUFFER_ALERT_DUPLICATE_POST_TEXT NSLocalizedString(@"BUFFER_ALERT_DUPLICATE_POST_TEXT", nil)
#define BUFFER_ALERT_UNKNOWN_TITLE NSLocalizedString(@"BUFFER_ALERT_UNKNOWN_TITLE", nil)
#define BUFFER_ALERT_UNKNOWN_TEXT NSLocalizedString(@"BUFFER_ALERT_UNKNOWN_TEXT", nil)

#define LINKEDIN_ALERT_DUPLICATE_TITLE NSLocalizedString(@"LINKEDIN_ALERT_DUPLICATE_TITLE", nil)
#define LINKEDIN_ALERT_DUPLICATE_TEXT NSLocalizedString(@"LINKEDIN_ALERT_DUPLICATE_TEXT", nil)
#define LINKEDIN_ALERT_AUTH_ERROR_TITLE NSLocalizedString(@"LINKEDIN_ALERT_AUTH_ERROR_TITLE", nil)
#define LINKEDIN_ALERT_AUTH_ERROR_TEXT NSLocalizedString(@"LINKEDIN_ALERT_AUTH_ERROR_TEXT", nil)
#define LINKEDIN_ALERT_UNKNOWN_TITLE NSLocalizedString(@"LINKEDIN_ALERT_UNKNOWN_TITLE", nil)
#define LINKEDIN_ALERT_UNKNOWN_TEXT NSLocalizedString(@"LINKEDIN_ALERT_UNKNOWN_TEXT", nil)


#define DEFAULTS_DISPLAY_AVATAR @"Display avatar"
#define DEFAULTS_REMOVE_PAGING_ANIMATION @"Remove paging animation"
#define DEFAULTS_REMOVE_ARTICLE_LINKS @"Hide article sources"
#define DEFAULTS_AVATAR_URL @"avatarURL"
#define DEFAULTS_REMOVE_ANIMATIONS @"Remove other animations"
#define DEFAULTS_DRAG_TO_SHARE_INSTANTLY @"Drag to share instantly"
#define DEFAULTS_TAP_TO_SHARE_INSTANTLY @"Tap to share instantly"

#define LINKEDIN_API_KEY @"77ygm0vi7z4u18"
#define LINKEDIN_API_SECRET @"j0qAOlL0iB8perHc"
#define LINKEDIN_SCOPE_ARRAY @[ @"r_basicprofile", @"w_share" ]
#define LINKEDIN_SCOPE_ARRAY_FIRST_INPUT @"r_basicprofile"
#define LINKEDIN_SCOPE_ARRAY_SECOND_INPUT @"w_share"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#endif
