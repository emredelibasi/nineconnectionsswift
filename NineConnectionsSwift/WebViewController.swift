//
//  WebViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/14/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import NXOAuth2Client
import Analytics

class WebViewController: UIViewController, UIWebViewDelegate {

    var headline: NCHeadline?
    var message = ""
    var channel = ""
    var immediate = false
    var network: SocialNetwork?
    var alternateURL: URL?
    var pushedFromACVC: Bool = false
    var pushedFromMVC = false
    let loggedInUser = NCDataStoreSwift.sharedStore.loggedInUser
    var callback: (([NCSocialAccount], Error?) -> Void)?
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.closeButton.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Medium", size: 18.0)!], for: UIControlState())
    }

    @IBAction func dismissWebView(_ sender: AnyObject) {

        UIApplication.shared.isStatusBarHidden = false
        self.presentingViewController?.dismiss(animated: true, completion: { () -> Void in
            //URLCache.shared.removeAllCachedResponses()
            self.webView = nil
        })
        
    }
    
    @IBAction func refreshWebPage(_ sender: AnyObject) {
        self.webView.reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.pushedFromACVC == true || self.pushedFromMVC == true {
            self.closeButton.title = NSLocalizedString("CLOSE_TEXT", comment: "")
        }
        
        var urlToLoad = self.headline?.url
        if (urlToLoad != nil && self.pushedFromMVC == false && self.headline != nil) {
            SEGAnalytics.shared().track(SEG_Full_Article_Viewed, properties: ["Article" : self.headline!.title, "ArticleID" : self.headline!.id])
        } else {
            urlToLoad = self.alternateURL
            self.webView.delegate = self
        }
        
        self.webView.loadRequest(URLRequest(url: urlToLoad! as URL))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (NXOAuth2AccountStore.sharedStore() as AnyObject).handleRedirectURL(request.url) {
            UIApplication.shared.isStatusBarHidden = false
            self.dismiss(animated: true, completion: { () -> Void in
                URLCache.shared.removeAllCachedResponses()
                self.webView = nil
            })
            return false
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if pushedFromACVC == true || pushedFromMVC == true {
            if let request = webView.request {
                if let resp = URLCache.shared.cachedResponse(for: request) {
                    if let response = resp.response as? HTTPURLResponse {
                        if response.statusCode == 200 {
                            DispatchQueue.main.async {
                                self.dismiss(animated: true, completion: {
                                    NCDataStoreSwift.sharedStore.checkConnectedAccounts(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!, withBlock: { (accounts: [NCSocialAccount], error: Error?) in
                                        if error == nil {
                                            UserDefaults.standard.set(["userId": NCDataStoreSwift.sharedStore.loggedInUser!.id, "accessToken" : NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken, "tokenRefreshDate" : Date().timeIntervalSince1970], forKey: "tokenRefreshUserDetails")
                                        }
                                        self.callback?(accounts, error)
                                    })
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    
        if (error as NSError).code == NSURLErrorCancelled && (pushedFromMVC == true || pushedFromACVC == true) {
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: {
                    if self.network != nil && self.headline != nil {
                        NCDataStoreSwift.sharedStore.prepareForPosting(self.message, toNetwork: self.network!, fromHeadline: self.headline!, inChannel: self.channel, immediately: self.immediate, withSuccess: { (succeded: Bool, error: Error?, response: HTTPURLResponse?) in
                            NCDataStoreSwift.sharedStore.checkConnectedAccounts(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!, withBlock: { (accounts: [NCSocialAccount], error: Error?) in
                                self.callback?(accounts, error)
                            })
                        })
                    } else {
                        NCDataStoreSwift.sharedStore.checkConnectedAccounts(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!, withBlock: { (accounts: [NCSocialAccount], error: Error?) in
                            self.callback?(accounts, error)
                        })
                    }
                })
            }
        }
    }
}
