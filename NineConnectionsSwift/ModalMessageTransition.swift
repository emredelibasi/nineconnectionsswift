//
//  ModalMessageTransition.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/3/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ModalMessageTransition: NSObject,UIViewControllerAnimatedTransitioning, UIDynamicAnimatorDelegate {
    
    var isPresenting: Bool?
    var transitionContext: UIViewControllerContextTransitioning?
    var animator:UIDynamicAnimator?
    var modalView: UIView?
    var dimView: UIView?
    var modalViewStartPositionY: CGFloat?
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    let oldIphone = UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        
        if self.isPresenting == true {
            self.animatePresentation()
        } else  {
            self.animateDismissal()
        }
    }
    
    func animatePresentation() {
        let fromVC: UIViewController = self.transitionContext!.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC: UIViewController = self.transitionContext!.viewController(forKey: UITransitionContextViewControllerKey.to)!
    
        self.transitionContext!.containerView.addSubview(fromVC.view)
        self.transitionContext!.containerView.addSubview(toVC.view)
        
        self.dimView = toVC.view.subviews[0]
        self.modalView = toVC.view.subviews[1]
        
        if (self.transitionContext!.containerView.frame.size.height > 480.0) {
            modalViewStartPositionY = 232;
        }
        else {
            modalViewStartPositionY = 290;
        }
        
        fromVC.view.isUserInteractionEnabled = false
        let dimAnimation = CABasicAnimation(keyPath: "opacity")
        dimAnimation.fromValue = 1.0
        dimAnimation.toValue = 0.3
        dimAnimation.duration = 0.5
        fromVC.view.layer.add(dimAnimation, forKey: "opacity")
        fromVC.view.layer.opacity = 0.3
        
        self.modalView!.center = CGPoint(x: -self.modalView!.frame.size.width, y: modalViewStartPositionY!)
        
        if (UserDefaults.standard.bool(forKey: DEFAULTS_REMOVE_ANIMATIONS)) {
            UIView.animate(withDuration: 0.5, animations: { () -> Void in

                self.modalView?.center = CGPoint(x: self.screenWidth/2, y: self.screenHeight/2 + (self.oldIphone ? 30 : 60))
                //toVC.view.frame = CGRect(x: self.screenWidth + 80, y: self.oldIphone ? 140 : 170, width: 280, height: 277)// toVC.view.frame.offsetBy(dx: 0, dy: 200)
                }, completion: { (finished: Bool) -> Void in
                    self.transitionContext!.completeTransition(true)
            })
        } else {
            self.animator = UIDynamicAnimator.init(referenceView: (self.transitionContext?.containerView)!)
            self.animator?.delegate = self
            self.addDynamicBehaviorForPresenting()
        }
    }
    
    func addDynamicBehaviorForPresenting() {
        let gravity = UIGravityBehavior(items: [self.modalView!])
        gravity.magnitude = 0.3
        self.animator?.addBehavior(gravity)
        
        let pusher = UIPushBehavior(items: [self.modalView!], mode: .instantaneous)
        pusher.pushDirection = CGVector(dx: 69, dy: self.transitionContext!.containerView.frame.size.height > 600.0 ? 5 : -5)
        pusher.active = true
        self.animator?.addBehavior(pusher)
        
        let snap = UISnapBehavior(item: self.modalView!, snapTo: CGPoint(x: screenWidth/2, y: screenHeight/2 + (oldIphone ? 30 : 60)))// (self.transitionContext?.containerView.center)!)
        snap.damping = 0.6
        
        gravity.action = { () -> Void in
            if self.modalView?.center.x > 0 {
                self.animator?.addBehavior(snap)
            }
        }
    }
    
    func animateDismissal() {
        
        let fromVC = self.transitionContext?.viewController(forKey: UITransitionContextViewControllerKey.from) as! MessageModalViewController
        let toVC: UIViewController = self.transitionContext!.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        self.transitionContext!.containerView.addSubview(toVC.view)
        self.transitionContext!.containerView.addSubview(fromVC.view)
        
        //self.dimView = fromVC.view.subviews[0];
        self.modalView = fromVC.view.subviews[1]
        
        //modalView.center = CGPointMake(-modalView.frame.size.width, modalView.center.y+40);
        
        if UserDefaults.standard.bool(forKey: DEFAULTS_REMOVE_ANIMATIONS) {
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                fromVC.view.alpha = 0
                }, completion: { (finished: Bool) -> Void in
            })
            self.undimBackground()
        } else {
            if (fromVC.messageCancelled == false) {
                UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                    self.modalView?.transform = CGAffineTransform(scaleX: 1.0, y: 0.6)
                    }, completion: { (finished: Bool) -> Void in
                        UIView.animate(withDuration: 0.09, delay: 0.3, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                            self.modalView?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            }, completion: { (finished: Bool) -> Void in
                                self.undimBackground()
                        })
                        UIView.animate(withDuration: 0.5, delay: 0.39, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                            fromVC.view.center = CGPoint(x: fromVC.view.center.x, y: -300)
                            }, completion: { (finished: Bool) -> Void in
                        })
                })
            } else {
                self.animator = UIDynamicAnimator(referenceView: self.transitionContext!.containerView)
                self.animator?.delegate = self
                self.addDynamicBehaviorForCanceledDismissal()
            }
        }
    }
    
    func addDynamicBehaviorForCanceledDismissal() {
        
        let gravity = UIGravityBehavior(items: [self.modalView!])
        gravity.magnitude = 1.9
        self.animator?.addBehavior(gravity)
        
        let generalBehavior = UIDynamicItemBehavior(items: [self.modalView!])
        generalBehavior.elasticity = 0.6
        self.animator?.addBehavior(generalBehavior)
        
        let push = UIPushBehavior(items: [self.modalView!], mode: .instantaneous)
        push.pushDirection = CGVector(dx: 1.5, dy: -4)
        self.animator?.addBehavior(push)
        
        let collision = UICollisionBehavior(items: [self.modalView!])
        let boundaryY: CGFloat = (self.transitionContext?.containerView.frame.size.height)!-80
        let boundaryMaxX: CGFloat = (self.transitionContext?.containerView.frame.size.width)!/2
        collision.addBoundary(withIdentifier: "DismissalBoundary" as NSCopying, from: CGPoint(x: 0, y: boundaryY), to: CGPoint(x: boundaryMaxX-115, y: boundaryY))
        
        self.animator?.addBehavior(collision)
        gravity.action = { if ((self.modalView?.frame)!.intersects((self.transitionContext?.containerView.frame)!)) == false {
            self.animator?.removeAllBehaviors()
            }
        }
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        if self.isPresenting == true {
            self.animator?.removeAllBehaviors()
            self.transitionContext?.completeTransition(true)
        } else {
            self.undimBackground()
        }
    }
    
    func undimBackground() {
        let duration = 0.4
        let toVC: UIViewController = self.transitionContext!.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let dimAnimation = CABasicAnimation(keyPath: "opacity")
        dimAnimation.fromValue = toVC.view.layer.opacity
        dimAnimation.toValue = 1.0
        dimAnimation.duration = duration
        toVC.view.layer.add(dimAnimation, forKey: "opacity")
        toVC.view.layer.opacity = 1.0
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            toVC.view.isUserInteractionEnabled = true;
            self.transitionContext?.completeTransition(true)
            UIApplication.shared.keyWindow?.addSubview(toVC.view)
            self.animator = nil
        }
    }
    
    deinit {
        self.animator = nil
    }
}
