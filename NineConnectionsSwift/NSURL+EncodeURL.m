//
//  NSURL+EncodeURL.m
//  NineConnections
//
//  Created by Floris van der Grinten on 21-08-14.
//
//

#import "NSURL+EncodeURL.h"

@implementation NSURL (EncodeURL)

- (NSString *)encodedStringFromURL
{
    NSString *urlString = [NSString stringWithFormat:@"%@", self];
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)urlString, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
    return encodedString;
}

@end
