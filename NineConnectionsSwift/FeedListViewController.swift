//
//  FeedListViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/7/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Analytics
import UIColor_Hex_Swift
import Alamofire

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class FeedListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    typealias SuccessBlock = (Bool, NSError?) -> Void

    var fetchedFeeds = [NCFeed]()
    var noFeedsMessageLabel: UILabel?
    var loginRedirectCounter = 0
    var lastContentOffset:CGFloat = 0.0
    var windowWidth: CGFloat?
    var company: NCCompany?
    var noNetworkIndicator: UILabel?
    var refreshDate: Date!
    var navImageView: UIImageView?
    var navImage: UIImage?
    var rowColor: UIColor?
    let defaultLogo = UIImage(named: "nc_logo.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        windowWidth = self.view.bounds.size.width
        
        self.noNetworkIndicator = UILabel()
        self.noNetworkIndicator?.backgroundColor = UIColor.black
        self.noNetworkIndicator?.textColor = UIColor.white
        self.noNetworkIndicator?.font = UIFont(name: "ProximaNova-Regular", size: 13.0)!
        self.noNetworkIndicator?.text = NSLocalizedString("NO_INTERNET_TEXT", comment: "")
        self.noNetworkIndicator?.textAlignment = .center
        
        processDataFetchingWithRefreshDateCheck()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil, queue: nil) { (NSNotification) -> Void in
            
            self.processDataFetchingWithRefreshDateCheck()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        self.company = NCDataStoreSwift.sharedStore.retrieveUser()?.company
        
        DispatchQueue.main.async {
            
            if self.company?.code != "nine_connections" && self.company?.logo != nil {
                self.configureUIForOtherCompany()
            } else {
                self.configureUIForNCTheme()
            }
                        
            if self.company?.code == "nnbank" {
                UIApplication.shared.setStatusBarStyle(.default, animated: false)
                
            } else {
                UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
            }
            
            self.navigationController?.navigationBar.barTintColor = UIColor.companyMainNavBar()
            self.tableView.backgroundColor = UIColor.companyBackground()
            self.tableView.separatorColor = UIColor.companyRowSeparator()
            self.view.backgroundColor = UIColor.companyBackground()
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.tableView.sectionFooterHeight = 0
            self.tableView.sectionHeaderHeight = 0
            self.tableView.selectRow(at: nil, animated: false, scrollPosition: UITableViewScrollPosition.none)
            self.tableView.reloadData()
        }
    }
    
    // MARK: Data retrieval from servers
    
    func retrieveData(withTokenCheck: Bool) {
        if NCDataStoreSwift.sharedStore.connectedToNetwork() == true {
            if NCDataStoreSwift.sharedStore.loggedInUser?.authorizationToken != nil {
                if withTokenCheck {
                    NCDataStoreSwift.sharedStore.checkTokenValidity(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!) { (returnToken: NCAuthorizationToken?, error: Error?, response: HTTPURLResponse?) in
                        
                        if returnToken != nil && error == nil {
                            
                            self.refreshDate = Date()
                            UserDefaults.standard.set(self.refreshDate, forKey: "refreshDate")
                            UserDefaults.standard.synchronize()
                            
                            self.proceedWithDataFetchingOperationsWithToken(token: returnToken!)
                            
                        } else if returnToken == nil && response == nil {
                            
                            self.displayAlertWithRefreshFeeds(NSLocalizedString("LOADING_FEEDS_ERROR_TITLE", comment: ""), messageInput: NSLocalizedString("LOADING_FEEDS_NO_INTERNET_MESSAGE", comment: ""))
                            self.showNoInternetIndicator(true)
                            
                        } else {
                            
                            self.showNoInternetIndicator(false)
                            self.redirectToLoginPage(withEmail: nil)
                        }
                    }
                } else {
                    self.proceedWithDataFetchingOperationsWithToken(token: NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!)
                }
            } else {
                self.showNoInternetIndicator(false)
                self.redirectToLoginPage(withEmail: nil)
            }
        } else {
            self.displayAlertWithRefreshFeeds(NSLocalizedString("LOADING_FEEDS_ERROR_TITLE", comment: ""), messageInput: NSLocalizedString("LOADING_FEEDS_NO_INTERNET_MESSAGE", comment: ""))
            self.showNoInternetIndicator(true)
        }
    }
    
    
    func proceedWithDataFetchingOperationsWithToken(token: NCAuthorizationToken) {
        
        NCDataStoreSwift.sharedStore.extractUserFromToken(token, withBlock: { (user: NCUser?, error:
            Error?, response: HTTPURLResponse?) in
            print(response.debugDescription)
        })
        
        self.refreshLinkedInToken()
        self.showNoInternetIndicator(false)
        self.refreshFeeds()
        
        let socialAccounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
        let socialStats = NCDataStoreSwift.sharedStore.retrieveSocialStats()
        
        if socialAccounts?.count == 0 {
            NCDataStoreSwift.sharedStore.checkConnectedAccounts(token, withBlock: { (accounts: [NCSocialAccount], error: Error?) in
                if error != nil {
                    print(error!.localizedDescription)
                }
            })
        }
        
        if socialStats.count == 0 {
            NCDataStoreSwift.sharedStore.fetchStatsForUser(NCDataStoreSwift.sharedStore.loggedInUser!, withBlock: { (fetchedStats: [NCSocialStat], error: Error?) in
                if error != nil {
                    print(error!.localizedDescription)
                }
            })
        }
    }
    
    func processDataFetchingWithRefreshDateCheck() {
        
        if let rDate = UserDefaults.standard.object(forKey: "refreshDate") as? Date {
            
            if Date().timeIntervalSince(rDate) > 1728000 {
                retrieveData(withTokenCheck: true)
            } else {
                retrieveData(withTokenCheck: false)
            }
        } else {
            retrieveData(withTokenCheck: true)
        }
    }
    
    
    func refreshFeeds() {
        
        fetchedFeeds = []
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        if NCDataStoreSwift.sharedStore.connectedToNetwork() == true {
            if NCDataStoreSwift.sharedStore.loggedInUser != nil {
                NCDataStoreSwift.sharedStore.fetchAllFeedsWithBlock({ (feeds: [NCFeed], error: Error?) -> Void in
                    if (error == nil) {
//                        self.fetchedFeeds = feeds.sorted(by: { (feed1: NCFeed, feed2: NCFeed) -> Bool in
//                            feed1.name < feed2.name
//                        })
                        self.fetchedFeeds = feeds
                        var feedsToSave  = [NCFeed]()
                        
                        for feed in self.fetchedFeeds {
                            NCDataStoreSwift.sharedStore.fetchNewHeadlinesFromFeed(feed, withBlock: { (headlines: [NCHeadline], error: Error?) -> Void in
                                var fetchedHeadlines = headlines
                                                                
                                let seenHeadlines = NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[feed.id]
                                if seenHeadlines != nil {
                                    for seen in seenHeadlines! {
                                        fetchedHeadlines = fetchedHeadlines.filter({ (headline: NCHeadline) -> Bool in
                                            headline.id != seen.id
                                            
                                        })
                                    }
                                }
                                feed.headlines = fetchedHeadlines
                                feed.unseenAmount = fetchedHeadlines.count
                                
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.tableView.reloadData()
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                })
                                
                                feedsToSave.append(feed)
                                if feedsToSave.count == self.fetchedFeeds.count {
                                    NCDataStoreSwift.sharedStore.allFeeds = feedsToSave
                                    NCDataStoreSwift.sharedStore.saveFeedsLocally()
                                }
                                
                            })
                            
                        }

                        DispatchQueue.main.async(execute: { () -> Void in
                            //UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                            if (self.tableView.delegate == nil) {self.tableView.delegate = self}
                            if (self.tableView.dataSource == nil) {self.tableView.dataSource = self}
                            self.tableView.reloadData()
                            
                            if self.fetchedFeeds.count == 0 {
                                let padding: CGFloat = 40.0
                                self.noFeedsMessageLabel = UILabel(frame: CGRect(x: padding, y: 0, width: self.view.frame.width-padding, height: 150))
                                self.noFeedsMessageLabel?.text = NSLocalizedString("NO_FEEDS_FOUND_MESSAGE", comment: "")
                                self.noFeedsMessageLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 16.0)
                                self.noFeedsMessageLabel?.numberOfLines = 0
                                self.noFeedsMessageLabel?.backgroundColor = UIColor.companyHeader()
                                self.noFeedsMessageLabel?.textColor = UIColor.white
                                self.noFeedsMessageLabel?.center = CGPoint(x: self.tableView.center.x, y: 40)
                                self.view.addSubview(self.noFeedsMessageLabel!)
                            }
                        })
                    } else {
                        self.displayAlertWithRefreshFeeds(NSLocalizedString("LOADING_FEEDS_ERROR_TITLE", comment: ""), messageInput: NSLocalizedString("LOADING_FEEDS_ERROR_MESSAGE", comment: ""))
                    }
                })
            } else {
                self.redirectToLoginPage(withEmail: nil)
            }
        } else {
            self.displayAlertWithRefreshFeeds(NSLocalizedString("LOADING_FEEDS_ERROR_TITLE", comment: ""), messageInput: NSLocalizedString("LOADING_FEEDS_NO_INTERNET_MESSAGE", comment: ""))
        }
    }
    
    // MARK: UI Configurations
    
    func configureUIForNCTheme() {
        
        DispatchQueue.main.async {
            
            if self.navImage != self.defaultLogo {
                
                if let subviews = self.navigationController?.navigationBar.subviews {
                    for subview in subviews {
                        subview.layer.removeAllAnimations()
                    }
                }
                
                self.navImageView?.removeFromSuperview()
                self.navImage = UIImage()
                self.navImage = self.defaultLogo
                self.navImageView = UIImageView(image: self.navImage)
                self.navImageView!.layer.masksToBounds = true;
                self.navImageView!.contentMode = .scaleAspectFit
                
                self.navigationItem.titleView = self.navImageView
                self.navImageView!.frame = CGRect(x: 0, y: 0, width: 26, height: 26);
            }
        }
    }
    
    func configureUIForOtherCompany() {
        
        DispatchQueue.main.async {
            
            if self.navImage != self.company?.logo {

                if let subviews = self.navigationController?.navigationBar.subviews {
                    for subview in subviews {
                        subview.layer.removeAllAnimations()
                    }
                }
        
                self.company = NCDataStoreSwift.sharedStore.loggedInUser!.company
                self.navImageView?.removeFromSuperview()
                self.navImage = UIImage()
                self.navImage = self.company?.logo
                self.navImageView = UIImageView(image: self.navImage)
                self.navImageView!.layer.masksToBounds = true;
                self.navImageView!.contentMode = .scaleAspectFit
                
                self.navigationItem.titleView = self.navImageView
                
                if self.company?.code == "unit4" {
                    self.navImageView!.frame = CGRect(x: 40, y: 0, width: 120, height: 22)
                } else if self.company?.code == "hartstichting" {
                    self.navImageView!.frame = CGRect(x: 40, y: 0, width: 120, height: 34)
                } else {
                    self.navImageView!.frame = CGRect(x: 40, y: 0, width: 120, height: 30)
                }
            }
        }
    }

    // MARK: TableView Configurations
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.fetchedFeeds.count > 0 ? 5 : 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let normalFeedsCount = self.fetchedFeeds.count
        
        if self.fetchedFeeds.count > 0 {
            switch (section) {
            case 0:
                return normalFeedsCount
            case 1:
                return 4
            case 2:
                return 3
                //            case 3:
                //                return 0
                //            case 4:
            //                return 1
            default:
                break
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var sectionTitle = ""
        
        switch (section) {
        case 0:
            sectionTitle = NSLocalizedString("HAPPENING_NOW_TEXT", comment: "")
            break
        case 1:
            sectionTitle = NSLocalizedString("ARTICLES_HEADER_TEXT", comment: "")
            break
        case 2:
            sectionTitle = NSLocalizedString("USER_HEADER_TEXT", comment: "")
//        case 3:
//            sectionTitle = ""
//        case 4:
//            sectionTitle = ""
        default:
            break
        }
        
        return sectionTitle
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedListTableViewCell
        
        cell.backgroundColor = UIColor.companyRow()
        
        rowColor = cell.backgroundColor
        
        if fetchedFeeds.count > 0 {
            
            cell.rowTextLabel.font = UIFont(name: "ProximaNova-Light", size: OLD_IPHONE ? 16.0 : 18.0)
            cell.rowTextLabel.textColor = UIColor.white
            cell.rowTextLabel.textAlignment = .left
            
            let selectedBackground = UIView(frame: cell.frame)
            selectedBackground.backgroundColor = UIColor.companyRowSelected()
            cell.selectedBackgroundView = selectedBackground
            
            if cell.contentView.subviews.count > 0 {
                for view in cell.contentView.subviews {
                if let uaview = view as? NCUnseenAmountView {
                    uaview.removeFromSuperview()
                    }
                }
            }
            
            var thisFeed: NCFeed? = nil
            
            switch (indexPath.section) {
            case 0:
                thisFeed = self.fetchedFeeds[indexPath.row]
                break
            default:
                break
            }
            
            // Cell Trending Image & Feed Name Configuration
            
            if (thisFeed != nil) {
                cell.rowTextLabel.text = thisFeed!.name
            
//                if thisFeed!.trendCount > 0 {
//                    
//                    cell.unseenTrendsImageView.isHidden = false
//                    cell.unseenTrendsImageView.image = #imageLiteral(resourceName: "rising_icon")
//                    cell.unseenTrendsImageView.image = cell.unseenTrendsImageView.image?.withRenderingMode(.alwaysTemplate)
//                    cell.unseenTrendsImageView.tintColor = UIColor.companyUnreadCount()
//                    
//                    let unseenTrendsView = NCUnseenAmountView(withAmount: thisFeed!.trendCount, color: UIColor.companyRowText(), fontSize: oldIphone ? 12.0 : 14.0)
//                    unseenTrendsView.center = CGPoint(x: cell.unseenTrendsImageView.center.x + 26, y: cell.bounds.height/2)
//                    cell.contentView.addSubview(unseenTrendsView)
//                    
//                } else {
//                    cell.unseenTrendsImageView.isHidden = true
//                    
//                }
            } else {
//                cell.unseenTrendsImageView.isHidden = true
                cell.unseenArticlesImageView.isHidden = true
            }
            

            if thisFeed?.unseenAmount > 0 && (indexPath as NSIndexPath).section == 0 {
                //amountView.center = CGPoint(x: cell.rowTextLabel.requiredWidth() + 55, y: cell.bounds.height/2 + 4)

                let unseenArticlesView = NCUnseenAmountView(withAmount: thisFeed!.unseenAmount!, color: UIColor.companyRowText(), fontSize: OLD_IPHONE ? 12.0 : 14.0)
                unseenArticlesView.center = CGPoint(x: cell.unseenArticlesImageView.center.x + 22, y: cell.bounds.height/2)
                cell.contentView.addSubview(unseenArticlesView)
                
                cell.unseenArticlesImageView.image = #imageLiteral(resourceName: "unread_dot")
                cell.unseenArticlesImageView.image = cell.unseenArticlesImageView.image?.withRenderingMode(.alwaysTemplate)
                cell.unseenArticlesImageView.tintColor = UIColor.companyUnreadCount()
                cell.unseenArticlesImageView.isHidden = false
                
                
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {cell.rowTextLabel.text = NSLocalizedString("SEEN_ARTICLES_TEXT", comment: "")}
                if indexPath.row == 1 {cell.rowTextLabel.text = NSLocalizedString("SHARED_ARTICLES_TEXT", comment: "")}
                if indexPath.row == 2 {cell.rowTextLabel.text = NSLocalizedString("SMART_SHARED_ARTICLES_TEXT", comment: "")}
                if indexPath.row == 3 {cell.rowTextLabel.text = NSLocalizedString("BOOKMARKED_ARTICLES_TEXT", comment: "")}
            } else if indexPath.section == 2 {
                if indexPath.row == 0 {cell.rowTextLabel.text = NSLocalizedString("SOCIAL_STATS_TEXT", comment: "")}
                if indexPath.row == 1 {cell.rowTextLabel.text = NSLocalizedString("CONNECTED_ACCOUNTS_TEXT", comment: "")}
                if indexPath.row == 2 {cell.rowTextLabel.text = NSLocalizedString("SETTINGS_TEXT", comment: "")}
            } else {
                cell.unseenArticlesImageView.isHidden = true
            }

            cell.rowTextLabel.sizeToFit()
            cell.rowTextLabel.textColor = UIColor.companyRowText()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
        
        for view in tableView.cellForRow(at: indexPath)!.contentView.subviews {
            if let uaview = view as? NCUnseenAmountView {
                uaview.setHighlighted(highlighted: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        
        for view in tableView.cellForRow(at: indexPath)!.contentView.subviews {
            if let uaview = view as? NCUnseenAmountView {
                uaview.setHighlighted(highlighted: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel(frame: CGRect(x: 32, y: 9, width: 200, height: 32))
        label.font = UIFont(name: "ProximaNova-Regular", size: 14)
        label.textColor = UIColor.white
        label.text = self.tableView(tableView, titleForHeaderInSection: section)
        label.sizeToFit()
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: windowWidth!, height: 32))
        headerView.backgroundColor = UIColor.companyHeader()
        
        headerView.addSubview(label)
        
        if section == 0 || section == 1 || section == 2 {
            
            if self.company?.code == "nnbank" {
                
                let gradient: CAGradientLayer = CAGradientLayer()
                
                gradient.colors = [UIColor("#E34D0D").cgColor, UIColor("#FAB400").cgColor]
                gradient.locations = [0.0 , 1.0]
                gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
                gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
                gradient.frame = CGRect(x: 22.0, y: 0.0, width: headerView.frame.size.width, height: headerView.frame.size.height)
                
                headerView.layer.insertSublayer(gradient, at: 0)
            }
            
        } else if section == 3 {
    
            let contactButton = UIButton()
            contactButton.frame = CGRect(x: 42, y: 18, width: windowWidth! - 52, height: 65)
            contactButton.backgroundColor = UIColor.companyContact()
            contactButton.layer.cornerRadius = 4
            contactButton.setTitle(NSLocalizedString("CONTACT_US_BUTTON_TEXT", comment: ""), for: UIControlState())
            contactButton.setTitleColor(UIColor.companyContactText(), for: UIControlState())
            contactButton.setTitleColor(UIColor.companyUnreadCount(), for: .highlighted)
            contactButton.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: OLD_IPHONE ? 19 : 21)
            contactButton.addTarget(self, action: #selector(FeedListViewController.contactUsPressed(_:)), for: .touchUpInside)
            
            headerView.addSubview(contactButton)
            
            let logoutButton = UIButton()
            logoutButton.frame = CGRect(x: 42, y: 96, width: windowWidth! - 52, height: 65)
            logoutButton.backgroundColor = UIColor.companyLogout()
            logoutButton.layer.cornerRadius = 4
            logoutButton.setTitle(NSLocalizedString("LOG_OUT_BUTTON_TEXT", comment: ""), for: UIControlState())
            logoutButton.setTitleColor(UIColor.companyLogoutText(), for: UIControlState())
            logoutButton.setTitleColor(UIColor.companyUnreadCount(), for: .highlighted)
            logoutButton.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: OLD_IPHONE ? 19 : 21)
            logoutButton.addTarget(self, action: #selector(FeedListViewController.logoutButtonPressed(_:)), for: .touchUpInside)
            
            headerView.addSubview(logoutButton)

        }
    
        return headerView;
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 3 ? 150 : 32
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).section == 0 {
            self.performSegue(withIdentifier: "showHeadlineCVCSegue"/*"ShowMainViewController"*/, sender: tableView.cellForRow(at: indexPath))
            tableView.deselectRow(at: indexPath, animated: true)
        } else if (indexPath as NSIndexPath).section == 1 {
            self.performSegue(withIdentifier: "showHeadlineCVCSegue"/*"ShowMainViewController"*/, sender: tableView.cellForRow(at: indexPath))
            tableView.deselectRow(at: indexPath, animated: true)
        } else if (indexPath as NSIndexPath).section == 2 {
            if (indexPath as NSIndexPath).row == 0 {
                self.performSegue(withIdentifier: "ShowStatsVC", sender: tableView.cellForRow(at: indexPath))
                tableView.deselectRow(at: indexPath, animated: true)
            } else if (indexPath as NSIndexPath).row == 1 {
                self.performSegue(withIdentifier: "ShowAccountsConnectVC", sender: tableView.cellForRow(at: indexPath))
                tableView.deselectRow(at: indexPath, animated: true)
            } else if (indexPath as NSIndexPath).row == 2 {
                self.performSegue(withIdentifier: "ShowSettingsVC", sender: tableView.cellForRow(at: indexPath))
                tableView.deselectRow(at: indexPath, animated: true
                )
            }
        }
    }
        
    // MARK: Helper Functions
    
    func refreshLinkedInToken() {
        if let userData = UserDefaults.standard.dictionary(forKey: "tokenRefreshUserDetails") as [String:Any]! {
            if let userId = userData["userId"] as? Int,
                let userToken = userData["accessToken"] as? String,
                let tokenRefresh = userData["tokenRefreshDate"] as? TimeInterval {
                
                let timeDifference = Date().timeIntervalSince1970 - tokenRefresh
                
                if userId == NCDataStoreSwift.sharedStore.loggedInUser?.id && timeDifference > 2592000 {
                    
                    Alamofire.request("\(NCRESTAPI)/oauth/linkedin?access_token=\(userToken)").responseData(completionHandler: { response in
                        
                        if response.result.isSuccess {
                            UserDefaults.standard.set(["userId": NCDataStoreSwift.sharedStore.loggedInUser!.id, "accessToken" : NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken, "tokenRefreshDate" : Date().timeIntervalSince1970], forKey: "tokenRefreshUserDetails")
                        }
                    })
                }
            }
        } else {
            Alamofire.request("\(NCRESTAPI)/oauth/linkedin?access_token=\(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken)").responseData(completionHandler: { response in
                
                if response.result.isSuccess {
                    UserDefaults.standard.set(["userId": NCDataStoreSwift.sharedStore.loggedInUser!.id, "accessToken" : NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken, "tokenRefreshDate" : Date().timeIntervalSince1970], forKey: "tokenRefreshUserDetails")
                }
            })
        }
    }
    
    func displayAlertWithRefreshFeeds(_ titleInput: String, messageInput: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: titleInput, message: messageInput, preferredStyle: UIAlertControllerStyle.alert)
            let okaction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            let tryAction = UIAlertAction(title: NSLocalizedString("TRY_AGAIN_TEXT", comment: ""), style: UIAlertActionStyle.default, handler: { (tryAgain: UIAlertAction) -> Void in
                self.refreshFeeds()
            })
            alert.addAction(okaction)
            alert.addAction(tryAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNoInternetIndicator(_ show: Bool) {
        
        DispatchQueue.main.async {
            
            if show {
                self.view.addSubview(self.noNetworkIndicator!)
                self.noNetworkIndicator?.frame = CGRect(x: 0, y: 0, width: self.windowWidth!, height: 30)
            } else {
                self.noNetworkIndicator?.removeFromSuperview()
            }
        }
    }
    
    func contactUsPressed(_ sender: UIButton!) {
        
        Smooch.show()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func logoutButtonPressed(_ sender: UIButton!) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGGING_OUT_TITLE", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let confirmAction = UIAlertAction(title: NSLocalizedString("LOGGING_OUT_CONFIRMATION_TEXT", comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) -> Void in
            NCDataStoreSwift.sharedStore.logoutAtNineConnections()
            Smooch.logout()
            self.redirectToLoginPage(withEmail: nil)
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("LOGGING_OUT_CANCELLED_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }


    func redirectToLoginPage(withEmail email: String?) {
        
        let loginScreen = self.storyboard!.instantiateViewController(withIdentifier: "LoginScreen") as! LogInViewController
        
        if email != nil {
            self.present(loginScreen, animated: false, completion: {
                loginScreen.usernameField.text = email!
            })
        } else {
            DispatchQueue.main.async {
                self.present(loginScreen, animated: true) {}
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.companyInnerNavBar()

        if segue.identifier == "ShowMainViewController" {
            let mainVC: MainViewController = segue.destination as! MainViewController
            mainVC.cleanUpViews()
            mainVC.company = self.company
        
            if let feed = sender as? NCFeed {
                mainVC.currentFeed = feed
                mainVC.company = self.company
            
            } else {
            
                if let cell = sender as? UITableViewCell {
                
                    let index = self.tableView.indexPath(for: cell)
                    
                    if (index! as NSIndexPath).section == 0 {

                        mainVC.currentFeed = self.fetchedFeeds[(index! as NSIndexPath).row]
                        mainVC.fetchedHeadlines = self.fetchedFeeds[(index! as NSIndexPath).row].headlines
                        
                    } else if (index! as NSIndexPath).section == 1 {
                        
                        switch ((index! as NSIndexPath).row) {
                        case 0:
                            mainVC.archiveMode = true
                        case 1:
                            mainVC.sharedMode = true
                        case 2:
                            mainVC.smartSharedMode = true
                        case 3:
                            mainVC.bookmarkedMode = true
                        default:
                            break
                        }
                    }
                }
            }
        } else if segue.identifier == "showHeadlineCVCSegue" {
            let headlineCVC: HeadlineCollectionViewController = segue.destination as! HeadlineCollectionViewController
//            mainVC.cleanUpViews()
            headlineCVC.userCompany = self.company

            if let cell = sender as? UITableViewCell, let index = self.tableView.indexPath(for: cell) {
                
                if index.section == 0 {
                    
                    headlineCVC.currentFeed = self.fetchedFeeds[index.row]                    
                    headlineCVC.fetchedHeadlines = self.fetchedFeeds[index.row].headlines
                    
                } else if index.section == 1 {
                    
                    switch (index.row) {
                    case 0:
                        headlineCVC.archiveMode = true
                    case 1:
                        headlineCVC.sharedMode = true
                    case 2:
                        headlineCVC.smartSharedMode = true
                    case 3:
                        headlineCVC.bookmarkedMode = true
                    default:
                        break
                    }
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
