//
//  NCSocialStat.m
//  NineConnections
//
//  Created by Floris van der Grinten on 28-05-14.
//
//

#import "NCSocialStat.h"

@implementation NCSocialStat

- (instancetype)initWithHeadline:(NCHeadline *)headline thatHas:(NSInteger)amount ofInteractionType:(InteractionType)interaction
{
    self = [super init];
    if (self) {
        self.headline = headline;
        self.amount = amount;
        self.interactionType = interaction;
        
        NSString *imageName;
        NSArray *message;
        switch (self.interactionType) {
            case TwitterInteractionTypeFavorite:
                imageName = @"favorite-icon";
                message = @[@"favorite", @"favorites"];
                break;
            case TwitterInteractionTypeReply:
                imageName = @"reply-icon";
                message = @[@"reply" , @"replies"];
                break;
            case TwitterInteractionTypeRetweet:
                imageName = @"retweet-icon";
                message = @[@"retweet", @"retweets"];
                break;
            case LinkedInInteractionTypeComment:
                imageName = @"comment-icon";
                message = @[@"comment", @"comments"];
                break;
            case LinkedInInteractionTypeLike:
                imageName = @"like-icon";
                message = @[@"like", @"likes"];
                break;
                
            default:
                break;
        }
        self.icon = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
        self.displayText = [NSString stringWithFormat:@"%li %@", (long)self.amount, message[MIN(self.amount,2)-1]];
        
    }
    return self;
}

@end
