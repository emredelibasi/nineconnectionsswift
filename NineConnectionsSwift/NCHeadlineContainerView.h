//
//  HeadlineContainerView.h
//  NineConnections
//
//  Created by Floris van der Grinten on 14-05-14.
//
//

#import <UIKit/UIKit.h>
#import "NCHeadline.h"
#import "UIImage+ImageEffects.h"

@interface NCHeadlineContainerView : UIView

@property (strong, nonatomic) NCHeadline *headline;
@property (strong, atomic) UIImage *blurredImage;
@property (strong, nonatomic) UIButton *URLButton;
@property (strong, nonatomic) UIImageView *backgroundImage;
@property (strong, nonatomic) CAGradientLayer *gradient;
@property (strong, nonatomic) UITextView *titleView;
@property (nonatomic, copy) void(^gestureBlock)(void);

- (instancetype)initWithHeadline:(NCHeadline *)headline width:(CGFloat)width;
+ (CGFloat)headlineContainerHeight;

@end
