//
//  HeadlineCollectionViewCell.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 2/10/17.
//  Copyright © 2017 Emre Delibasi. All rights reserved.
//

import UIKit
import Analytics

class HeadlineCollectionViewCell: UICollectionViewCell {
    
    var headline: NCHeadline?
    var gestureBlock: (() -> ())?
    
    @IBOutlet weak var bookmarkedImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var urlButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        //        let tagsListView = TagListView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 70, height: 89))
        //        tagsListView.center = CGPoint(x: self.windowWidth!/2, y: urlBar.frame.maxY + 70)
        //        tagsListView.alignment = .center
        //
        //        if self.headline!.trends.count < 6 {
        //            for trend in self.headline!.trends {
        //                tagsListView.addNLPTag(trend.lowercased(), withColor: UIColor.companyInnerNavBar())
        //            }
        //
        //            if self.headline!.tags.count > 0 {
        //                for i in 0...(self.headline!.tags.count-1) {
        //                    if i==6 {break}
        //                    tagsListView.addKeywordTag(self.headline!.tags[i].lowercased())
        //                }
        //            }
        //
        //        } else {
        //            for i in 0...5 {
        //                tagsListView.addNLPTag(self.headline!.trends[i].lowercased(), withColor: UIColor.companyInnerNavBar())
        //            }
        //
        //        }
        //
        //        tagsListView.layer.masksToBounds = true
        //        self.addSubview(tagsListView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func bookmarkPressed(_ sender: Any) {
        
        self.headline!.isBookmarked = !self.headline!.isBookmarked
        
        if self.headline!.isBookmarked {
            bookmarkedImageView.image = #imageLiteral(resourceName: "bookmarked_icon")
            bookmarkedImageView.image = bookmarkedImageView.image?.withRenderingMode(.alwaysTemplate)
            bookmarkedImageView.tintColor = UIColor.companyInnerNavBar()
            
            let bookmarkProperties: [String:Any] = ["Network" : "LinkedIn", "Article" : self.headline!.title, "ArticleID" : self.headline!.id, "Url" : self.headline!.url!, "Date" : Date()]
            SEGAnalytics.shared().track(SEG_Article_Bookmarked, properties: bookmarkProperties)
            
            NCDataStoreSwift.sharedStore.saveHeadlineBookmarkLocally(self.headline!)
            
        } else {
            
            bookmarkedImageView.image = #imageLiteral(resourceName: "unbookmarked_icon")
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.gestureBlock!()
    }
}
