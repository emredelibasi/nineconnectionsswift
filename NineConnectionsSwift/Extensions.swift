//
//  Extensions.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 3/15/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit


/// MARK: CONSTANTS

let NCRESTAPI = "https://itao-api.nineconnections.com"

let OLD_IPHONE = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"


// MARK: EXTENSIONS

extension Date {
    
    func addHours(_ hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }

}

extension Array where Element:NCHeadline {
    func getUnread() -> [NCHeadline] {
        var headlines = [NCHeadline]()
        for headline in self {
            if !headline.isSeen {
                headlines.append(headline)
            }
        }
        return headlines
    }
}


public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone9,2":                               return "iPhone 7 Plus"
        case "iPhone9,3":                               return "iPhone 7"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

public extension UILabel {
    
    public func requiredWidth() -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude ,height: self.frame.height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        label.sizeToFit()
        
        return label.frame.width
    }
}

public extension UIColor {
        
    public class func companyMainNavBar() -> UIColor {
                
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.mainNavBarColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.mainNavBarColor! : UIColor("#2E3A4A")
    }
    
    public class func companyInnerNavBar() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.innerNavBarColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.innerNavBarColor! : UIColor("#4A90E2")
    }
    
    public class func companyHeader() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.headerColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.headerColor! : UIColor("#323E4E")
    }
    
    public class func companyRow() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.rowColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.rowColor! : UIColor("#3F4D61")
    }
    
    public class func companyRowText() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.rowTextColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.rowTextColor! : UIColor.white
    }
    
    public class func companyRowSeparator() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.rowSeparatorColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.rowSeparatorColor! : UIColor.backgroundGrey()
    }
    
    public class func companyRowSelected() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.rowSelectedColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.rowSelectedColor! : UIColor("#4A90E2")
    }
    
    public class func companyRowSelectedText() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.rowSelectedTextColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.rowSelectedTextColor! : UIColor.white
    }
    
    public class func companyUnreadCount() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.unreadCountColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.unreadCountColor! : UIColor("#4A90E2")
    }
    
    public class func companyUnreadCountSelected() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.unreadCountSelectedColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.unreadCountSelectedColor! : UIColor.white
    }
    
    public class func companyContact() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.contactColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.contactColor! : UIColor("#B9C1C7")
    }
    
    public class func companyContactText() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.contactTextColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.contactTextColor! : UIColor("#323E4E")
    }
    
    public class func companyLogout() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.logoutColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.logoutColor! : UIColor("#323E4E")
    }
    
    public class func companyLogoutText() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.logoutTextColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.logoutTextColor! : UIColor("#B9C1C7")
    }
    
    public class func companyBackground() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.backgroundColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.backgroundColor! : UIColor("#323E4E")
    }
    
    public class func companySecondaryBackground() -> UIColor {
        
        return NCDataStoreSwift.sharedStore.loggedInUser?.company?.secondaryBackgroundColor != nil ? NCDataStoreSwift.sharedStore.loggedInUser!.company!.secondaryBackgroundColor! : UIColor("#323E4E")
    }
    
}

public extension UIView {
    
    func addDashedBorder(view: UIView,color: UIColor) {
        
        let color = color.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = view.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        view.layer.addSublayer(shapeLayer)
        
    }
}


public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:(Void)->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}
