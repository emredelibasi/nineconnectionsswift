//
//  NCBufferIntegration.m
//  NineConnections
//
//  Created by Floris van der Grinten on 01-09-14.
//
//

#import "NCBufferIntegration.h"
#import "NCHeadline.h" 

@interface NCBufferIntegration ()

//@property (nonatomic, strong) NSMutableDictionary *profiles;

@end

@implementation NCBufferIntegration

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.loggedInAccount = [[NXOAuth2AccountStore sharedStore] accountsWithAccountType:@"Buffer"].lastObject;
    }
    return self;
}

- (void)fetchUserWithBlock:(FetchReturnBlock)block
{
    NSString *urlString = [NSString stringWithFormat:@"https://api.bufferapp.com/1/profiles.json"];
    NSURL *sessionURL = [NSURL URLWithString:urlString];
    
    dispatch_async(dispatch_get_main_queue(), ^{ [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES]; });
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:sessionURL];
    request.HTTPMethod = @"GET";
    [request addValue:[NSString stringWithFormat:@"Bearer %@", self.loggedInAccount.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];

    
    NSURLSessionDataTask *userTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{ [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO]; });
    
        NSArray *userJSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if (!error && userJSONData && [userJSONData isKindOfClass:[NSArray class]]) {
            
            //NSString *displayName;
       //     NSMutableDictionary *fetchedProfiles = [NSMutableDictionary new];
           
            NSMutableDictionary *linkedInAccounts = [NSMutableDictionary new];
            NSMutableDictionary *twitterAccounts = [NSMutableDictionary new];
            
            for (NSDictionary *account in [userJSONData reverseObjectEnumerator]) {
                NSString *service = account[@"formatted_service"];
                NSString *accountID = account[@"_id"];
                NSString *accountName = account[@"formatted_username"];
                if (accountID.length && accountName.length) {
                    if ([service isEqualToString:@"Twitter"]) [twitterAccounts setObject:accountID forKey:accountName];
                    else if ([service isEqualToString:@"LinkedIn"]) [linkedInAccounts setObject:accountID forKey:accountName];
                }
            }
            NSArray *allProfiles = @[ linkedInAccounts, twitterAccounts ];
            block(allProfiles, error);
            
//                if ([account[@"default"] boolValue]) {
//                    NSString *service = account[@"formatted_service"];
//                    if ([service isEqualToString:@"LinkedIn"] || [service isEqualToString:@"Twitter"]) {
//                        if (displayName) displayName = [NSString stringWithFormat:@"%@ & ", displayName];
//                        [fetchedProfiles setObject:account[@"_id"] forKey:service];
//                        displayName = [NSString stringWithFormat:@"%@%@", displayName ? displayName : @"", [account[@"formatted_username"] stringByReplacingOccurrencesOfString:@"@" withString:@""]];
//                    }
//                }
            
            //[[NSUserDefaults standardUserDefaults] setObject:fetchedProfiles forKey:@"BufferAccountIDs"];
        //    block(@[displayName], error);
        }
        else {
            block(@[ @"" ], error);
        }
      //  NSString *userFullName = [NSString stringWithFormat:@"%@ %@", userJSONData[@"firstName"], userJSONData[@"lastName"]];
       // block(@[userFullName], error);
        
    }];
    
    [userTask resume];
    
    
    //https://api.bufferapp.com/1/profiles.json
}

- (void)postMessage:(NSString *)msg andURL:(NSString *)url toNetwork:(SocialNetwork)network withBlock:(PostReturnBlock)block
{
    NSString *urlString = [NSString stringWithFormat:@"https://api.bufferapp.com/1/updates/create.json"];
    NSURL *sessionURL = [NSURL URLWithString:urlString];
    
    dispatch_async(dispatch_get_main_queue(), ^{ [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES]; });
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:sessionURL];
    request.HTTPMethod = @"POST";

    NSString *optionalURLForLinkedIn;
    NSString *profileIDKey;
    switch (network) {
        case SocialNetworkLinkedIn: {
            profileIDKey = @"LinkedIn";
            optionalURLForLinkedIn = [NSString stringWithFormat:@"&media[link]=%@", [[NSURL URLWithString:url] encodedStringFromURL]];
        } break;
        case SocialNetworkTwitter: {
            profileIDKey = @"Twitter";
            optionalURLForLinkedIn = @"";
        } break;
        default: break;
    }
    
    NSString *profileID = self.profiles[profileIDKey];
    NSString *paramsString = [NSString stringWithFormat:@"text=%@&profile_ids[]=%@%@&shorten=false&attachment=false", msg, profileID, optionalURLForLinkedIn];

    [request addValue:[NSString stringWithFormat:@"Bearer %@", self.loggedInAccount.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];
    //request.HTTPBody = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    request.HTTPBody = [paramsString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionDataTask *postTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{ [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO]; });
        NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if (!error && JSONData) {
            if (((NSHTTPURLResponse *)response).statusCode == 200) {
                NSLog(@"json %@", JSONData);
                NSDictionary *update = [JSONData[@"updates"] firstObject];
                NSString *bufferId = update[@"id"];
                block(bufferId, error);
            }
            else {
                NSString *alertTitle, *alertText, *bufferStatusCode;
                bufferStatusCode = JSONData[@"code"];
                switch ([bufferStatusCode integerValue]) {
                    case 1025:
                        alertTitle = BUFFER_ALERT_DUPLICATE_POST_TITLE;
                        alertText = BUFFER_ALERT_DUPLICATE_POST_TEXT;
                        break;
                    case 1023:
                        alertTitle = BUFFER_ALERT_FREE_LIMIT_TITLE;
                        alertText = BUFFER_ALERT_FREE_LIMIT_TEXT;
                        break;
                    default:
                        alertTitle = BUFFER_ALERT_UNKNOWN_TITLE;
                        NSString *message = JSONData[@"message"];
                        alertText = message ? message : BUFFER_ALERT_UNKNOWN_TEXT;
                        break;
                }
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [alert show];
                });
                [[SEGAnalytics sharedAnalytics] track:@"Buffer status code" properties:@{ @"Account" : self.profiles.description ? self.profiles.description : @"NoAccount", @"Status code" : bufferStatusCode }];

            }
        }
    }];
    [postTask resume];
    NSLog(@"%@", msg);
}

- (void)fetchActivityUpdates:(NSArray *)posts withBlock:(FetchReturnBlock)block
{
    NSLog(@"buffer: need to check: %@", posts);
    for (NCHeadline *h in posts) {
        NSMutableArray *potentialBufferedItems = [NSMutableArray new];
        if (h.bufferLinkedInId.length) [potentialBufferedItems addObject:h.bufferLinkedInId];
        if (h.bufferTwitterId.length) [potentialBufferedItems addObject:h.bufferTwitterId];
        
        for (NSString *bufferId in potentialBufferedItems) {
            NSString *urlString = [NSString stringWithFormat:@"https://api.bufferapp.com/1/updates/%@.json", bufferId];
            NSURL *sessionURL = [NSURL URLWithString:urlString];
        
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:sessionURL];
            request.HTTPMethod = @"GET";
            [request addValue:[NSString stringWithFormat:@"Bearer %@", self.loggedInAccount.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];

            NSURLSessionDataTask *postTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
               // NSLog(@"buff API CALL, %@, %@", error, response);
                if (!error) {
                    NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    if (JSONData) {
                        NSArray *jsonArray = @[JSONData, h];
                        block(jsonArray, error);
                    }
                }
            }];
            [postTask resume];
        }
    }

}

- (NSDictionary *)profiles
{
    _profiles = [[NSUserDefaults standardUserDefaults] objectForKey:@"BufferAccountIDs"];
    NSLog(@"BUFFER: %@", _profiles);
    return _profiles;
}

- (void)setProfiles:(NSDictionary *)profiles
{
    _profiles = profiles;
    [[NSUserDefaults standardUserDefaults] setObject:_profiles forKey:@"BufferAccountIDs"];
}

@synthesize profiles = _profiles;

//- (NSURLRequest *)signedRequestWithURL:(NSURL *)url method:(NSString *)method
//{
//    NXOAuth2Request *request = [[NXOAuth2Request alloc] initWithResource:url method:method parameters:nil];
//    request.account = self.loggedInAccount;
//
//    return [request signedURLRequest];
//}

@end
