//
//  NCUnseenAmountView.m
//  NineConnections
//
//  Created by Floris van der Grinten on 08-09-14.
//
//

#import "NCUnseenAmountView.h"

@implementation NCUnseenAmountView


- (instancetype)initWithAmount:(NSInteger)amount color:(UIColor*)color fontSize:(CGFloat)size
{
    self = [super initWithFrame:CGRectMake(0, 0, 38, 20)];
    if (!self) return nil;
    
    self.badgeColor = color;
    self.backgroundColor = [UIColor clearColor];
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;

    self.amountLabel = [UILabel new];
    self.amountLabel.text = amount ? [NSString stringWithFormat:@"%zd", amount] : @" ";
    self.amountLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:size];
    [self.amountLabel sizeToFit];
    self.amountLabel.textColor = self.badgeColor;
    self.amountLabel.textAlignment = NSTextAlignmentLeft;
    //self.amountLabel.center = self.center;
    
    [self addSubview:self.amountLabel];
    
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{
    _highlighted = highlighted;
    if (_highlighted) {
        self.backgroundColor = [UIColor clearColor];
        self.amountLabel.textColor = [UIColor whiteColor];
    }
    else {
        self.backgroundColor = [UIColor clearColor];
        self.amountLabel.textColor = self.badgeColor;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
