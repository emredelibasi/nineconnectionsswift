//
//  NCFeed.h
//  NineConnections
//
//  Created by Floris van der Grinten on 30-06-14.
//
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface NCFeed : NSObject <NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) FeedType feedType;
@property (nonatomic, strong) NSMutableArray *headlines;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, assign) NSInteger feedId;
@property (nonatomic, assign) NSInteger unseenAmount;
//@property (nonatomic, assign) NSInteger ncHash;

- (instancetype)initWithTitle:(NSString *)title ofType:(FeedType)feedType withFeedId:(NSInteger)feedId userId:(NSInteger)userId url:(NSURL *)url;
//- (NSUInteger)ncHash;
@end
