//
//  HeadlineStore.m
//  NineConnections
//
//  Created by Floris van der Grinten on 20-05-14.
//
//

#import "NCDataStore.h"
#import <Analytics/Analytics.h>
#import "NCFeed.h"


@interface NCDataStore ()

@property (nonatomic) NSMutableArray *headlinesPrivate;
@property (atomic, readwrite) NSMutableArray *allFeeds;
@property (nonatomic, readwrite) NSMutableOrderedSet *postedHeadlines;
@property (nonatomic, readwrite) NSMutableOrderedSet *sequentialSeenHeadlines;
//@property (nonatomic, readwrite) NSMutableDictionary *seenHeadlinesByFeed;
@property (nonatomic) NSData *jsonData;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic) NSMutableArray *allStats;
@property (nonatomic, strong, readwrite) ACAccount *loggedInTwitterAccount;
@property (nonatomic, strong, readwrite) NSString *loggedInLinkedInAccount;
@property (nonatomic, strong, readwrite) NSString *loggedInBufferAccount;
@property (nonatomic, strong, readwrite) NSString *nineConnectionsUsername;
@property (nonatomic, strong, readwrite) LIALinkedInHttpClient *linkedInClient;
@property (nonatomic, strong, readwrite) NCBufferIntegration *bufferClient;
@property (nonatomic, strong) UIViewController *linkedInPresenter;
@property (nonatomic, strong) NSDateFormatter *itemDateFormatter;

@end

@implementation NCDataStore

+ (instancetype)sharedStore{
    static NCDataStore *dataStore = nil;
    if (!dataStore) {
        dataStore = [[self alloc] initPrivate];
    }
    return dataStore;
}

- (instancetype)init{
    @throw [NSException exceptionWithName:@"Singleton" reason:nil userInfo:nil];
    return nil;
}

- (instancetype)initPrivate{
    self = [super init];
    if (self){
        self.headlinesPrivate = [NSMutableArray new];
        self.allFeeds = [NSMutableArray new];
        self.postedHeadlines = [NSMutableOrderedSet new];
        self.allStats = [NSMutableArray new];
        self.sequentialSeenHeadlines = [NSMutableOrderedSet new];
        self.seenHeadlinesByFeed = [NSMutableDictionary new];
        self.itemDateFormatter = [NSDateFormatter new];
        self.itemDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS Z";
    }
    return self;
}


#pragma mark - API

- (void)fetchAllFeedsWithBlock:(FetchReturnBlock)block
{
    if (self.userId) {
        [self.allFeeds removeAllObjects];
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.HTTPShouldSetCookies = YES;
        sessionConfig.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
        
        NSString *userChannelInfoURLString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/group/all?userId=%ld&includeNewsrooms=true&%i", (long)self.userId, abs(arc4random())];
        
        NSURL *userChannelInfoUrl = [NSURL URLWithString:userChannelInfoURLString];
        NSURL *aitoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://staging.nineconnections.com:8081/9x-aito/user?%@", self.nineConnectionsUsername]];
        
        NSArray *urls = @[ userChannelInfoUrl, aitoUrl ];
        
        for (NSURL *url in urls) {
            NSURLSessionDataTask *userChannelInfoTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if (httpResponse.statusCode == 200) {
                    
                    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    NSArray *followingChannels = jsonData[@"groupInfos"];
                    
                    for (NSDictionary *c in followingChannels) {
                        NSString *type = c[@"gridType"];
                        FeedType feedType;
                        if ([type isEqualToString:@"CHANNEL"]) {
                            feedType = FeedTypeChannel;
                        }
                        else if ([type isEqualToString:@"NORMAL"]) {
                            feedType = FeedTypeNewsroom;
                        }
                        else if ([type isEqualToString:@"AITO"]) {
                            feedType = FeedTypeAito;
                        }
                        else {
                            feedType = FeedTypeTwitterList;
                        }
                        
                        NSString *urlString = c[@"url"];
                        NCFeed *feed = [[NCFeed alloc] initWithTitle:c[@"name"] ofType:feedType withFeedId:[c[@"id"] integerValue] userId:self.userId url:urlString ? [NSURL URLWithString:urlString] : nil];
                        NSLog(@"%@", feed.title);
                        [self.allFeeds addObject:feed];

                    }
                    if (url == userChannelInfoUrl) [self saveFeedsLocally];
                    block(self.allFeeds, error);
                }
                
            }];
            
            [userChannelInfoTask resume];
        }
        
    }
    else
    {
        NSLog(@"NO USERID FROM NC SERVER.....");
    }
}

- (void)fetchNewHeadlinesFromFeed:(NCFeed *)feed withBlock:(FetchReturnBlock)block
{
//    self.jsonData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mockfeed" ofType:@"json"]];
//    NSError *error;
//    if (self.jsonData) {
//        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:self.jsonData options:kNilOptions error:&error];
//
//        NSArray *headlines = [jsonData valueForKey:@"headlines"];
//        for (NSDictionary *parsedHeadline in headlines) {
//            NCHeadline *headline = [[NCHeadline alloc] initWithTitle:parsedHeadline[@"title"] articleText:parsedHeadline[@"article-text"]];
//            headline.articleURL = [NSURL URLWithString:parsedHeadline[@"url"]];
//            [self.headlinesPrivate addObject:headline];
//        }
//    }
//    block(self.headlinesPrivate, error);

    
    
    
    
//    self.jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&q=http%3A%2F%2Fwww.nineconnections.com%2Fapi%2Frss%2Fchannel%2F7.rss%3Fmax%3D10%26period%3D0%26reader%3Dstandard%26ln%3DEN%26ln%3DNL%26content_type%3DARTICLE"]];
//    NSError *error;
//    if (self.jsonData) {
//        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:self.jsonData options:kNilOptions error:&error];
//        NSDictionary *responseData = jsonData[@"responseData"];
//        NSDictionary *feed = responseData[@"feed"];
//        NSLog(@"%@", jsonData);
//        NSArray *headlines = feed[@"entries"];
//        
//        for (NSDictionary *parsedHeadline in headlines) {
//            NCHeadline *headline = [[NCHeadline alloc] initWithTitle:parsedHeadline[@"title"] articleText:parsedHeadline[@"content"]];
//            headline.articleURL = [NSURL URLWithString:parsedHeadline[@"link"]];
//            [self.headlinesPrivate addObject:headline];
//        }
//    }
//    block(self.headlinesPrivate, error);
    
    
    
    

    
    
//    self.jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.nineconnections.com/api/rss/channel/7.rss?max=2&period=0&reader=standard&ln=EN&ln=NL&content_type=ARTICLE"]];
//    NSError *error;
//    if (self.jsonData) {
//        RXMLElement *rootXML = [RXMLElement elementFromXMLData:self.jsonData];
//        
//        [rootXML iterate:@"entry" usingBlock:^(RXMLElement *entry) {
//            
//            NSString *titleWithThreeSeperators = [self stringFromHTMLString:[entry child:@"title"].text];
//            NSString *titleWithTwoSeperators = [[titleWithThreeSeperators componentsSeparatedByString:@"|"] firstObject];
//            
//            NSString *titleWithOneSeperator = [titleWithTwoSeperators stringByReplacingOccurrencesOfString:@" – " withString:@" - "];
//            
//            NSString *longestString;
//            NSInteger longestStringLength = 0;
//            for (NSString *str in [titleWithOneSeperator componentsSeparatedByString:@" - "]) {
//                if (longestString == nil || [str length] > longestStringLength) {
//                    longestString = str;
//                    longestStringLength = [longestString    length];
//                }
//            }
//            
//            NSString *title = longestString;
//            NSString *content = [entry child:@"content"].text;
//            NSString *url = [entry child:@"id"].text;
//            
//            NCHeadline *headline = [[NCHeadline alloc] initWithTitle:title articleText:[self stringFromHTMLString:content]];
//            headline.articleURL = [NSURL URLWithString:url];
//            
//           // NSString *imageURL = [[content componentsSeparatedByString:@"&quot"] objectAtIndex:1];
//            NSArray *seperatedContent = [content componentsSeparatedByString:@"\""];
//            if (seperatedContent.count > 1) {
//                NSString *potentialLink = seperatedContent[1];
//                if ([[potentialLink substringToIndex:4] isEqualToString:@"http"]) {
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                        //NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:potentialLink]];
//                        //headline.articleImage = [UIImage imageWithData:imageData];
//                    });
//                }
//
//            }
//            //NSLog(@"imageURL = %@", [[content componentsSeparatedByString:@"\""] objectAtIndex:1]);
//            [self.headlinesPrivate addObject:headline];
//        }];
//    }
//    block(self.headlinesPrivate, error);
    
    
    
    if (feed.URL) {
        
        NSURL *url = feed.feedType == FeedTypeAito ? [NSURL URLWithString:[NSString stringWithFormat:@"%@&user=%@", feed.URL.description, self.nineConnectionsUsername]] : feed.URL;

        NSURLSessionDataTask *headlinesTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {

                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [self.headlinesPrivate removeAllObjects];
                
                if (feed.feedType == FeedTypeChannel) {
                    NSArray *entries = jsonData[@"items"];
                    for (NSDictionary *item in entries) {
                        NSString *title = item[@"title"];
                        NSURL *articleURL = [NSURL URLWithString:item[@"url"]];
                        NSString *articleText = item[@"description"];
                        
                        NCHeadline *headline = [[NCHeadline alloc] initWithTitle:title
                                                                     articleText:[articleText hasSuffix:@"..."] ? nil : articleText
                                                                            feed:feed];
                        
                        headline.articleURL = articleURL;
                        headline.date = [self.itemDateFormatter dateFromString:item[@"published"]];
                        headline.apiItemId = item[@"id"];
                        [self.headlinesPrivate addObject:headline];
                        if (self.headlinesPrivate.count == 15) break;
                    }
                }
                else {

                    NSArray *entries = jsonData[@"suggestions"];
                    
                   // NSLog(@"%@ ----", jsonData);
                    
                    for (NSDictionary *entry in entries) {
                        NSDictionary *item = entry[@"item"];
                        NSString *title = item[@"title"];
                        NSURL *articleURL = [NSURL URLWithString:item[@"url"]];
                        //NSString *articleText = [item[@"mediaInfo"] objectForKey:@"content"];

                        NCHeadline *headline = [[NCHeadline alloc] initWithTitle:title articleText:nil feed:feed];
                        headline.articleURL = articleURL;
                        headline.date = [NSDate dateWithTimeIntervalSince1970:[item[@"createdDate"] integerValue]/1000];
                        headline.apiItemId = [NSString stringWithFormat:@"%@", item[@"itemId"]];
                        [self.headlinesPrivate addObject:headline];
                    }
                }
                block(self.headlinesPrivate, error);
            }
        }];
        
        
        [headlinesTask resume];
        
    
        
        
//        NSString *urlString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/channel/30/feed/shared"];
//        
//        NSURL *sessionURL = [NSURL URLWithString:urlString];
//        
//        NSURLSessionDataTask *headlinesTask = [[NSURLSession sharedSession] dataTaskWithURL:sessionURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            NSArray *entries = jsonData[@"suggestions"];
//            
//            
//    //            for (NSDictionary *entry in entries) {
//    //                NSDictionary *item = entry[@"item"];
//    //                NSString *title = item[@"title"];
//    //                NSLog(@"ITEMMM: %@", title);
//    //               
//    //                
//    //                NCHeadline *headline = [[NCHeadline alloc] initWithTitle:title articleText:@"HEY you"];
//    //                [self.headlinesPrivate addObject:headline];
//    //            }
//    //            block(self.headlinesPrivate, error);
//
//            
//            
//            NSLog(@"%@ ---- %@", response, jsonData[@"type"]);
//        }];
//        
//        
//        [headlinesTask resume];
    
    }
    else
    {
        NSLog(@"NO FEED URL.....");
        NSError *error = [NSError errorWithDomain:@"No feed URL" code:403 userInfo:nil];
        block(self.headlinesPrivate, error);
    }
   
    
}

- (void)fetchFullArticleTextFromHeadline:(NCHeadline *)headline inFeed:(NCFeed *)feed withBlock:(FetchReturnBlock)block
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.HTTPShouldSetCookies = YES;
    sessionConfig.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    NSString *itemIdURLString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/item/%@", headline.apiItemId];
    NSString *markReadUrlString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/behaviour/read/%zd?channelId=%zd", headline.apiItemId.integerValue, feed.feedId];
    
    NSURL *itemIdURL = [NSURL URLWithString:itemIdURLString];
    NSURL *markReadURL = [NSURL URLWithString:markReadUrlString];

    NSURLSessionDataTask *itemIdDataTask = [[NSURLSession sharedSession] dataTaskWithURL:itemIdURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
            NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //NSLog(@"val: %@", string);

            NSData *adata = [string dataUsingEncoding:NSUTF8StringEncoding];
           // NSLog(@"val: %@", [[NSString alloc] initWithData:adata encoding:NSUTF8StringEncoding]);
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:adata options:0 error:nil];
           // NSLog(@"json seri: %@", jsonData);
            NSDictionary *mediaInfo = jsonData[@"mediaInfo"];
            NSString *content = mediaInfo[@"content"];
            headline.articleText = content;
            NSLog(@"%@", headline.articleText);
        }
        block(@[@(httpResponse.statusCode)],error);
        
    }];
    
    [itemIdDataTask resume];
    
    NSURLSessionDataTask *markReadDataTask = [[NSURLSession sharedSession] dataTaskWithURL:markReadURL completionHandler:nil];
    [markReadDataTask resume];
}

- (void)fetchNewStatsWithBlock:(FetchReturnBlock)block;
{
    //NSOrderedSet *recentHeadlines = [self.postedHeadlines subarrayWithRange:NSMakeRange(0, MIN(self.postedHeadlines.count, 15))];
    NSMutableArray *recentHeadlines = [NSMutableArray new];
    for (NCHeadline *h in [[self.postedHeadlines copy] reversedOrderedSet]) {
        [recentHeadlines addObject:h];
        if (recentHeadlines.count == 15) break;
    }
    
    [[NCTwitterIntegration sharedClient] fetchActivityUpdates:recentHeadlines withBlock:^(NSArray *objects, NSError *error) {
        if (!error && objects.count > 1) {
            NSString *twitterName = self.loggedInTwitterAccount.accountDescription ?: @"NoAccount";
            
            id response = objects.firstObject;
            
            if ([response isKindOfClass:[NSArray class]]) {
                NSCountedSet *mentionedIds = [NSCountedSet new];
                for (NSDictionary *mention in response) {
                    id possibleMentionId = mention[@"in_reply_to_status_id_str"];
                    if ([possibleMentionId isKindOfClass:[NSString class]]) [mentionedIds addObject:possibleMentionId];
                }
                for (NCHeadline *h in recentHeadlines) {
                    NSInteger repliesAmount = [mentionedIds countForObject:h.twitterId];
                    if (repliesAmount) {
                        NCSocialStat *replyStat = [[NCSocialStat alloc] initWithHeadline:h thatHas:repliesAmount ofInteractionType:TwitterInteractionTypeReply];
                        [self.allStats addObject:replyStat];
                        block(@[replyStat], error);

                        [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter reply", @"Amount" : @(repliesAmount), @"MessageID" : h.twitterId, @"Message" : h.title, @"Through Buffer" : @NO, @"Network": @"Twitter", @"Url": h.articleURL, @"Username": twitterName }];
                    }
                }
            }
            else if ([objects[1] isKindOfClass:[NCHeadline class]]) {
                NCHeadline *headline = objects[1];
           //     for (NSDictionary *tweet in objects) {}
                NSInteger retweetsAmount = [response[@"retweet_count"] integerValue];
                NSInteger favoritesAmount = [response[@"favorite_count"] integerValue];
                //NSInteger repliesAmount = [tweet[@"repliers_count"] integerValue];
                NSLog(@"tweet %@", response[@"text"]);
                NSArray *twitterError = response[@"errors"];
                if (twitterError) {
                    NSLog(@"%@", twitterError);
                   // NSMutableDictionary *details = [twitterError.firstObject mutableCopy];
                    [[SEGAnalytics sharedAnalytics] track:@"Twitter stat error" properties:@{ @"Tweet" : headline.title, @"Error message" : twitterError.description }];
                }
                NSString *segNetworkId = headline.twitterId;
                
                if (retweetsAmount) {
                    NCSocialStat *retweetStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:retweetsAmount ofInteractionType:TwitterInteractionTypeRetweet];
                    [self.allStats addObject:retweetStat];
                    block(@[retweetStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter retweet", @"Amount" : @(retweetsAmount), @"MessageID" : segNetworkId, @"Message" : headline.title, @"Through Buffer" : @NO, @"Network": @"Twitter", @"Url": headline.articleURL, @"Username": twitterName }];

                  //  NSLog(@"%@ has %li rts", retweetStat.headline.title, retweetStat.amount);
                }
                if (favoritesAmount) {
                    NCSocialStat *favoriteStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:favoritesAmount ofInteractionType:TwitterInteractionTypeFavorite];
                    [self.allStats addObject:favoriteStat];
                    block(@[favoriteStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter favorite", @"Amount" : @(favoritesAmount), @"MessageID" : segNetworkId, @"Message" : headline.title, @"Through Buffer" : @NO, @"Network": @"Twitter", @"Url": headline.articleURL, @"Username": twitterName }];

                }
            }
            
        }
        else {
            NSLog(@"Error: %@", error);
            block(@[], error);
        }
    }];
    
//    [self.linkedInClient fetchActivityUpdates:recentHeadlines withBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            NSString *linkedInName = self.loggedInLinkedInAccount ?: @"NoAccount";
//
//            NSDictionary *post = objects.firstObject;
//            NSInteger amount = [post[@"_total"] integerValue];
//            if (amount) {
//                NCHeadline *headline = objects[1];
//                //NSLog(@"I've got %zd %@ for %@", amount, objects[2],((NCHeadline *)objects[1]).title);
//                NSString *segNetworkId = headline.linkedInId;
//                
//                InteractionType type;
//                NSString *segInteractionType;
//                if ([objects[2] isEqualToString:@"likes"]) {
//                    type = LinkedInInteractionTypeLike;
//                    segInteractionType = @"LinkedIn like";
//                }
//                else if ([objects[2] isEqualToString:@"update-comments"]) {
//                    type = LinkedInInteractionTypeComment;
//                    segInteractionType = @"LinkedIn comment";
//                }
//                
//                NCSocialStat *stat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:amount ofInteractionType:type];
//                [self.allStats addObject:stat];
//                block(@[stat], error);
//                
//                [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : segInteractionType, @"Amount" : @(amount), @"MessageID" : segNetworkId, @"Message" : headline.title, @"Through Buffer" : @NO, @"Network": @"LinkedIn", @"Url": headline.articleURL, @"Username": linkedInName }];
//
//            }
//          //  NSInteger
//        }
//        else {
//            block(@[], error);
//        }
//
//    }];
    
    [self.bufferClient fetchActivityUpdates:recentHeadlines withBlock:^(NSArray *objects, NSError *error) {
        if (!error && objects.count) {
            NSString *bufferName = self.loggedInBufferAccount ?: @"NoAccount";
            NSDictionary *statistics = objects.firstObject[@"statistics"];
            NCHeadline *headline = objects[1];
            NSLog(@"%@ on %@ has %@", headline.title, objects.firstObject[@"profile_service"], statistics);
            if (statistics) {
                NSInteger retweetsAmount   =  [statistics[@"retweets"] integerValue];
                NSInteger favoritesAmount  =  [statistics[@"favorites"] integerValue];
                NSInteger repliesAmount    =  [statistics[@"mentions"] integerValue];
                NSInteger likesAmount      =  [statistics[@"likes"] integerValue];
                NSInteger commentsAmount   =  [statistics[@"comments"] integerValue];
                
                if (retweetsAmount) {
                    NCSocialStat *retweetStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:retweetsAmount ofInteractionType:TwitterInteractionTypeRetweet];
                    [self.allStats addObject:retweetStat];
                    block(@[retweetStat], error);

                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter retweet", @"Amount" : @(retweetsAmount), @"MessageID" : headline.bufferTwitterId, @"Message" : headline.title, @"Through Buffer" : @YES, @"Network": @"Twitter", @"Url": headline.articleURL, @"Username": bufferName }];
                }
                
                if (favoritesAmount) {
                    NCSocialStat *favoriteStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:favoritesAmount ofInteractionType:TwitterInteractionTypeFavorite];
                    [self.allStats addObject:favoriteStat];
                    block(@[favoriteStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter favorite", @"Amount" : @(favoritesAmount), @"MessageID" : headline.bufferTwitterId, @"Message" : headline.title, @"Through Buffer" : @YES, @"Network": @"Twitter", @"Url": headline.articleURL, @"Username": bufferName }];
                    
                }
                
                if (repliesAmount) {
                    NCSocialStat *replyStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:repliesAmount ofInteractionType:TwitterInteractionTypeReply];
                    [self.allStats addObject:replyStat];
                    block(@[replyStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"Twitter reply", @"Amount" : @(repliesAmount), @"MessageID" : headline.bufferTwitterId, @"Message" : headline.title, @"Through Buffer" : @YES, @"Network": @"Twitter", @"Url": headline.articleURL, @"Username": bufferName }];
                }
                
                if (likesAmount) {
                    NCSocialStat *likeStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:likesAmount ofInteractionType:LinkedInInteractionTypeLike];
                    [self.allStats addObject:likeStat];
                    block(@[likeStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"LinkedIn like", @"Amount" : @(likesAmount), @"MessageID" : headline.bufferLinkedInId, @"Message" : headline.title, @"Through Buffer" : @YES, @"Network": @"LinkedIn", @"Url": headline.articleURL, @"Username": bufferName }];
                }
                
                if (commentsAmount) {
                    NCSocialStat *commentStat = [[NCSocialStat alloc] initWithHeadline:headline thatHas:commentsAmount ofInteractionType:LinkedInInteractionTypeComment];
                    [self.allStats addObject:commentStat];
                    block(@[commentStat], error);
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Social_Stat_Retrieved properties:@{ @"Interaction type" : @"LinkedIn comment", @"Amount" : @(commentsAmount), @"MessageID" : headline.bufferLinkedInId, @"Message" : headline.title, @"Through Buffer" : @YES, @"Network": @"LinkedIn", @"Url": headline.articleURL, @"Username": bufferName }];
                }
            }
            else {
                block(@[], error);
            }
        }
    }];
    
//    NSMutableString *tweetIds = [NSMutableString new];
//    
//    
//    for (NCHeadline *h in self.postedHeadlines) {
//        [tweetIds appendFormat:@"%li,", h.twitterId];
//    }
//    
//    [tweetIds deleteCharactersInRange:NSMakeRange([tweetIds length]-1, 1)];
//    [[NCTwitterIntegration sharedStore] fetchActivityUpdatesAlternatively:tweetIds withBlock:^(NSArray *objects, NSError *error) {
//        NSLog(@"%@", objects);
//        
//        NSDictionary *tweet = objects[0];
//        for (NSDictionary *tweet in objects) {
//            NSInteger retweetsAmount = [tweet[@"retweet_count"] integerValue];
//            NSInteger favoritesAmount = [tweet[@"favorite_count"] integerValue];
//            NSString *text = tweet[@"text"];
//            
//            if (retweetsAmount) {
//                NCSocialStat *retweetStat = [[NCSocialStat alloc] initWithMessage:text thatHas:retweetsAmount ofInteractionType:TwitterInteractionTypeRetweet];
//                [self.allStats addObject:retweetStat];
//                //NSLog(@"%li jeej een rt = ", retweetsAmount);
//            }
//            if (favoritesAmount) {
//                NCSocialStat *favoriteStat = [[NCSocialStat alloc] initWithMessage:text thatHas:favoritesAmount ofInteractionType:TwitterInteractionTypeFavorite];
//                NSLog(@"%li favo's ", favoritesAmount);
//            }
//           // NSLog(@"%@",tweet);
//        }
//        block(objects, error);
//    }];
    
}

- (NSURL *)constructBitlyURL:(NSURL *)url network:(NSString *)network account:(NSString *)account {
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSMutableArray *query = [NSMutableArray arrayWithArray:components.queryItems];
    
    [query addObject:[[NSURLQueryItem alloc] initWithName:@"ref" value:@"nineconnections"]];
    
    NSString *metadata = [NSString stringWithFormat:@"%@,%@", network];
    [query addObject:[[NSURLQueryItem alloc] initWithName:@"data" value:@"nineconnections"]];
    
    components.queryItems = query;
    return nil;
}

- (void)prepareForPosting:(NSString *)msg toNetwork:(SocialNetwork)network fromHeadline:(NCHeadline *)headline withBlock:(PostReturnBlock)block
{
    BOOL toBuffer = NO;
    NSString *shareMethod;
    NSString *optionalBufferMessage;
    NSString *optionalBufferURL;
    NSDictionary *bufferProfiles = self.bufferClient.profiles;
    
//    NSString *accountName;
//    NSString *networkString;
//    switch (network) {
//        case SocialNetworkBuffer:
//            networkString = 
//            break;
//            
//        default:
//            break;
//    }
    
    if (!msg) {
        msg = headline.title;
        toBuffer = [[NSUserDefaults standardUserDefaults] boolForKey:DEFAULTS_DRAG_TO_BUFFER];
        shareMethod = SEG_Share_Method_Drag;

        //msg = [NSString stringWithFormat:@"test %i", arc4random()];
    } else {
        toBuffer = [[NSUserDefaults standardUserDefaults] boolForKey:DEFAULTS_POPUP_TO_BUFFER];
        shareMethod = SEG_Share_Method_Modal;
    }
    
    NSError *bitlyError;
    NSString *bitlyApi = [NSString stringWithFormat:@"http://api.bit.ly/shorten?longUrl=%@&login=o_e7jifk6cs&apiKey=R_44be9c9c588f7cf35892096bf7517895&format=text",[headline.articleURL encodedStringFromURL]];
    NSString *optionalBitlyURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:bitlyApi] encoding:NSUTF8StringEncoding error:&bitlyError];
    //NSLog(@"val: %@ %@ %@", [headline.articleURL encodedStringFromURL], optionalBitlyURL, bitlyError);
    if (bitlyError || ![optionalBitlyURL hasPrefix:@"http"]) {
        optionalBitlyURL = [NSString stringWithFormat:@"%@", headline.articleURL];
    }
    
    NSString *fullMessage = [NSString stringWithFormat:@"%@ %@", [msg stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]], optionalBitlyURL];
    
    BOOL claimed = NO;
    if (network == SocialNetworkTwitter) {

        
        if (!toBuffer || ![bufferProfiles[@"Twitter"] length]) {
            claimed = YES;
           [[NCTwitterIntegration sharedClient] postTweet:fullMessage withBlock:^(NSString *messageId, NSError *error) {
                if (!error && messageId.length) {
                    NSString *twitterName = self.loggedInTwitterAccount.accountDescription ?: @"NoAccount";

                    NSLog(@"%@", messageId);
                    headline.twitterId = messageId;
                    [[NCDataStore sharedStore] saveHeadlinePostLocally:headline];

                    [[SEGAnalytics sharedAnalytics] track:SEG_Article_Shared properties:@{ @"Network" : @"Twitter", SEG_Share_Method_Key : shareMethod, @"Article" : headline.title,  @"Using Buffer" : @(toBuffer), @"Url": headline.articleURL, @"Username": twitterName }];
                }
                block(messageId, error);
            }];
        }
        else {
            optionalBufferMessage = fullMessage;
            optionalBufferURL = nil;
        }
    }
    else if (network == SocialNetworkLinkedIn) {
        //self.linkedInClient ;
        if (!toBuffer || ![bufferProfiles[@"LinkedIn"] length]) {
            claimed = YES;
            [self.linkedInClient postStatus:msg andURL:optionalBitlyURL withBlock:^(NSString *messageId, NSError *error) {
                if (!error && messageId.length) {
                    NSString *linkedInName = self.loggedInLinkedInAccount ?: @"NoAccount";

                    NSLog(@"linked in postt %@", messageId);
                    headline.linkedInId = messageId;
                    [[NCDataStore sharedStore] saveHeadlinePostLocally:headline];
                    
                    [[SEGAnalytics sharedAnalytics] track:SEG_Article_Shared properties:@{ @"Network" : @"LinkedIn", SEG_Share_Method_Key : shareMethod, @"Article" : headline.title,  @"Using Buffer" : @(toBuffer), @"Url": headline.articleURL, @"Username": linkedInName }];

                }
                block(messageId, error);
            }];
        }
        else {
            optionalBufferMessage = msg;
            optionalBufferURL = optionalBitlyURL;
        }
        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"NoAccount" object:self];
//        });
    }
    if (toBuffer && !claimed) {
        [self.bufferClient postMessage:optionalBufferMessage andURL:optionalBufferURL toNetwork:network withBlock:^(NSString *messageId, NSError *error) {
            if (!error && messageId.length) {
                NSString *bufferName = self.loggedInBufferAccount ?: @"NoAccount";

                NSString *stringNetwork;
                switch (network) {
                    case SocialNetworkLinkedIn: headline.bufferLinkedInId = messageId; stringNetwork = @"LinkedIn"; break;
                    case SocialNetworkTwitter: headline.bufferTwitterId = messageId; stringNetwork = @"Twitter"; break;
                    default: break;
                }
                [[NCDataStore sharedStore] saveHeadlinePostLocally:headline];
                
                [[SEGAnalytics sharedAnalytics] track:SEG_Article_Shared properties:@{ @"Network" : stringNetwork, SEG_Share_Method_Key : shareMethod, @"Article" : headline.title,  @"Using Buffer" : @(toBuffer), @"Url": headline.articleURL, @"Username": bufferName }];

            }
            NSLog(@"yay buff %@", messageId);
            block(messageId, error);
        }];
    }
}

- (ACAccount *)authenticateTwitterLogin
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *identifier = [userDefaults stringForKey:@"TwitterAccountID"];
    
   // if (identifier) {
        self.loggedInTwitterAccount = [[NCTwitterIntegration sharedClient] identifyAccount:identifier];

    if (identifier) {
    }
   // }
   // else
   // {
   //     self.loggedInAccount = nil;
   // }
    NSLog(@"Twitter acc: %@", self.loggedInTwitterAccount.accountDescription);

    return self.loggedInTwitterAccount;
}

- (void)retrieveAccountsFromNetwork:(SocialNetwork)network withBlock:(FetchReturnBlock)block sender:(id)sender
{
    if (network == SocialNetworkTwitter) {
        [[NCTwitterIntegration sharedClient] askForTwitterAccounts:block];
    }
    else if (network == SocialNetworkLinkedIn) {
        self.linkedInPresenter = sender;
        [self.linkedInClient getAuthorizationCode:^(NSString *code) {
            [self.linkedInClient getAccessToken:code success:^(NSDictionary *response) {
                NSString *token = response[@"access_token"];
                NSLog(@"val: %@", response);
                [self.linkedInClient fetchUserWithBlock:^(NSArray *objects, NSError *error) {
                    if (!error && objects.count) {
                        block(objects,error);
                    }
                }];
            } failure:^(NSError *error) {
                NSLog(@"Quering accessToken failed %@", error);
            }];
        } cancel:^{
            
        } failure:^(NSError *error) {
            
        }];
//        NSLog(@"NO LINKEDIN SUPPORT AS OF YET...");
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [UIAlertView showAlertWithTitle:@"We currently don't support LinkedIn."];
//        });
    }
    else if (network == SocialNetworkBuffer) {

    }
}

- (LIALinkedInHttpClient *)linkedInClient
{
    if (!_linkedInClient) {
        LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"http://www.nineconnections.com" clientId:LINKEDIN_API_KEY clientSecret:LINKEDIN_API_SECRET state:@"sgajdh33ga29e" grantedAccess:LINKEDIN_SCOPE_ARRAY];
        
        _linkedInClient = [LIALinkedInHttpClient clientForApplication:application presentingViewController:nil];
    }
    return _linkedInClient;
}

- (NCBufferIntegration *)bufferClient
{
    if (!_bufferClient) {
        _bufferClient = [[NCBufferIntegration alloc] init];
    }
    return _bufferClient;
}

- (void)authenticateNineConnectionsCookieWithBlock:(FetchReturnBlock)block
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.HTTPShouldSetCookies = YES;
    sessionConfig.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    sessionConfig.allowsCellularAccess = YES;
    //sessionConfig.timeoutIntervalForRequest = 889;

    NSMutableDictionary *httpHeaders = [NSMutableDictionary new];
//    for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
//        //NSLog(@"Cookie: %@", c.properties);
//        if ([c.domain isEqualToString:@"www.nineconnections.com"]) {
//            NSDictionary *cookieProperties = c.properties;
//            //NSLog(@"%@", cookieProperties);
//            for (NSString *key in [cookieProperties allKeys]) {
//                [httpHeaders setValue:[NSString stringWithFormat:@"%@", cookieProperties[key]] forKey:key];
//            }
//        }
//    }
    
    [httpHeaders setValue:@"no-cache" forKey:@"Cache-Control"];
    
    sessionConfig.HTTPAdditionalHeaders = httpHeaders;
    
    NSString *loginURLString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/user?%i", abs(arc4random())];
    
    NSURL *sessionURL = [NSURL URLWithString:loginURLString];


//    for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
//        //NSLog(@"THE COOKIE: %@", c);
//    }
    NSURLSessionDataTask *userTask = [[NSURLSession sharedSession] dataTaskWithURL:sessionURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *userJSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (!error && userJSONData) {
            NSString *avatarURLString = userJSONData[@"userImage"];
            if (![avatarURLString isKindOfClass:[NSNull class]] && avatarURLString) {
                NSLog(@"AVATAR:: %@", avatarURLString);
                [[NSUserDefaults standardUserDefaults] setObject:avatarURLString forKey:@"avatarURL"];
            }
            
            self.nineConnectionsUsername = userJSONData[@"userName"];
            self.userId = [userJSONData[@"id"] integerValue];
            
            //NSLog(@"userid: %zd & response is %@", self.userId, response);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            
            block(@[ httpResponse ? httpResponse : @"" ], error);
        }
    }];
    //headlinesTask.currentRequest
//    for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
//        NSLog(@"Cookie: %@", c.properties);
//        if ([c.domain isEqualToString:@"www.nineconnections.com"]) {
//            NSDictionary *cookieProperties = c.properties;
//            NSLog(@"%@", cookieProperties);
//            for (NSString *key in [cookieProperties allKeys]) {
//                [headlinesTask.currentRequest.mutableCopy addValue:[NSString stringWithFormat:@"%@", cookieProperties[key]] forHTTPHeaderField:key];
//            }
//        }
//    }
    //NSLog(@"REQQQQQQQ %@",headlinesTask.currentRequest.allHTTPHeaderFields);
    [userTask resume];
    
}

- (void)loginAtNineConnections:(NSArray *)credentials withBlock:(FetchReturnBlock)block
{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.HTTPShouldSetCookies = YES;
    sessionConfig.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    NSString *loginURLString = [NSString stringWithFormat:@"http://www.nineconnections.com/api/user/signin?username=%@&password=%@&%i", credentials[0],  credentials[1], abs(arc4random())];
    
    NSLog(@"Logging in with URL: %@", loginURLString);
    
    NSURL *sessionURL = [NSURL URLWithString:loginURLString];

    NSURLSessionDataTask *headlinesTask = [[NSURLSession sharedSession] dataTaskWithURL:sessionURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (!error && jsonData && httpResponse) {
            block(@[jsonData, httpResponse], error);
        }
    }];
    [headlinesTask resume];
    
}

#pragma mark - Storage & retrieval

- (NSString *)loggedInLinkedInAccount
{
    
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"LinkedInAccountName"];
}

- (NSString *)loggedInBufferAccount
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"BufferAccountNames"];
}

- (void)saveHeadlinePostLocally:(NCHeadline *)headline
{
    //    NSLog(@"%ld aaaa",(long)headline.twitterId);
    //   for (NCHeadline *h in [self retrievePostedHeadlinesFromArchive]) {
    //     NSLog(@"%@ %li", h.title, h.twitterId);
    // }
    
    [self.postedHeadlines addObject:headline];
    
    [NSKeyedArchiver archiveRootObject:self.postedHeadlines toFile:[NCDataStore headlinesArchivePath]];
    // NSLog(@"NCDataStore: %@", self.postedHeadlines);
}

- (void)saveSeenHeadlinesLocally
{
//    NSLog(@"im now saving: %@", self.sequentialSeenHeadlines);
//    [NSKeyedArchiver archiveRootObject:self.sequentialSeenHeadlines toFile:[NCDataStore seenHeadlineArchivePath]];
    NSMutableDictionary *dictionaryToBeSaved = [self.seenHeadlinesByFeed copy];
    NSInteger maxCountPerFeed = 20;
    
    for (NSNumber *key in dictionaryToBeSaved.allKeys) {
        NSMutableOrderedSet *headlinesInFeed = self.seenHeadlinesByFeed[key];
        if (headlinesInFeed.count > maxCountPerFeed) {
            NSMutableArray *setToBeSaved = [NSMutableArray new];
            int count = 0;
            
            for (NCHeadline *headline in headlinesInFeed) {
                [setToBeSaved addObject:headline];
                if (count == maxCountPerFeed) break;
            }
            
            [headlinesInFeed removeAllObjects];
            [headlinesInFeed addObjectsFromArray:setToBeSaved];
        }
        
    }
    [NSKeyedArchiver archiveRootObject:dictionaryToBeSaved toFile:[NCDataStore seenHeadlineArchivePath]];
    NSLog(@"shared url apth %@", [[NCDataStore seenHeadlineArchiveSharedURL] path]);
    [NSKeyedArchiver archiveRootObject:dictionaryToBeSaved toFile:[[NCDataStore seenHeadlineArchiveSharedURL] path]];
}

- (void)saveFeedsLocally
{
    [NSKeyedArchiver archiveRootObject:self.allFeeds toFile:[[NCDataStore feedsArchiveSharedURL] path]];
}

//- (void)setLastSeenHeadlineId:(NSInteger)lastSeenHeadlineId forFeedId:(NSInteger)feedId{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        NSMutableDictionary *lastSeenHeadlineIds = [[userDefaults dictionaryForKey:@"LastSeenHeadlineIDs"] mutableCopy];
//        [lastSeenHeadlineIds setValue:@(lastSeenHeadlineId) forKey:[NSString stringWithFormat:@"%zd", feedId]];
//        [userDefaults setObject:lastSeenHeadlineIds forKey:@"LastSeenHeadlineIDs"];
//        NSLog(@"IDS, %@", lastSeenHeadlineIds);
//    });
//}

//- (NSInteger)lastSeenHeadlineIdForFeedId:(NSInteger)feedId
//{
//    NSDictionary *lastSeenHeadlineIds = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"LastSeenHeadlineIDs"];
//    NSInteger headlineId = [lastSeenHeadlineIds[[NSString stringWithFormat:@"%zd", feedId]] integerValue];
//    return headlineId;
//}

- (void)storeTwitterAccountLocally:(ACAccount *)account
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account.identifier forKey:@"TwitterAccountID"];
}

- (void)storeLinkedInAccountLocally:(NSString *)account
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account forKey:@"LinkedInAccountName"];
}

- (void)storeBufferAccountLocally:(NSString *)account
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:account forKey:@"BufferAccountNames"];
}

- (void)storeAvatarImageLocally:(UIImage *)avatar
{
    if (avatar) {
        [NSKeyedArchiver archiveRootObject:avatar toFile:[NCDataStore avatarArchivePath]];
    }
}

- (NSOrderedSet *)retrievePostedHeadlines
{
    if (self.postedHeadlines.count == 0){
        NSArray *archiveContents = [NSKeyedUnarchiver unarchiveObjectWithFile:[NCDataStore headlinesArchivePath]];
        if (archiveContents) {
            for (NCHeadline *h in [archiveContents reverseObjectEnumerator]) {
                [self.postedHeadlines addObject:h];
                if (_postedHeadlines.count == 25) break;
            }
        }
    }
    return self.postedHeadlines;
}

- (NSArray *)retrieveStoredFeeds
{
    NSArray *archiveContents = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NCDataStore feedsArchiveSharedURL] path]];
    
    return archiveContents;
}

- (NSMutableDictionary *)retrieveSeenHeadlines
{
    //NSLog(@"im now retrieving: %zd headlines and %zd is how many feeds you have", self.seenHeadlines.count, self.feedsPrivate.count);
    
    id archiveContents = [NSKeyedUnarchiver unarchiveObjectWithFile:[NCDataStore seenHeadlineArchivePath]];

    if (archiveContents) {
        if ([archiveContents isKindOfClass:[NSDictionary class]]) {
            self.seenHeadlinesByFeed = [archiveContents mutableCopy];
        }

//        for (NCFeed *feed in self.feedsPrivate) {
//            NSOrderedSet *headlinesByFeed = [archiveContents filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"feedHash == %zd", feed.hash]];
//            [self.seenHeadlinesByFeed setObject:headlinesByFeed forKey:@(feed.hash)];
//        }
        
//        NSInteger seenMax = 30;
////        if (self.feedsPrivate.count > 3) seenMax = 70;
//        for (NCHeadline *h in archiveContents) {
//            [self.sequentialSeenHeadlines addObject:h];
//            if (self.sequentialSeenHeadlines.count > seenMax) break;
//        }
    }
    return self.seenHeadlinesByFeed;
}

- (NSMutableOrderedSet *)sequentialSeenHeadlines
{
    if (!_sequentialSeenHeadlines.count) {
        for (NSNumber *key in self.seenHeadlinesByFeed.allKeys) {
            [_sequentialSeenHeadlines unionOrderedSet:self.seenHeadlinesByFeed[key]];
        }
        [_sequentialSeenHeadlines sortUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO] ]];
    }
    return _sequentialSeenHeadlines;
}

- (UIImage *)retrieveStoredAvatar
{
    UIImage *archiveContents = [NSKeyedUnarchiver unarchiveObjectWithFile:[NCDataStore avatarArchivePath]];
    UIImage *avatar;
    if (archiveContents) {
        avatar = archiveContents;
    }
    return avatar;
}

- (NSArray *)getAllStats {
    return self.allStats;
}

- (NSString *)initialAvatarURL
{
    if (!_initialAvatarURL) {
        _initialAvatarURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatarURL"];
    }
    return _initialAvatarURL;
}

+ (NSString *)headlinesArchivePath
{
    return [[NCDataStore documentDirectory] stringByAppendingPathComponent:@"posted-headlines.archive"];
}

+ (NSString *)avatarArchivePath
{
    return [[NCDataStore documentDirectory] stringByAppendingPathComponent:@"avatar.archive"];
}

+ (NSString *)seenHeadlineArchivePath
{
    return [[NCDataStore documentDirectory] stringByAppendingPathComponent:@"seen-headlines.archive"];
}

+ (NSURL *)feedsArchiveSharedURL
{
    return [[NCDataStore sharedDocumentDirectory] URLByAppendingPathComponent:@"feeds.archive"];
}

+ (NSURL *)seenHeadlineArchiveSharedURL
{
    NSLog(@"asdasdas%@", [NCDataStore sharedDocumentDirectory]);
    return [[NCDataStore sharedDocumentDirectory] URLByAppendingPathComponent:@"seen-headlines.archive"];
}

+ (NSString *)documentDirectory
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    return documentDirectory;
}

+ (NSURL *)sharedDocumentDirectory
{
    return [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:@"group.com.kimengi.NineConnections"];
}

- (NSString *)userNameForHockeyManager:(BITHockeyManager *)hockeyManager componentManager:(BITHockeyBaseManager *)componentManager
{
    return self.nineConnectionsUsername;
}

- (void)logoutAtNineConnections
{
    for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:c];
        NSLog(@"%@", c.domain);
    }

    NSError *error;
    for (NSString *path in @[
                                 [NCDataStore avatarArchivePath],
                                 [NCDataStore headlinesArchivePath],
                                 [NCDataStore seenHeadlineArchivePath],
                                 [[NCDataStore seenHeadlineArchiveSharedURL] path],
                                 [[NCDataStore feedsArchiveSharedURL] path]
                            ])
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:path])
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        NSLog(@"val: %@", error);
    }
    
    for (NXOAuth2Account *account in [[NXOAuth2AccountStore sharedStore] accounts]) {
        [[NXOAuth2AccountStore sharedStore] removeAccount:account];
    }
    
    [self.allStats removeAllObjects];
    [self.postedHeadlines removeAllObjects];
    [self.headlinesPrivate removeAllObjects];
    [self.allFeeds removeAllObjects];
    [self.sequentialSeenHeadlines removeAllObjects];
    [self.seenHeadlinesByFeed removeAllObjects];
    
    self.nineConnectionsUsername = nil;
    self.loggedInBufferAccount = nil;
    self.loggedInTwitterAccount = nil;
    self.loggedInLinkedInAccount = nil;

    [[NSUserDefaults standardUserDefaults] resetAppDefaults];
}

- (void)identifyUser
{
    NSString *username = self.nineConnectionsUsername ? self.nineConnectionsUsername : [[NSUserDefaults standardUserDefaults] stringForKey:@"NineConnections username"];
    [[SEGAnalytics sharedAnalytics] identify:username ? [username lowercaseString] : @"NoAccount"
                                      traits:@{
                                                   @"Twitter account"       :   self.loggedInTwitterAccount.accountDescription ? self.loggedInTwitterAccount.accountDescription : @"NoAccount",
                                                   @"LinkedIn account"      :   self.loggedInLinkedInAccount ? self.loggedInLinkedInAccount : @"NoAccount",
                                                   @"Buffer account"        :   self.loggedInBufferAccount ? self.loggedInBufferAccount : @"NoAccount",
                                                   @"Twitter followers"     :   @([[NSUserDefaults standardUserDefaults] integerForKey:@"Twitter followers"]),
                                                   @"LinkedIn connections"  :   @([[NSUserDefaults standardUserDefaults] integerForKey:@"LinkedIn connections"]),
                                                   @"$ios_devices"          :   @[[[NSUserDefaults standardUserDefaults] objectForKey:@"Push token"] ?: @""]
                                             }];
}


#pragma mark - Custom setter methods

- (void)setNineConnectionsUsername:(NSString *)nineConnectionsUsername
{
    if (!_nineConnectionsUsername) {
        _nineConnectionsUsername = [nineConnectionsUsername lowercaseString];
        [[NSUserDefaults standardUserDefaults] setObject:_nineConnectionsUsername forKey:@"NineConnections username"];
        [self identifyUser];
    }
    else _nineConnectionsUsername = nineConnectionsUsername;
}

- (void)setLoggedInBufferAccount:(NSString *)loggedInBufferAccount
{
    if (!_loggedInBufferAccount) {
        _loggedInBufferAccount = loggedInBufferAccount;
       [self identifyUser];
    }
    else _loggedInBufferAccount = loggedInBufferAccount;
}

- (void)setLoggedInLinkedInAccount:(NSString *)loggedInLinkedInAccount
{
    if (!_loggedInLinkedInAccount) {
        _loggedInLinkedInAccount = loggedInLinkedInAccount;
        [self identifyUser];
    }
    else _loggedInLinkedInAccount = loggedInLinkedInAccount;
}

- (void)setLoggedInTwitterAccount:(ACAccount *)loggedInTwitterAccount
{
    if (!_loggedInTwitterAccount) {
        _loggedInTwitterAccount = loggedInTwitterAccount;
        [self identifyUser];
    }
    else _loggedInTwitterAccount = loggedInTwitterAccount;
}

@synthesize loggedInBufferAccount = _loggedInBufferAccount;
@synthesize loggedInTwitterAccount = _loggedInTwitterAccount;
@synthesize loggedInLinkedInAccount = _loggedInLinkedInAccount;


@end
