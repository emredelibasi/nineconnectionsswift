//
//  NSUserDefaults+AppDefaults.h
//  NineConnections
//
//  Created by Floris van der Grinten on 28-08-14.
//
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface NSUserDefaults (AppDefaults)

- (void)resetAppDefaults;
- (void)registerDefaultPreferences;

@end
