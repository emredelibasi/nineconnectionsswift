//
//  NCHeadline.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/4/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

class NCHeadline: NSObject, NSCoding {
    
    let id: Int
    var title: String
    var url: URL?
    var date: Date?
    var sharedAt = Date()
    var seenAt = Date()
    var bookmarkedAt = Date()
    var articleText: String?
    var articleImage: UIImage?
    var trends = [String]()
    var tags = [String]()
    var isSeen = false
    var isBookmarked = false
    var sharedInstantlyOnTwitter: Bool = false
    var sharedInstantlyOnLinkedIn: Bool = false
    var sharedWithScheduleOnTwitter: Bool = false
    var sharedWithScheduleOnLinkedIn: Bool = false
    var shared:Bool {
        get {
            return sharedInstantlyOnTwitter == true || sharedInstantlyOnLinkedIn == true || sharedWithScheduleOnTwitter == true || sharedWithScheduleOnLinkedIn == true
        }
    }
    var smartShared: Bool {
        get {
            return sharedWithScheduleOnTwitter == true || sharedWithScheduleOnLinkedIn == true
        }
    }
    
    init(id: Int, title: String, url: URL) {
        
        self.id = id
        self.title = title
        self.url = url
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "headlineId")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.url, forKey: "URL")
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.isSeen, forKey: "isSeen")
        aCoder.encode(self.isBookmarked, forKey: "isBookmarked")
        aCoder.encode(self.sharedAt, forKey: "sharedAt")
        aCoder.encode(self.trends, forKey: "trends")
        aCoder.encode(self.tags, forKey: "tags")
        aCoder.encode(self.seenAt, forKey: "seenAt")
        aCoder.encode(self.bookmarkedAt, forKey: "bookmarkedAt")
        aCoder.encode(self.sharedInstantlyOnTwitter, forKey: "sharedInstantlyOnTwitter")
        aCoder.encode(self.sharedInstantlyOnLinkedIn, forKey: "sharedInstantlyOnLinkedIn")
        aCoder.encode(self.sharedWithScheduleOnTwitter, forKey: "sharedWithScheduleOnTwitter")
        aCoder.encode(self.sharedWithScheduleOnLinkedIn, forKey: "sharedWithScheduleOnLinkedIn")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let headlineTitle = aDecoder.decodeObject(forKey: "title") as? String,
            let headlineURL = aDecoder.decodeObject(forKey: "URL") as? URL,
            let headlineSharedAt = aDecoder.decodeObject(forKey: "sharedAt") as? Date,
            let headlineSeenAt = aDecoder.decodeObject(forKey: "seenAt") as? Date else {
                return nil
        }
        
        let id = aDecoder.decodeInteger(forKey: "headlineId")
        let trends = aDecoder.decodeObject(forKey: "trends") as? [String]
        let tags = aDecoder.decodeObject(forKey: "tags") as? [String]
        
        self.init(id: id, title: headlineTitle, url: headlineURL)

        self.articleText = aDecoder.decodeObject(forKey: "articleText") as? String
        self.date = aDecoder.decodeObject(forKey: "date") as? Date
        self.sharedAt = headlineSharedAt
        self.trends = trends != nil ? trends! : []
        self.tags = tags != nil ? tags! : []
        self.seenAt = headlineSeenAt
        
        // MARK: TEST BEFORE BOOKMARKS RELEASE
        
        if let headlineBookmarkedAt = aDecoder.decodeObject(forKey: "bookmarkedAt") as? Date,
            let bookmarked = aDecoder.decodeObject(forKey: "isBookmarked") as? Bool {
            self.bookmarkedAt = headlineBookmarkedAt
            self.isBookmarked = bookmarked
        } else {
            self.isBookmarked = false
        }
        
//        self.isBookmarked = aDecoder.decodeBool(forKey: "isBookmarked")
        self.sharedInstantlyOnTwitter = aDecoder.decodeBool(forKey: "sharedInstantlyOnTwitter")
        self.sharedInstantlyOnLinkedIn = aDecoder.decodeBool(forKey: "sharedInstantlyOnLinkedIn")
        self.sharedWithScheduleOnTwitter = aDecoder.decodeBool(forKey: "sharedWithScheduleOnTwitter")
        self.sharedWithScheduleOnLinkedIn = aDecoder.decodeBool(forKey: "sharedWithScheduleOnLinkedIn")
    }
    
}
