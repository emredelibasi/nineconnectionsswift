//
//  NCCompany.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 1/20/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCCompany: NSObject, NSCoding {
    
    let id: Int
    let name: String
    let code: String
    var mainNavBarColor: UIColor?
    var innerNavBarColor: UIColor?
    var headerColor: UIColor?
    var rowColor: UIColor?
    var rowTextColor: UIColor?
    var rowSeparatorColor: UIColor?
    var rowSelectedColor: UIColor?
    var rowSelectedTextColor: UIColor?
    var unreadCountColor: UIColor?
    var unreadCountSelectedColor: UIColor?
    var contactColor: UIColor?
    var contactTextColor: UIColor?
    var logoutColor: UIColor?
    var logoutTextColor: UIColor?
    var backgroundColor: UIColor?
    var secondaryBackgroundColor: UIColor?
    var logoUrlString: String?
    var logo: UIImage?
    
    init(id: Int, name: String, code: String) {
        
        self.id = id
        self.name = name
        self.code = code
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.code, forKey: "code")
        aCoder.encode(self.mainNavBarColor, forKey: "mainNavBarColor")
        aCoder.encode(self.innerNavBarColor, forKey: "innerNavBarColor")
        aCoder.encode(self.headerColor, forKey: "headerColor")
        aCoder.encode(self.rowColor, forKey: "rowColor")
        aCoder.encode(self.rowTextColor, forKey: "rowTextColor")
        aCoder.encode(self.rowSeparatorColor, forKey: "rowSeparatorColor")
        aCoder.encode(self.rowSelectedColor, forKey: "rowSelectedColor")
        aCoder.encode(self.rowSelectedTextColor, forKey: "rowSelectedTextColor")
        aCoder.encode(self.unreadCountColor, forKey: "unreadCountColor")
        aCoder.encode(self.unreadCountSelectedColor, forKey: "unreadCountSelectedColor")
        aCoder.encode(self.contactColor, forKey: "contactColor")
        aCoder.encode(self.contactTextColor, forKey: "contactTextColor")
        aCoder.encode(self.logoutColor, forKey: "logoutColor")
        aCoder.encode(self.logoutTextColor, forKey: "logoutTextColor")
        aCoder.encode(self.backgroundColor, forKey: "backgroundColor")
        aCoder.encode(self.secondaryBackgroundColor, forKey: "secondaryBackgroundColor")

        
        if self.logo != nil {
            let imageData = UIImagePNGRepresentation(self.logo!)!
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            aCoder.encode(strBase64, forKey: "base64logo")
        }
    }
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String,
        let code = aDecoder.decodeObject(forKey: "code") as? String
        else { return nil }

        let id = aDecoder.decodeInteger(forKey: "id")
        self.init(id: id, name: name, code: code)
        
        self.mainNavBarColor = aDecoder.decodeObject(forKey: "mainNavBarColor") as? UIColor
        self.innerNavBarColor = aDecoder.decodeObject(forKey: "innerNavBarColor") as? UIColor
        self.headerColor = aDecoder.decodeObject(forKey: "headerColor") as? UIColor
        self.rowColor = aDecoder.decodeObject(forKey: "rowColor") as? UIColor
        self.rowTextColor = aDecoder.decodeObject(forKey: "rowTextColor") as? UIColor
        self.rowSeparatorColor = aDecoder.decodeObject(forKey: "rowSeparatorColor") as? UIColor
        self.rowSelectedColor = aDecoder.decodeObject(forKey: "rowSelectedColor") as? UIColor
        self.rowSelectedTextColor = aDecoder.decodeObject(forKey: "rowSelectedTextColor") as? UIColor
        self.unreadCountColor = aDecoder.decodeObject(forKey: "unreadCountColor") as? UIColor
        self.unreadCountSelectedColor = aDecoder.decodeObject(forKey: "unreadCountSelectedColor") as? UIColor
        self.contactColor = aDecoder.decodeObject(forKey: "contactColor") as? UIColor
        self.contactTextColor = aDecoder.decodeObject(forKey: "contactTextColor") as? UIColor
        self.logoutColor = aDecoder.decodeObject(forKey: "logoutColor") as? UIColor
        self.logoutTextColor = aDecoder.decodeObject(forKey: "logoutTextColor") as? UIColor
        self.backgroundColor = aDecoder.decodeObject(forKey: "backgroundColor") as? UIColor
        self.secondaryBackgroundColor = aDecoder.decodeObject(forKey: "secondaryBackgroundColor") as? UIColor

        let strBase64 = aDecoder.decodeObject(forKey: "base64logo") as? String
        if strBase64 != nil {
            let dataDecoded = Data(base64Encoded: strBase64!, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
            self.logo = UIImage(data: dataDecoded)!
        }
    }
    
}
