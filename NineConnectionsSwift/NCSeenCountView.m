//
//  NCSeenCountView.m
//  NineConnections
//
//  Created by Floris van der Grinten on 15-08-14.
//
//

#import "NCSeenCountView.h"

@interface NCSeenCountView ()

@property (nonatomic, assign) CGFloat lowestCount;

@end

@implementation NCSeenCountView

- (instancetype)initWithArticleCount:(NSInteger)count excistingTitle:(UINavigationItem *)navItem fontSize:(CGFloat)size
{
    self = [super init];
    if (self) {
        CGFloat length = 18;
        self.frame = CGRectMake(0, 0, length, length);
        //self.backgroundColor = [UIColor whiteColor];
        self.count = count;
       // UIFont *font = [UIFont fontWithName:@"Lato-LightItalic" size:18];
        
        UILabel *originalTitleView = (UILabel *)navItem.titleView;
        
        //originalTitleView.frame = CGRectMake(0, 0, CGRectGetWidth(originalTitleView.frame), CGRectGetHeight(originalTitleView.frame));
//        NSLog(@"%@ %@", originalTitleView.font, originalTitleView.text);
      //  originalTitleView.frame = originalTitleView.bounds;
      //  [self addSubview:originalTitleView];
        
        UILabel *title = [UILabel new];
        NSMutableAttributedString *attributedString;
        attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", originalTitleView.text]];
        [attributedString addAttribute:NSKernAttributeName value:@-0.5 range:NSMakeRange(0, originalTitleView.text.length)];
        [title setAttributedText:attributedString];
        title.font = originalTitleView.font;
        title.textColor = originalTitleView.textColor;
        [title sizeToFit];
        
        
        self.countLabel = [UILabel new];
        self.countLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:size];
        self.countLabel.textColor = [UIColor whiteColor];
        self.countLabel.text = [NSString stringWithFormat:@"  %@", self.count ? @(self.count) : @" "];
        [self.countLabel sizeToFit];
        self.countLabel.adjustsFontSizeToFitWidth = YES;
        self.countLabel.frame = CGRectOffset(self.countLabel.frame, CGRectGetMaxX(title.frame) + 3, 5);
        [self addSubview:title];
        [self addSubview:self.countLabel];

       // [originalTitleView removeFromSuperview];


        
        _lowestCount = self.count;
       // self.alpha = 0.99;
        
      //  self.center = CGPointMake(CGFloat x, CGFloat y)
    }
    return self;
}

- (void)setCount:(NSInteger)count
{
    _count = count;


    if (_count <= _lowestCount) {
        NSString *countToDisplay = _count ? [NSString stringWithFormat:@"%zd", _count] : @" ";
        
        self.countLabel.text = countToDisplay;
        self.lowestCount = _count;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
