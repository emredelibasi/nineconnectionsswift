//
//  HeadlineStore.h
//  NineConnections
//
//  Created by Floris van der Grinten on 20-05-14.
//
//

#import <Foundation/Foundation.h>
//#import <HockeySDK/HockeySDK.h>
#import "NCHeadline.h"
#import "NCTwitterIntegration.h"
#import "NCBufferIntegration.h"
#import "NCSocialStat.h"
#import "NCFeed.h"
#import "NSURL+EncodeURL.h"
#import "NSUserDefaults+AppDefaults.h"
//#import <IOSLinkedInAPI/IOSLinkedInAPI.h>
//#import "LIALinkedInApplication.h"
//#import "LIALinkedInHttpClient.h"
//#import "NXOAuth2.h"


@interface NCDataStore : NSObject

@property (nonatomic) NSString *initialAvatarURL;
@property (nonatomic, strong, readonly) ACAccount *loggedInTwitterAccount;
@property (nonatomic, strong, readonly) NSString *loggedInLinkedInAccount;
@property (nonatomic, strong, readonly) NSString *loggedInBufferAccount;
@property (nonatomic, strong, readonly) NSString *nineConnectionsUsername;
@property (nonatomic, strong, readonly) NCBufferIntegration *bufferClient;
@property (nonatomic, readonly) NSMutableOrderedSet *postedHeadlines;
@property (nonatomic, readonly) NSMutableOrderedSet *sequentialSeenHeadlines;
@property (nonatomic, strong) NSMutableDictionary *seenHeadlinesByFeed;
@property (atomic, readonly) NSMutableArray *allFeeds;

+ (instancetype)sharedStore;

- (void)fetchAllFeedsWithBlock:(FetchReturnBlock)block;
- (void)fetchNewHeadlinesFromFeed:(NCFeed *)feed withBlock:(FetchReturnBlock)block;
- (void)fetchNewStatsWithBlock:(FetchReturnBlock)block;
- (void)fetchFullArticleTextFromHeadline:(NCHeadline *)headline inFeed:(NCFeed *)feed withBlock:(FetchReturnBlock)block;

- (NSArray *)retrievePostedHeadlines;
- (NSArray *)retrieveSeenHeadlines;
- (NSArray *)retrieveStoredFeeds;
- (UIImage *)retrieveStoredAvatar;

- (void)prepareForPosting:(NSString *)msg toNetwork:(SocialNetwork)network fromHeadline:(NCHeadline *)headline withBlock:(PostReturnBlock)block;

- (void)retrieveAccountsFromNetwork:(SocialNetwork)network withBlock:(FetchReturnBlock)block sender:(id)sender;
- (void)storeTwitterAccountLocally:(ACAccount *)account;
- (void)storeLinkedInAccountLocally:(NSString *)account;
- (void)storeBufferAccountLocally:(NSString *)account;
- (void)storeAvatarImageLocally:(UIImage *)avatar;
- (ACAccount *)authenticateTwitterLogin;

- (void)loginAtNineConnections:(NSArray *)credentials withBlock:(FetchReturnBlock)block;
- (void)logoutAtNineConnections;
- (void)authenticateNineConnectionsCookieWithBlock:(FetchReturnBlock)block;

//- (void)setLastSeenHeadlineId:(NSInteger)lastSeenHeadlineId forFeedId:(NSInteger)feedId;
//- (NSInteger)lastSeenHeadlineIdForFeedId:(NSInteger)feedId;

- (void)saveSeenHeadlinesLocally;

@end
