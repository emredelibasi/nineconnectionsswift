 //
//  AppDelegate.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/3/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Analytics
import NXOAuth2Client
import Social
import Fabric
import Crashlytics
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var launchedFromURL = false
    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // NC TOKEN
        let smoochSettings = SKTSettings(appToken: "dpapa9jeg8g0cskuvo3q3xkwd")
        // TEST TOKEN
        //let smoochSettings = SKTSettings(appToken: "7vm9r64dp2yzedhqk8x6xhao8")

        smoochSettings.conversationStatusBarStyle = .lightContent
        Smooch.initWith(smoochSettings)
        
        UINavigationBar.appearance().barTintColor = UIColor.backgroundGrey()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName : UIFont(name: "ProximaNova-Semibold", size: 19.0)!]
        
        UIApplication.shared.statusBarStyle = .lightContent
        UISwitch.appearance().onTintColor = UIColor.mainBlue()
        UserDefaults.standard.registerDefaultPreferences()
        
        // NC TOKEN
        let segConf = SEGAnalyticsConfiguration(writeKey: "6tduyu2i8w")
        // TEST TOKEN
//        let segConf = SEGAnalyticsConfiguration(writeKey: "hy6LpYdEo8yORyOYCDhy02fBG1uFXpLb")
        
        segConf?.flushAt = 1
        SEGAnalytics.setup(with: segConf)
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            SEGAnalytics.shared().disable()
        #endif
        
        _ = NCDataStoreSwift.sharedStore.retrieveUser()
        _ = NCDataStoreSwift.sharedStore.retrieveSeenHeadlines()
        _ = NCDataStoreSwift.sharedStore.retrievePostedHeadlines()
        _ = NCDataStoreSwift.sharedStore.retrieveBookmarkedHeadlines()
        
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        let user = NCDataStoreSwift.sharedStore.retrieveUser()
        if user != nil {
            Crashlytics.sharedInstance().setUserIdentifier(String(user!.id))
            Crashlytics.sharedInstance().setUserName(user!.username)
        }
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                // actions based on whether notifications were authorized or not
            }
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let userNotificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: userNotificationTypes, categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        if let payload = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary, let aps = payload["aps"] as? NSDictionary, let identifier = aps["identifier"] as? String {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: identifier)
            window?.rootViewController = vc
        }
        
        application.registerForRemoteNotifications()

        return true
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
        if let articleString = notification.userInfo?["articleURL"] as? String, let articleURL = URL(string: articleString), let components = URLComponents(url: articleURL, resolvingAgainstBaseURL: true) {
            self.navigateToArticleFor(components: components)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let articleString = userInfo["articleURL"] as? String, let articleURL = URL(string: articleString), let components = URLComponents(url: articleURL, resolvingAgainstBaseURL: true) {
            self.navigateToArticleFor(components: components)
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        SEGAnalytics.shared().registerForRemoteNotifications(withDeviceToken: deviceToken)
        let chars = ["<", ">", " "]
        var formattedToken = deviceToken.description
        for c in chars {
            formattedToken = formattedToken.replacingOccurrences(of: c, with: "")
        }
        UserDefaults.standard.set(formattedToken, forKey: "Push token")
        
        SEGAnalytics.shared().track("Push registration successful", properties: ["Device": deviceToken.description])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        SEGAnalytics.shared().track("Push notification access denied", properties: ["Potential error": error.localizedDescription.characters.count > 0 ? error.localizedDescription : "N/A"])
        UserDefaults.standard.set("", forKey: "Push token")
        print("Push registration failed")
    }
    
//    func application(application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
//        print("launch from url? \(self.launchedFromURL)")
//        return true
//    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    //  MARK: Universal Linking Setup
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let webURL = userActivity.webpageURL {
            if !present(URL: webURL) {
                UIApplication.shared.openURL(webURL)
            }
        }

        return true
    }
    
    private func present(URL url: URL) -> Bool {
        
        if let components = URLComponents(url: url, resolvingAgainstBaseURL: true), let host = components.host {
            
            let pathComponents = components.path.components(separatedBy: "/")
            
            switch host {
                case "app.nineconnections.com":
                    if pathComponents.count >= 4 {
                        
                    }
                default:
                    return false
            }
        }
        return false
    }
    
    // MARK: URL Scheme For Login Redirect Setup
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        self.launchedFromURL = true
        let mainNavigationController: UINavigationController = self.window?.rootViewController as! UINavigationController
        mainNavigationController.popToRootViewController(animated: false)
        
        if (url.host != nil) {
            for vc: UIViewController in mainNavigationController.childViewControllers {
                vc.presentedViewController?.dismiss(animated: false, completion: nil)
            }
        }
        
        if let childVC = mainNavigationController.childViewControllers.first as? FeedListViewController, url.host != nil {
            childVC.redirectToLoginPage(withEmail: url.host)
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        UserDefaults.standard.synchronize()
        if (NCDataStoreSwift.sharedStore.loggedInUser != nil) {NCDataStoreSwift.sharedStore.storeUserLocally(NCDataStoreSwift.sharedStore.loggedInUser!)}
        NCDataStoreSwift.sharedStore.saveSeenHeadlinesLocally()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        _ = NCDataStoreSwift.sharedStore.retrieveUser()
        _ = NCDataStoreSwift.sharedStore.retrieveSeenHeadlines()
        _ = NCDataStoreSwift.sharedStore.retrievePostedHeadlines()
        _ = NCDataStoreSwift.sharedStore.retrieveBookmarkedHeadlines()

        NotificationCenter.default.post(name: Notification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        SEGAnalytics.shared().track(SEG_App_Opens, properties: ["Twitter followers": UserDefaults.standard.integer(forKey: "Twitter Followers"), "LinkedIn connections": UserDefaults.standard.integer(forKey: "LinkedIn connections"), "From widget": self.launchedFromURL])
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    @available(iOS 10.0, *)
    func scheduleNotification() {
                
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "New article in channel"
        content.body = "Share zis mofo"
        content.userInfo = ["articleURL" : "http://www.nineconnections.com/channel/43/item/159029", "data" : ["attachment-url" : "https://en.wikipedia.org/wiki/Galatasaray_S.K._(football)#/media/File:Galatasaray_Sports_Club_Logo.png"]]
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "asd"
        
        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
        
    }
    
    func navigateToArticleFor(components: URLComponents) {
        
        let pathComponents = components.path.components(separatedBy: "/")
        
        if pathComponents[1] == "channel" && pathComponents[3] == "item" {
            let channelID = pathComponents[2]
            let articleID = pathComponents[4]
            
            let storedFeeds = NCDataStoreSwift.sharedStore.retrieveStoredFeeds()
            
            // Checks if user has channel
            
            let feedIndex = storedFeeds.index(where: { (feed: NCFeed) -> Bool in
                feed.id  == Int(channelID)
            })
            
            if feedIndex != nil {
                
                let matchedFeed = storedFeeds[feedIndex!]
                
                // Checks if the channel contains the article
                
                let articleIndex = matchedFeed.headlines.index(where: { (headline: NCHeadline) -> Bool in
                    headline.id == Int(articleID)
                })
                
                if articleIndex != nil {
                    
                    let mainNavigationController = self.window?.rootViewController as! UINavigationController
                    mainNavigationController.popToRootViewController(animated: false)
                    
                    for vc: UIViewController in mainNavigationController.childViewControllers {
                        vc.presentedViewController?.dismiss(animated: false, completion: nil)
                    }
                    
                    if let mvc = mainNavigationController.storyboard?.instantiateViewController(withIdentifier: "MainVC") as? MainViewController {
                        
                        mvc.currentFeed = matchedFeed
                        mvc.fetchedHeadlines = matchedFeed.headlines
                        _ = mvc.view
                        mvc.offsetForPushNotification = CGPoint(x: mvc.windowWidth! * CGFloat(articleIndex!), y: 0)
                        
                        mainNavigationController.pushViewController(mvc, animated: false)
                    }
                    
                } else {
                    
                    let sharedHeadlines: [NCHeadline] = NCDataStoreSwift.sharedStore.postedHeadlines.reversed()
                    let articleIndex = sharedHeadlines.index(where: { (headline: NCHeadline) -> Bool in
                        headline.id == Int(articleID)
                    })
                    
                    if articleIndex != nil {
                        if sharedHeadlines[articleIndex!].smartShared {
                            locateAndNavigateToArticle(articleIndex: CGFloat(articleIndex!), in: "smartSharedMode", headlines: sharedHeadlines)
                        } else {
                            locateAndNavigateToArticle(articleIndex: CGFloat(articleIndex!), in: "sharedMode", headlines: sharedHeadlines)
                        }
                    } else {
                        
                        let bookmarkedHeadlines: [NCHeadline] = NCDataStoreSwift.sharedStore.retrieveBookmarkedHeadlines().reversed()
                        let articleIndex = bookmarkedHeadlines.index(where: { (headline: NCHeadline) -> Bool in
                            headline.id == Int(articleID)
                        })
                        
                        if articleIndex != nil {
                            locateAndNavigateToArticle(articleIndex: CGFloat(articleIndex!), in: "bookmarkedMode", headlines: sharedHeadlines)
                        } else {
                            let seenHeadlines: [NCHeadline] = NCDataStoreSwift.sharedStore.sequentialSeenHeadlines.reversed()
                            let articleIndex = seenHeadlines.index(where: { (headline: NCHeadline) -> Bool in
                                headline.id == Int(articleID)
                            })
                            
                            if articleIndex != nil {
                                locateAndNavigateToArticle(articleIndex: CGFloat(articleIndex!), in: "archiveMode", headlines: seenHeadlines)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func locateAndNavigateToArticle(articleIndex: CGFloat, in feedType: String, headlines: [NCHeadline]) {
        
        let mainNavigationController = self.window?.rootViewController as! UINavigationController
        mainNavigationController.popToRootViewController(animated: false)
        
        for vc: UIViewController in mainNavigationController.childViewControllers {
            vc.presentedViewController?.dismiss(animated: false, completion: nil)
        }
        
        if let mvc = mainNavigationController.storyboard?.instantiateViewController(withIdentifier: "MainVC") as? MainViewController {
            
            switch feedType {
            case "archiveMode":
                mvc.archiveMode = true
            case "sharedMode":
                mvc.sharedMode = true
            case "smartSharedMode":
                mvc.smartSharedMode = true
            case "bookmarkedMode":
                mvc.bookmarkedMode = true
            default:
                break
            }
        
            mvc.fetchedHeadlines = headlines
            mvc.offsetForPushNotification = CGPoint(x: UIScreen.main.bounds.width * articleIndex, y: 0)
            _ = mvc.view
            
            mainNavigationController.pushViewController(mvc, animated: false)
        }

    }
}

