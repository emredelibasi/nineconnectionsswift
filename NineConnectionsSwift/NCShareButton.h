//
//  ShareButton.h
//  NineConnections
//
//  Created by Floris van der Grinten on 16-05-14.
//
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "Constants.h"


@protocol ShareButtonDelegate

- (void)instantShareActivationBegan;
- (void)instantShareActivationEnded:(BOOL)finished toShareInstantly:(BOOL)instantly toNetwork:(SocialNetwork)network sender:(id)sender;
- (void)shareButtonTapped:(id)sender;
- (void)shareButtonMoving:(id)sender;


@end

@interface NCShareButton : UIView

@property (assign, nonatomic) SocialNetwork buttonType;
@property (assign, nonatomic) CGPoint centerPosition;
@property (assign, nonatomic) BOOL shareInteractionEnabled;
@property (assign, nonatomic, getter = isScheduled) BOOL scheduled;
@property (strong, nonatomic) UIDynamicAnimator *dynamicsAnimator;
@property (nonatomic, strong, readonly) UIColor *socialMediaColor;
@property (nonatomic, assign) double translationDetect;

@property (weak) id <ShareButtonDelegate> delegate;

- (instancetype)initWithButtonType:(SocialNetwork)buttonType;
- (void)addShareButtonsSnappingBehavior;
- (void)morphShareButton;
- (void)animateSharedState:(BOOL)shared;
- (BOOL)shouldShareInstantly;

@end


