//
//  TwitterCommunication.m
//  NineConnections
//
//  Created by Floris van der Grinten on 26-05-14.
//
//

#import "NCTwitterIntegration.h"


@interface NCTwitterIntegration ()

//@property (nonatomic) NSMutableArray *retweetsPrivate;
@property (strong, nonatomic) ACAccountStore *accountStore;

@end

@implementation NCTwitterIntegration

+ (instancetype)sharedClient{
    static NCTwitterIntegration *dataStore = nil;
    if (!dataStore) {
        dataStore = [[self alloc] initPrivate];
    }
    return dataStore;
}

- (instancetype)init{
    @throw [NSException exceptionWithName:@"Singleton" reason:nil userInfo:nil];
    return nil;
}

- (instancetype)initPrivate{
    self = [super init];
    if (self){
        self.accountStore = [ACAccountStore new];
       // self.dataStorePrivate = [NSMutableArray new];
    }
    return self;
}

- (ACAccount *)identifyAccount:(NSString *)identifier
{
    self.loggedInAccount = [self.accountStore accountWithIdentifier:identifier];
    return self.loggedInAccount;
}


- (void)postTweet:(NSString *)tweet withBlock:(PostReturnBlock)block
{
    if (self.loggedInAccount) {
        NSLog(@"posting tweet... %@", tweet);
        //[NSString stringWithFormat:@"dev test %i", arc4random()]
        NSDictionary *twitterMessage = @{ @"status" : tweet };
        NSURL *twitterRequestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
        
        SLRequest *postTweetRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                         requestMethod:SLRequestMethodPOST
                                                                   URL:twitterRequestURL
                                                            parameters:twitterMessage];
        
        postTweetRequest.account = self.loggedInAccount;
        [postTweetRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
            if (!error) {
                NSDictionary *returnedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error ];
                NSLog(@"statuscode: %zd", urlResponse.statusCode);
                switch (urlResponse.statusCode) {
                    case 200: {
                        //NSLog(@"%@ /n %@", returnedJSON, [returnedJSON[@"id"] stringValue]);
                        [[NSUserDefaults standardUserDefaults] setInteger:[returnedJSON[@"user"][@"followers_count"] integerValue] forKey:@"Twitter followers"];
                        block([returnedJSON[@"id"] stringValue], error);

                    } break;
                    default: {
                        NSDictionary *errors = [returnedJSON[@"errors"] firstObject];
                        NSInteger twitterStatusCode = [errors[@"code"] integerValue];
                        NSString *alertTitle, *alertText;
                        //twitterStatusCode = 220;
                        switch (twitterStatusCode) {
                            case 187:
                                alertTitle = TWITTER_ALERT_DUPLICATE_TITLE;
                                alertText = TWITTER_ALERT_DUPLICATE_TEXT;
                                break;
                            case 220:
                                alertTitle = TWITTER_ALERT_RENEW_TITLE;
                                alertText = TWITTER_ALERT_RENEW_TEXT;
                                break;
                            case 429:
                                alertTitle = TWITTER_ALERT_OVERCAPACITY_TITLE;
                                alertText = TWITTER_ALERT_OVERCAPACITY_TEXT;
                                break;
                            case 186:
                                alertTitle = TWITTER_ALERT_MAX_CHARS_TITLE;
                                alertText = TWITTER_ALERT_MAX_CHARS_TEXT;
                                break;
                            default:
                                alertTitle = TWITTER_ALERT_UNKNOWN_TITLE;
                                alertText = TWITTER_ALERT_UNKNOWN_TEXT;
                                break;
                        }
                        [[SEGAnalytics sharedAnalytics] track:@"Twitter status code" properties:@{ @"Account" : self.loggedInAccount.accountDescription ? self.loggedInAccount.accountDescription : @"NoAccount", @"Status code" : @(twitterStatusCode) }];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                        });
                        block(@"", error);

//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_NO_ACCOUNT_TITLE message:ALERT_NO_ACCOUNT_ACCESS_MESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [alert show];
//                        });
//                        [self.accountStore renewCredentialsForAccount:self.loggedInAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
//                            NSLog(@"%ld",renewResult);
//                        }];
                    } break;
                        
                }
       //         NSLog(@"%zd",urlResponse.statusCode);
                           }
            else {
                [self askForTwitterAccounts:nil];
                NSLog(@"HELAAS %@",error);
            }
        }];
    }
    else {
        NSLog(@"NCTwitterIntegration: no account");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NoAccount" object:self];
        });
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:TWITTER_ALERT_RENEW_TITLE]) {
        [self.accountStore renewCredentialsForAccount:self.loggedInAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ErrorAlertDismissed" object:nil];
        }];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ErrorAlertDismissed" object:nil];
    }
}

- (void)fetchActivityUpdates:(NSArray *)tweets withBlock:(FetchReturnBlock)block
{
    NSURL *mentionsURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/mentions_timeline.json?trim_user=true&count=100"];
    SLRequest *mentionsRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:mentionsURL parameters:0];
    mentionsRequest.account = self.loggedInAccount;
    
    [mentionsRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"TWITTER API CALL");
        if (!error) {
            NSDictionary *returnedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
           // NSLog(@"%@", returnedJSON);
            NSArray *jsonArray = @[returnedJSON, @"Mentions"];
            block(jsonArray, error);
        }
    }];

    
    for (NCHeadline *h in tweets) {
        NSLog(@"twitter id NCTwitterInt: %@", h.twitterId);
        if (h.twitterId) {
            NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/show.json?id=%@&trim_user=true", h.twitterId];
            NSURL *twitterRequestURL = [NSURL URLWithString:urlString];
            SLRequest *statRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                             requestMethod:SLRequestMethodGET
                                                                       URL:twitterRequestURL
                                                                parameters:0];
            
            statRequest.account = self.loggedInAccount;
            [statRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                NSLog(@"TWITTER API CALL");
                if (!error) {
                    NSDictionary *returnedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
                   // NSLog(@"twitter stats json: %@", returnedJSON);
                    NSArray *jsonArray = @[returnedJSON, h];
                    block(jsonArray, error);
                }
                else {
                    NSLog(@"HELAAS %@",error);
                }
             }];
        }
    }
}

- (void)fetchActivityUpdatesAlternatively:(NSString *)tweets withBlock:(FetchReturnBlock)block;
{

    NSDictionary *twitParameters = @{ @"id" : tweets };
    NSLog(@"%@",tweets);
    NSURL *twitterRequestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/lookup.json"];
    
    SLRequest *postTweetRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                     requestMethod:SLRequestMethodGET
                                                               URL:twitterRequestURL
                                                        parameters:twitParameters];
    

    postTweetRequest.account = self.loggedInAccount;
    [postTweetRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (!error) {
            NSArray *returnedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            block(returnedJSON, error);
        }
        else {
            NSLog(@"HELAAS %@",error);
        }
    }];

}

- (void)askForTwitterAccounts:(FetchReturnBlock)block
{
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted && !error) {
            self.accessGranted = YES;
            NSArray *accountsFromStore = [self.accountStore accountsWithAccountType:accountType];
            block(accountsFromStore, error);
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_NO_ACCOUNT_TITLE message:ALERT_NO_ACCOUNT_ACCESS_MESSAGE delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [alert show];
                });
            //block(nil, error);
        }
    }];

}





@end
