//
//  NCFeed.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/30/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Foundation

class NCFeed: NSObject, NSCoding {
    
    let id: Int
    let name: String
    var headlines: [NCHeadline]
    var unseenAmount: Int?
    private var _trendCount = 0
    var trendCount: Int {
        get {
            var count = 0
            for headline in self.headlines {
                if headline.isSeen == false && headline.trends.count > 0 {
                    count += 1
                }
            }
            return count
        }
        set {
            _trendCount = newValue
        }
    }
    
    
    override var hash: Int {
        get {
            return self.name.hash &* self.id
        }
    }
    
    init(id: Int, name: String) {
        
        self.id = id
        self.name = name
        self.unseenAmount = 0
        self.headlines = []
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "feedId")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.headlines, forKey: "headlines")
        aCoder.encode(self.trendCount, forKey: "trendCount")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else { return nil }
        let id = aDecoder.decodeInteger(forKey: "feedId")

        self.init(id: id, name: name)
        let savedHeadlines = aDecoder.decodeObject(forKey: "headlines") as? [NCHeadline]
        self.trendCount = aDecoder.decodeInteger(forKey: "trendCount")
        self.headlines = savedHeadlines != nil ? savedHeadlines! : []

    }

}
