//
//  LogInViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/15/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Crashlytics
import SCLAlertView
import MessageUI

class LogInViewController: UIViewController, UITextFieldDelegate /*, MFMailComposeViewControllerDelegate */{

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var requestViewToBottomGuideConstraint: NSLayoutConstraint!
    var keyboardSlideDuration: NSNumber?
    var keyboardSlideCurve: NSNumber?
    var keyboardFrame: CGRect?
    var ncLogoImageView: UIImageView?
    @IBOutlet weak var forgotCredentialsButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        let parallaxMagnitude: CGFloat = 40.0
        
        let backgroundImage = UIImageView(frame: self.view.frame)
        backgroundImage.frame = backgroundImage.frame.insetBy(dx: -parallaxMagnitude, dy: 0)
        backgroundImage.center = CGPoint(x: self.view.center.x+40, y: self.view.center.y-40)
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        
        let overlayMainBackgroundImage = CAGradientLayer()
        overlayMainBackgroundImage.backgroundColor = UIColor.orange.cgColor
        overlayMainBackgroundImage.opacity = 0.6
        overlayMainBackgroundImage.frame = backgroundImage.frame
        
        ncLogoImageView = UIImageView(image: UIImage(named: "nc_logo.png"))
        self.view.addSubview(ncLogoImageView!)
        ncLogoImageView?.center = CGPoint(x: self.view.center.x, y: 150)
        
        self.view.backgroundColor = UIColor.white
        self.view.gradientBackground(from: UIColor.darkGradient(), to: UIColor.lightGradient())
        
        let textBackgroundAlpha: CGFloat = 0.98
        self.usernameField.superview?.backgroundColor = UIColor(white: 1.0, alpha: textBackgroundAlpha)
        self.passwordField.superview?.backgroundColor = UIColor(white: 1.0, alpha: textBackgroundAlpha)
        self.usernameField.delegate = self
        self.passwordField.delegate = self
        
        let inputFieldSeparator = UIView(frame: CGRect(x: 20, y: self.usernameField.superview!.frame.size.height-1, width: self.usernameField.superview!.frame.size.width, height: 1))
        inputFieldSeparator.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        self.usernameField.superview?.addSubview(inputFieldSeparator)
        
        let backgroundImageParallax = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        backgroundImageParallax.minimumRelativeValue = -parallaxMagnitude/2
        backgroundImageParallax.maximumRelativeValue = parallaxMagnitude/2
        backgroundImage.addMotionEffect(backgroundImageParallax)
        
        self.logInButton.backgroundColor = UIColor.clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            if let keyboardSlideDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber),
                let keyboardSlideCurve = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber),
                let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                
                self.keyboardSlideDuration = keyboardSlideDuration
                self.keyboardSlideCurve = keyboardSlideCurve
                self.keyboardFrame = keyboardFrame
                
                UIView.animate(withDuration: TimeInterval(keyboardSlideDuration), delay: 0.1, options: .allowAnimatedContent, animations: { () -> Void in
                    self.requestViewToBottomGuideConstraint.constant = self.keyboardFrame!.size.height
                    self.view.layoutIfNeeded()
                    let ncLogoCenterY = (self.view.frame.height - self.keyboardFrame!.height - self.usernameField.superview!.frame.height*2 - self.logInButton.frame.height + 20) / 2
                    self.ncLogoImageView!.center = CGPoint(x: self.ncLogoImageView!.center.x, y: ncLogoCenterY-40);
                    let transformValue: CGFloat = self.view.frame.size.height > 500 ? 0.5 : 0.3;
                    self.ncLogoImageView!.transform = CGAffineTransform(scaleX: transformValue, y: transformValue)
                })
            }
        }
//        
//        keyboardSlideDuration = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as AnyObject)//  CGFloat(((notification as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey]! as AnyObject).floatValue)
//        keyboardSlideCurve = ((notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey] as AnyObject).intValue
//        keyboardFrame = ((notification as NSNotification).userInfo![UIKeyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue
//        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        let touch = touches.first
        if (touch!.view == self.view) {self.view.endEditing(true)}
    }
    
    func keyboardWillHide(_ notification: Notification) {
        
        if keyboardSlideDuration != nil {
            UIView.animate(withDuration: Double(keyboardSlideDuration!), delay: 0, options: .allowAnimatedContent, animations: { () -> Void in
                self.requestViewToBottomGuideConstraint.constant = 0
                self.ncLogoImageView?.center = CGPoint(x: self.view.center.x, y: 150);
                self.ncLogoImageView?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.view.layoutIfNeeded()
                }) { (finished: Bool) -> Void in }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonPressed(_ sender: AnyObject) {
        
        self.logInButton.isUserInteractionEnabled = false
        self.loginUser()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.usernameField) {self.passwordField.becomeFirstResponder()}
        else if (textField == self.passwordField) {self.loginUser()}
        return true
    }
    
    func loginUser() {
        
        for c in HTTPCookieStorage.shared.cookies! {
            HTTPCookieStorage.shared.deleteCookie(c)
        }
        self.view.endEditing(true)
        
        let username = self.usernameField.text!
        let password = self.passwordField.text!
        
        if (username.characters.count > 0 && password.characters.count > 0) && (username.contains(" ") == false) && (password.contains(" ") == false) {
            
            let credentials = [username, password]
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
  
            NCDataStoreSwift.sharedStore.loginAtNineConnections(credentials, withBlock: { (user: NCUser?, error: Error?, response: HTTPURLResponse?) -> Void in
                
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.logInButton.isUserInteractionEnabled = true
                    if (user != nil && error == nil) {
                        var feedVC: FeedListViewController?
                        switch (self.presentingViewController!.childViewControllers.count) {
                        case 1:
                            feedVC = self.presentingViewController?.childViewControllers[0] as? FeedListViewController
                            break
                        case 2:
                            let mainVC = self.presentingViewController?.childViewControllers[1] as? MainViewController
                            mainVC?.cleanUpViews()
                            break
                        default:
                            break
                        }
                        //let navVC = self.presentingViewController as? UINavigationController
                        //navVC?.popToRootViewController(animated: false)
                        feedVC?.company = user?.company
                        feedVC?.refreshFeeds()
                        self.dismiss(animated: true, completion: nil)
                    } else if response?.statusCode == 400 {
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            let alert = UIAlertController(title: NSLocalizedString("INCORRECT_CREDENTIALS_TITLE", comment: ""), message: NSLocalizedString("INCORRECT_CREDENTIALS_MESSAGE", comment: ""), preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alert.addAction(action)
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else if (error as? NSError)?.code == -1009 {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let alert = UIAlertController(title: NSLocalizedString("NO_INTERNET_ERROR_TITLE", comment: ""), message: NSLocalizedString("NO_INTERNET_ERROR_MESSAGE", comment: ""), preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alert.addAction(action)
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let alert = UIAlertController(title: NSLocalizedString("LOGGING_IN_ERROR_TITLE", comment: ""), message: NSLocalizedString("LOGGING_IN_ERROR_MESSAGE", comment: ""), preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alert.addAction(action)
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                }
            })
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.logInButton.isUserInteractionEnabled = true
                let alert = UIAlertController(title: NSLocalizedString("INCORRECT_CREDENTIALS_TITLE", comment: ""), message: NSLocalizedString("INCORRECT_CREDENTIALS_MESSAGE", comment: ""), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            })
        }

    }
    
    @IBAction func pressedForgotCreds(_ sender: AnyObject) {
        
        /*let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "ProximaNova-Semibold", size: 20)!,
            kTextFont: UIFont(name: "ProximaNova-Regular", size: 15)!,
            kButtonFont: UIFont(name: "ProximaNova-Semibold", size: 14)!,
            kWindowWidth: 290,
            kWindowHeight: 250,
            showCloseButton: false,
            showCircularIcon: true
        )*/
        
        let appearanceAlert = SCLAlertView.SCLAppearance(kDefaultShadowOpacity: 0.7, kCircleTopPosition: -12.0, kCircleBackgroundTopPosition: -15.0, kCircleHeight: 56.0, kCircleIconHeight: 20.0, kTitleTop: 30.0, kTitleHeight:25.0, kWindowWidth: 290.0, kWindowHeight: 250.0, kTextHeight: 90.0, kTextFieldHeight: 45.0, kTextViewdHeight: 80.0, kButtonHeight: 40.0, kTitleFont: UIFont(name: "ProximaNova-Semibold", size: 20)!, kTextFont: UIFont(name: "ProximaNova-Regular", size: 15)!, kButtonFont: UIFont(name: "ProximaNova-Semibold", size: 14)!, showCloseButton: false, showCircularIcon: true, shouldAutoDismiss: true, contentViewCornerRadius: 5.0, fieldCornerRadius: 3.0, buttonCornerRadius: 3.0, hideWhenBackgroundViewIsTapped: false, contentViewColor: UIColor("#FFFFFF"), contentViewBorderColor: UIColor("#CCCCCC"), titleColor: UIColor("#4D4D4D"))
        
        let appearanceFailed = SCLAlertView.SCLAppearance(kDefaultShadowOpacity: 0.7, kCircleTopPosition: -12.0, kCircleBackgroundTopPosition: -15.0, kCircleHeight: 56.0, kCircleIconHeight: 20.0, kTitleTop: 30.0, kTitleHeight:35.0, kWindowWidth: 290.0, kWindowHeight: 120.0, kTextHeight: 100.0, kTextFieldHeight: 45.0, kTextViewdHeight: 80.0, kButtonHeight: 25.0, kTitleFont: UIFont(name: "ProximaNova-Semibold", size: 20)!, kTextFont: UIFont(name: "ProximaNova-Regular", size: 15)!, kButtonFont: UIFont(name: "ProximaNova-Semibold", size: 14)!, showCloseButton: false, showCircularIcon: true, shouldAutoDismiss: true, contentViewCornerRadius: 5.0, fieldCornerRadius: 3.0, buttonCornerRadius: 3.0, hideWhenBackgroundViewIsTapped: false, contentViewColor: UIColor("#FFFFFF"), contentViewBorderColor: UIColor("#CCCCCC"), titleColor: UIColor("#4D4D4D"))
        
        let alertView = SCLAlertView(appearance: appearanceAlert)
        let feedbackAV = SCLAlertView(appearance: appearanceFailed)
        
        let email = alertView.addTextField(NSLocalizedString("EMAIL_TEXT", comment: ""))
        email.keyboardType = .emailAddress
        email.autocapitalizationType = .none
        
        alertView.addButton(NSLocalizedString("REQUEST_CREDENTIALS_TEXT", comment: "")) {
//            
//            
//            let subject = "Credentials Request"
//            let message = "Username/E-mail: \(username.text!)"
            
            if !email.text!.isEmpty {
                alertView.hideView()
                
                let encodedEmail = email.text!                
                guard let url = NSURL(string: encodedEmail)!.encodedStringFromURL() else {
                    return
                }
                
                let emailString = "https://api.nineconnections.com/password-reset/start?email=\(url)"
                
                var request = URLRequest(url: URL(string: emailString)!)
                request.httpMethod = "GET"
                
                let credentialRequestTask = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                    
                    if let httpResponse = response as? HTTPURLResponse , (httpResponse.statusCode == 200) {
                        
                        feedbackAV.addButton(NSLocalizedString("OK_TEXT", comment: ""), action: {
                            feedbackAV.hideView()
                        })
                        DispatchQueue.main.async {
                            feedbackAV.showInfo(NSLocalizedString("CREDENTIAL_REQUEST_RECEIVED_TITLE", comment: ""), subTitle: NSLocalizedString("CREDENTIAL_REQUEST_RECEIVED_MESSAGE", comment: ""), closeButtonTitle: NSLocalizedString("CANCEL_TEXT", comment: ""), duration: 400, colorStyle: 0x2E3A4A, colorTextButton: 0xFFFFFF, circleIconImage: UIImage(named: "nc_logo.png"), animationStyle: .topToBottom)
                        }
                    } else {
                        feedbackAV.addButton(NSLocalizedString("OK_TEXT", comment: ""), action: {
                            feedbackAV.hideView()
                        })
                        DispatchQueue.main.async {
                            feedbackAV.showInfo(NSLocalizedString("ERROR_PROCESSING_REQUEST_TITLE", comment: ""), subTitle: NSLocalizedString("ERROR_PROCESSING_REQUEST_MESSAGE", comment: ""), closeButtonTitle: NSLocalizedString("CANCEL_TEXT", comment: ""), duration: 400, colorStyle: 0x2E3A4A, colorTextButton: 0xFFFFFF, circleIconImage: UIImage(named: "nc_logo.png"), animationStyle: .topToBottom)
                        }
                    }
                }
                credentialRequestTask.resume()

            } else {
                
                feedbackAV.addButton(NSLocalizedString("OK_TEXT", comment: ""), action: {
                    feedbackAV.hideView()
                })
                DispatchQueue.main.async {

                    feedbackAV.showInfo(NSLocalizedString("MISSING_INFORMATION_TITLE", comment: ""), subTitle: NSLocalizedString("MISSING_INFORMATION_MESSAGE", comment: ""), closeButtonTitle: NSLocalizedString("CANCEL_TEXT", comment: ""), duration: 400, colorStyle: 0x2E3A4A, colorTextButton: 0xFFFFFF, circleIconImage: UIImage(named: "nc_logo.png"), animationStyle: .topToBottom)
                }
            }
        }
        
        alertView.addButton(NSLocalizedString("CANCEL_TEXT", comment: ""), backgroundColor: UIColor.clear, textColor: UIColor("#2E3A4A"), showDurationStatus: false) {
            alertView.hideView()
        }
        
        alertView.showInfo(NSLocalizedString("FORGOT_CREDENTIALS_TITLE", comment: ""), subTitle: NSLocalizedString("FORGOT_CREDENTIALS_MESSAGE", comment: ""), closeButtonTitle: NSLocalizedString("CANCEL_TEXT", comment: ""), duration: 400, colorStyle: 0x2E3A4A, colorTextButton: 0xFFFFFF, circleIconImage: UIImage(named: "nc_logo.png"), animationStyle: .bottomToTop)
        
    }
    
    @IBAction func problemSigningInPressed(_ sender: AnyObject) {
        
        Smooch.show()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}
