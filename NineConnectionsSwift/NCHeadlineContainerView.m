//
//  HeadlineContainerView.m
//  NineConnections
//
//  Created by Floris van der Grinten on 14-05-14.
//
//

#import "NCHeadlineContainerView.h"
#import "UIColor+CustomColors.h"


@implementation NCHeadlineContainerView {
    CGFloat windowWidth;
    CGFloat currentTransformFactor;
    
    CGFloat headlineContainerViewHeight;
    
    CGPoint twitterButtonPosition;
    CGPoint linkedInButtonPosition;
    
}

- (instancetype)initWithHeadline:(NCHeadline *)headline width:(CGFloat)width
{
    self = [super init];
    if (self) {
        windowWidth = width;
        self.headline = headline;
        headlineContainerViewHeight = [NCHeadlineContainerView headlineContainerHeight];
        
        currentTransformFactor = 1.0;
        self.frame = CGRectMake(0, 0, windowWidth, headlineContainerViewHeight);
        
        CGPoint bottomCenter = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMidY(self.frame));
        self.layer.anchorPoint = CGPointMake(1.0,0.5);
        self.layer.position = bottomCenter;
        
        //self.backgroundColor = [UIColor mainBlueColor];
        
        self.gradient = [CAGradientLayer layer];
        self.gradient.colors = @[ (id)[UIColor lightGradientColor].CGColor, (id)[UIColor darkGradientColor].CGColor ];
        self.gradient.startPoint = CGPointMake(0, 0);
        self.gradient.endPoint = CGPointMake(0, 1.0);
        self.gradient.frame = self.frame;
        [self.layer addSublayer:self.gradient];
        
        //self.backgroundColor = [UICoxlor colorWithRed:0 green:0 blue:0 alpha:0.2];
        [self addContainerContent];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, self.window.screen.scale);
    //        [self drawViewHierarchyInRect:self.frame afterScreenUpdates:YES];
            [self.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.blurredImage = [snapshotImage applyBlurWithRadius:6.0 tintColor:nil saturationDeltaFactor:1.0 maskImage:nil];
        });

        self.clipsToBounds = YES;
        
    }
    return self;
}

//- (UIImage *)blurredImage
//{
//    if (!_blurredImage) {
//        UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, self.window.screen.scale);
//        [self drawViewHierarchyInRect:self.frame afterScreenUpdates:YES];
//        UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
//        
//        UIGraphicsEndImageContext();
//        self.blurredImage = [snapshotImage applyBlurWithRadius:6.0 tintColor:nil saturationDeltaFactor:1.0 maskImage:nil];
//    }
//    return _blurredImage;
//}


- (void)addContainerContent
{
    self.titleView = [UITextView new];
    self.titleView.scrollEnabled = NO;

    self.titleView.backgroundColor = [UIColor clearColor];
    //self.titleView = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    self.titleView.textColor = [UIColor whiteColor];
    //self.titleView = [UIColor blackColor];
    //self.titleView.editable = NO;
    //self.titleView.selectable = NO;
    self.titleView.userInteractionEnabled = NO;
    self.titleView.font = [UIFont fontWithName:@"Lato-Light" size:30];
    CGFloat titleViewMargin = 28;
    self.titleView.frame = CGRectMake(titleViewMargin, 0, self.frame.size.width-titleViewMargin*2, headlineContainerViewHeight);
    self.titleView.text = self.headline.title;
    [self.titleView sizeToFit];
    
    if (self.titleView.frame.size.height > headlineContainerViewHeight-60) {
        NSLog(@"%f en %f", self.titleView.frame.size.height, headlineContainerViewHeight);
        self.titleView.font = [UIFont fontWithName:@"Lato-Light" size:25.0];
        [self.titleView sizeToFit];
        
        NSLog(@"%f en %f", self.titleView.frame.size.height, self.frame.size.height);
    }
    
    self.titleView.center = CGPointMake(self.titleView.center.x, CGRectGetMidY(self.frame)-22);
    
    CGFloat yMarginURLBar = 7;
    UIView *URLBar = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.titleView.frame)+yMarginURLBar, windowWidth, 34)];
    URLBar.backgroundColor = [UIColor whiteColor];
    URLBar.layer.cornerRadius = 4;
    
    self.URLButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.URLButton.frame = CGRectMake(12, -3, 0, 0);
    NSString *optionalQuery = self.headline.articleURL.query ? [NSString stringWithFormat:@"?%@", self.headline.articleURL.query] : @"";
    NSString *hostWithoutWWW = [self.headline.articleURL.host stringByReplacingOccurrencesOfString:@"www." withString:@""];
    [self.URLButton setTitle:[NSString stringWithFormat:@"%@%@%@", hostWithoutWWW, self.headline.articleURL.path, optionalQuery] forState:UIControlStateNormal];
    self.URLButton.titleLabel.font = [UIFont fontWithName:@"Merriweather-Italic" size:22];
    [self.URLButton setTintColor:[UIColor mainBlueColor]];
    [self.URLButton sizeToFit];
    self.URLButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.URLButton.frame = CGRectMake(CGRectGetMinX(self.URLButton.frame), CGRectGetMinY(self.URLButton.frame), windowWidth-60, CGRectGetHeight(self.URLButton.frame));
    self.URLButton.clipsToBounds = YES;
    
    [URLBar sizeToFit];
    
    //self.backgroundImage = [[UIImageView alloc] initWithFrame:self.frame];
    //self.backgroundImage.image = self.headline.articleImage;
    //self.backgroundImage.alpha = 0.58;
    //self.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;


    //[self addSubview:self.backgroundImage];
    [self addSubview:self.titleView];
    [self addSubview:URLBar];
    [URLBar addSubview:self.URLButton];
}

- (void)showFullWebPage:(id)sender
{
    //NSLog(@"%li", gesture.);
    self.gradient.opacity = 1.0;
}

- (void)highlightView:(id)sender
{
    NSLog(@"%@", sender);
    self.gradient.opacity = 0.8;
}

+ (CGFloat)headlineContainerHeight
{
//    CGFloat headlineContainerViewHeight = 384.0;
    
    CGFloat headlineContainerViewHeight;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

    
    if (screenHeight < 568.0) {
        headlineContainerViewHeight = 290.0;
    }
    else if (screenHeight < 667.0) {
        headlineContainerViewHeight = 374.0;
       // headlineContainerViewHeight = 384.0;
    }
    else if (screenHeight < 736.0) {
        headlineContainerViewHeight = 440.0;
    }
    else {
        headlineContainerViewHeight = 490.0;
    }
    return headlineContainerViewHeight;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    self.gradient.opacity = 0.9;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    self.gradient.opacity = 1.0;
    self.gestureBlock();
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    self.gradient.opacity = 1.0;
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    self.gradient.opacity = 1.0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
