//
//  ArticleViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/14/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Analytics

class ArticleViewController: UIViewController, UITextViewDelegate {

    var backgroundSnapshot: UIImage?
    var currentHeadline: NCHeadline?
    var firstParagraphCharCount = 0
    var articleString = NSMutableAttributedString()
    var firstParagraph = NSMutableAttributedString()
    @IBOutlet weak var articleTextView: UITextView!
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "Simulator"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.articleTextView.delegate = self
        self.articleTextView.textContainerInset = UIEdgeInsetsMake(15, 12, 32, 12)

    }
    
    @IBAction func dismissViewController(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.presentedViewController == nil) {
            SEGAnalytics.shared().track(SEG_Full_Article_Viewed, properties: ["Article" : self.currentHeadline!.title, "ArticleID" : self.currentHeadline!.id])
            
            self.view.backgroundColor = UIColor.white
                        
            var textWithStrippedImages = (self.currentHeadline!.articleText?.replacingOccurrences(of: "<img[^>]*>", with: "", options: [.caseInsensitive, .regularExpression], range: self.currentHeadline!.articleText!.startIndex..<self.currentHeadline!.articleText!.endIndex))
            textWithStrippedImages = self.divideTextToParts(textWithStrippedImages!)
            //textWithStrippedImages = textWithStrippedImages?.stringByReplacingOccurrencesOfString("\n", withString: "<p>")
            let stringData = textWithStrippedImages!.data(using: String.Encoding.utf8)
            let options: [String:Any] = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue]
            
            let titleWithLinkString = "<a href='\(self.currentHeadline!.url!)'>\(self.currentHeadline!.title)</a> <br/><br/>"
            let titleStringData = titleWithLinkString.data(using: .utf8)
            
            do {
                let decodedString = try NSMutableAttributedString(data: stringData!, options: options, documentAttributes: nil)
                let titleString = try NSMutableAttributedString(data: titleStringData!, options: options, documentAttributes: nil)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 2
                
                self.articleTextView.linkTextAttributes = [NSForegroundColorAttributeName: UIColor("#33353A")]
                decodedString.insert(titleString, at: 0)
                decodedString.addAttributes([NSForegroundColorAttributeName : UIColor("#33353A"), NSFontAttributeName : UIFont(name: "ProximaNova-Light", size: oldIphone ? 19.0 : 21.0)!, NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, decodedString.length))
                decodedString.addAttributes([NSForegroundColorAttributeName : UIColor("#33353A"), NSFontAttributeName : UIFont(name: "ProximaNova-Semibold", size: oldIphone ? 19.0 : 21.0)!, NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, firstParagraph.length + titleString.length))
                    decodedString.addAttributes([NSForegroundColorAttributeName : UIColor("#33353A"), NSFontAttributeName : UIFont(name: "ProximaNova-Bold", size: oldIphone ? 32.0 : 35.0)!], range: NSMakeRange(0, titleString.length))
                decodedString.removeAttribute(NSUnderlineStyleAttributeName, range: NSMakeRange(0, decodedString.length))
                
                DispatchQueue.main.async(execute: {
                    self.articleTextView.attributedText = decodedString
                    self.navigationController?.navigationBar.barTintColor = UIColor.red
                })
            } catch {
                print(error.localizedDescription)
            }
        }
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        self.showWebPage(URL as AnyObject)
        return false
    }
    
    @IBAction func showWebPage(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "ShowWebPageFromURL", sender: sender is URL ? sender : self.currentHeadline?.url)
    }
    
    func divideTextToParts(_ textToDivide: String) -> String {
        
        var foundNumber = false
        var regularEx: NSRegularExpression
        let textCheck = textToDivide as NSString
        do {
            let regex: NSRegularExpression = try NSRegularExpression(pattern: "\\s?(\\b\\d{1}\\.)\\s", options: .caseInsensitive)
            regularEx = regex
            let results = regex.matches(in: textToDivide, options: [], range: NSMakeRange(0, textCheck.length))
            
            if results.count > 2 {
                foundNumber = true
            }
            
            if textToDivide.contains("\n") {
                return textToDivide.replacingOccurrences(of: "\n", with: "<p>")
            } else if foundNumber == true {
                return regularEx.stringByReplacingMatches(in: textToDivide, options: [], range: NSMakeRange(0, textCheck.length), withTemplate: "<p><p>$1<p>")
                //return textToDivide.stringByReplacingOccurrencesOfString("\\s?(\\d\\.)\\s", withString: "<p>$1<p>")
            } else {
                
                var substrings: [String] = []
                substrings.append("<p><p>")
                var substringsCharCount = 0
                var isFirstParagraph = true
                let range = textToDivide.startIndex ..< textToDivide.endIndex

                textToDivide.enumerateSubstrings(in: range, options: .bySentences) { (substring, substringRange, enclosingRange, stop) in
                    
                    substrings.append(substring!)
                    
                    if isFirstParagraph { self.firstParagraph.append(NSMutableAttributedString(string: substring!)) }
                    
                    substringsCharCount = substringsCharCount + substring!.characters.count
                    
                    if substringsCharCount > 300 {
                        isFirstParagraph = false
                        substrings.append("<p><p>")
                        substringsCharCount = 0
                    }
                }
                
                if results.count > 5 {
                    for i in 0..<5 {
                        firstParagraph.append(NSMutableAttributedString(string: substrings[i]))
                    }
                }
                
                return substrings.joined(separator: "")
            }
            
        } catch {
            
            print(error)
        }
        
        return ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowWebPageFromURL" {
            if let webVC = segue.destination as? WebViewController {
                if let url = sender as? URL {
                    webVC.alternateURL = url
                    webVC.pushedFromACVC = false
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

