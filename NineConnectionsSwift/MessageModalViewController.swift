//
//  MessageModalViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/14/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

class MessageModalViewController: UIViewController, UITextViewDelegate {

    typealias SuccessfullySharedBlock = () -> ()
    var successfullySharedBlock: SuccessfullySharedBlock?
    var messageCancelled = false
    var instant = false
    var headline: NCHeadline?
    var feed: NCFeed?
    var network: SocialNetwork?
    var postMadeItsWayToTheInternet = false
    var viewIsDismissed = false
    var bottomCenter: CGPoint?
    var networkError: Error?
    var serverResponse: HTTPURLResponse?
    
    @IBOutlet weak var messageBox: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var postInstantlyButton: UIButton!
    @IBOutlet weak var postWithSchedule: UIButton!
    @IBOutlet weak var characterCountLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.messageBox.layer.cornerRadius = 3
        self.textView.text = self.headline?.title
        self.textView.delegate = self
        
        var buttonColor = UIColor()
        var buttonText = ""
        
        switch (self.network!) {
        case .twitter:
            buttonColor = UIColor.twitter()
            buttonText = "Tweet"
            self.characterCountLabel.text = "\(116 - self.textView.text.characters.count)"
            break
        case .linkedIn:
            buttonColor = UIColor.linkedIn()
            buttonText = NSLocalizedString("SHARE_TEXT", comment: "")
            self.characterCountLabel.text = ""
            break
        default:
            break
        }
        self.postInstantlyButton.setTitle("\(buttonText) \(NSLocalizedString("NOW_TEXT", comment: ""))", for: UIControlState())
        self.postInstantlyButton.backgroundColor = buttonColor
        self.postWithSchedule.setTitle("\(buttonText) \(NSLocalizedString("SMART_TEXT", comment: ""))", for: UIControlState())
        self.postWithSchedule.backgroundColor = buttonColor
    }
    
    func prepareForDismissal(_ notification: Notification?) {
        
        self.dismiss(animated: true) { () -> Void in
            
            self.validatePost(error: self.networkError, response: self.serverResponse)
            self.networkError = nil
            self.serverResponse = nil
        }
    }
    
    func validatePost(error: Error?, response: HTTPURLResponse?) {
        
        if error == nil && response?.statusCode == 202 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "MessagePosted"), object: nil)
        } else if (error as? NSError)?.code == -1009 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "NoInternet"), object: nil)
        } else {
            if self.network == .twitter {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ServerError"), object: nil)
            } else if self.network == .linkedIn {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "LinkedInError"), object: nil, userInfo: ["message": self.textView.text, "network" : self.network!, "channel" : self.feed!, "immediate" : self.instant])
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textView.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.messageBox.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
    }
    
    @IBAction func postInstantly(_ sender: AnyObject) {
        
        self.textView.resignFirstResponder()
        let msg = self.textView.text
        self.messageCancelled = false
        self.instant = true
        
        DispatchQueue.global(qos: .default).async {
            NCDataStoreSwift.sharedStore.prepareForPosting(msg!, toNetwork: self.network!, fromHeadline: self.headline!, inChannel: self.feed != nil ? String(self.feed!.id) : "Shared from Seen Headlines", immediately: true, withSuccess: { (succeeded: Bool, error: Error?, response: HTTPURLResponse?) in
                
                self.networkError = error
                self.serverResponse = response
                
                DispatchQueue.main.async { () -> Void in
                    self.prepareForDismissal(nil)

                }
            })
        }
    }
    
    @IBAction func postWithSchedule(_ sender: AnyObject) {
        
        self.textView.resignFirstResponder()
        let msg = self.textView.text
        self.messageCancelled = false
        self.instant = false
        
        DispatchQueue.global(qos: .default).async {
            
            NCDataStoreSwift.sharedStore.prepareForPosting(msg!, toNetwork: self.network!, fromHeadline: self.headline!, inChannel: self.feed != nil ? String(self.feed!.id) : "Shared from Seen Headlines", immediately: false, withSuccess: { (succeeded: Bool, error: Error?, response: HTTPURLResponse?) in
                
                self.networkError = error
                self.serverResponse = response
                
                DispatchQueue.main.async { () -> Void in
                    self.prepareForDismissal(nil)
                    
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        let touch = touches.first
        if (touch!.view == self.view) {
            self.view.endEditing(true)
            self.messageCancelled = true
            self.dismiss(animated: true, completion: {})
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let characterCount = textView.text.characters.count
        if self.network == SocialNetwork.twitter {
            if characterCount < 116 {
                characterCountLabel.text = "\(116-characterCount)"
                characterCountLabel.textColor = UIColor.white
            } else {
                characterCountLabel.text = "0"
                characterCountLabel.textColor = UIColor.red
            }
        } else {
            characterCountLabel.text = ""
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
//        self.networkError = nil
//        self.serverResponse = nil
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text.characters.count < 1) {return textView.text.characters.count > 0 ? true : false}
        else if (textView.text.characters.count > (self.network == SocialNetwork.twitter ? 115 : 700)) {return false}
        return true
    }

}
