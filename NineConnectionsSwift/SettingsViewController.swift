//
//  SettingsViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/15/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController{
    
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.createCustomTitle(NSLocalizedString("SETTINGS_TEXT", comment: "").uppercased(), fontSize: oldIphone ? 17 : 20)
        self.view.backgroundColor = UIColor.companySecondaryBackground()
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        
//        DispatchQueue.main.async(execute: {
//            self.navigationController?.navigationBar.barTintColor = NCDataStoreSwift.sharedStore.loggedInUser?.company?.mainColor != nil ?  NCDataStoreSwift.sharedStore.loggedInUser?.company?.mainColor : UIColor.mainBlue()
//            
//        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
