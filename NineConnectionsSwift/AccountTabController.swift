//
//  AccountTabController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/14/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import CCARadialGradientLayer

class AccountTabController: UIViewController {
    
    var backgroundSnapshot: UIImage?
    var remainingBackgroundColor: UIColor?
    var initialTabIndex = 0
    var gradientView: UIView?
    var firstVC: UIViewController?
    var secondVC: UIViewController?
    var thirdVC: UIViewController?
    var childViewControllerFrame: CGRect?
    
    @IBOutlet weak var segmentedTabControl: UISegmentedControl!
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.segmentedTabControl.selectedSegmentIndex = self.initialTabIndex
        self.gradientView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        self.view.backgroundColor = self.remainingBackgroundColor
        self.childViewControllerFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-70)
        self.segmentedTabControl.setTitleTextAttributes([NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 15.0)!], forState: .Normal)
        self.segmentedTabControl.tintColor = UIColor.mainBlueColor()
        
        self.firstVC = self.storyboard?.instantiateViewControllerWithIdentifier("StatsVC")
        self.secondVC = self.storyboard?.instantiateViewControllerWithIdentifier("ConnectVC")
        self.thirdVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsVC")
        self.addChildViewController(self.firstVC!)
        self.addChildViewController(self.secondVC!)
        self.addChildViewController(self.thirdVC!)
        self.firstVC?.view.clipsToBounds = true
        self.secondVC?.view.clipsToBounds = true
        self.thirdVC?.view.clipsToBounds = true
        self.updateChildViewController()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let overlayGradient = CCARadialGradientLayer()
        overlayGradient.colors = [UIColor.clearColor().CGColor, UIColor.blackColor().CGColor]
        overlayGradient.locations = [0,1]
        overlayGradient.gradientOrigin = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)
        overlayGradient.gradientRadius = self.view.frame.size.width
        overlayGradient.frame = self.backgroundView.frame;

        self.backgroundView.backgroundColor = UIColor(patternImage: self.backgroundSnapshot!)
        self.gradientView!.backgroundColor = UIColor.blackColor()
        self.gradientView!.layer.mask = overlayGradient;
        
        self.view.insertSubview(self.gradientView!, aboveSubview: self.backgroundView)
    }
    
    @IBAction func changeTab(sender: AnyObject) {
        self.updateChildViewController()
    }
    
    func updateChildViewController() {
        var newVC = UIViewController()
        
        switch(self.segmentedTabControl.selectedSegmentIndex) {
        case 0:
            newVC = self.firstVC!
            break
        case 1:
            newVC = self.secondVC!
            break
        case 2:
            newVC = self.thirdVC!
            break
        default:
            newVC = self.firstVC!
            break
        }
        
        self.firstVC?.view.removeFromSuperview()
        self.secondVC?.view.removeFromSuperview()
        self.thirdVC?.view.removeFromSuperview()
        
        self.view.addSubview(newVC.view)
        newVC.view.frame = childViewControllerFrame!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        let touch: UITouch = touches.first!
        if (touch.locationInView(touch.view).y < childViewControllerFrame?.size.height) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

}
