//
//  TwitterCommunication.h
//  NineConnections
//
//  Created by Floris van der Grinten on 26-05-14.
//
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <Analytics/Analytics.h>
//#import "SEGAnalytics.h"
#import "NCHeadline.h"

typedef void(^FetchReturnBlock)(NSArray *objects, NSError *error);
typedef void(^PostReturnBlock)(NSString *messageId, NSError *error);

@interface NCTwitterIntegration : NSObject <UIAlertViewDelegate>

@property (nonatomic, assign) BOOL accessGranted;
@property (nonatomic, strong) ACAccount *loggedInAccount;


+ (instancetype)sharedClient;

- (void)fetchActivityUpdatesAlternatively:(NSString *)tweets withBlock:(FetchReturnBlock)block;
- (void)fetchActivityUpdates:(NSArray *)tweets withBlock:(FetchReturnBlock)block;

- (void)askForTwitterAccounts:(FetchReturnBlock)block;
- (ACAccount *)identifyAccount:(NSString *)identifier;

- (void)postTweet:(NSString *)tweet withBlock:(PostReturnBlock)block;


@end
