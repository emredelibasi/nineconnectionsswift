//
//  NCAuthorizationToken.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 6/10/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCAuthorizationToken: NSObject, NSCoding {
    
    let tokenType: String
    let refreshToken: String
    let accessToken:String
    
    init(tokenType: String, refreshToken: String, accessToken:String) {
        
        self.tokenType = tokenType
        self.refreshToken = refreshToken
        self.accessToken = accessToken
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.tokenType, forKey: "tokenType")
        aCoder.encode(self.refreshToken, forKey: "refreshToken")
        aCoder.encode(self.accessToken, forKey: "accessToken")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let tType = aDecoder.decodeObject(forKey: "tokenType") as? String,
        let rToken = aDecoder.decodeObject(forKey: "refreshToken") as? String,
        let aToken = aDecoder.decodeObject(forKey: "accessToken") as? String else {
            return nil
        }
        
        self.init(tokenType: tType, refreshToken: rToken, accessToken:aToken)

    }
}
