//
//  NCUnseenAmountView.h
//  NineConnections
//
//  Created by Floris van der Grinten on 08-09-14.
//
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"

@interface NCUnseenAmountView : UIView

@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, assign, getter = isHighlighted) BOOL highlighted;
@property (nonatomic, strong) UIColor *badgeColor;

- (instancetype)initWithAmount:(NSInteger)amount color:(UIColor*)color fontSize:(CGFloat)size;

@end
