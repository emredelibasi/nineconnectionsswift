//
//  NCFeed.m
//  NineConnections
//
//  Created by Floris van der Grinten on 30-06-14.
//
//

#import "NCFeed.h"


@interface NCFeed ()

@property (nonatomic, assign) NSInteger userId;


@end

@implementation NCFeed

- (instancetype)initWithTitle:(NSString *)title ofType:(FeedType)feedType withFeedId:(NSInteger)feedId userId:(NSInteger)userId url:(NSURL *)url
{
    self = [super init];
    if (self) {
        self.title = title;
        self.feedType = feedType;
        self.userId = userId;
        self.feedId = feedId;
        //self.ncHash = self.title.hash * self.feedId;
        
        if (url) {
            self.URL = url;
        }
        else {
            switch (feedType) {
                case FeedTypeChannel:
                    self.URL = [NSURL URLWithString:
                                //[NSString stringWithFormat:@"http://www.nineconnections.com/api/channel/%zd/feed/shared?maxresults=15&%i", self.feedId, arc4random()]
                                [NSString stringWithFormat:@"http://54.78.39.126:8081/9x/channel/%zd", self.feedId]
                    ];
                    break;
                case FeedTypeNewsroom:
                case FeedTypeTwitterList:
                    self.URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.nineconnections.com/api/feed/timeline/viaUsers?userId=%zd&maxShown=15&groupId=%zd&period=12&prostats=true&content_type=ARTICLE&ln=NL&ln=EN&%i", self.userId, self.feedId, arc4random()]];
                    break;
                default:
                    break;
            }
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeInteger:self.feedType forKey:@"feedType"];
    [aCoder encodeInteger:self.feedId forKey:@"feedId"];
    [aCoder encodeInteger:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.URL forKey:@"URL"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [self initWithTitle:[aDecoder decodeObjectForKey:@"title"]
                        ofType:[aDecoder decodeIntegerForKey:@"feedType"]
                    withFeedId:[aDecoder decodeIntegerForKey:@"feedId"]
                        userId:[aDecoder decodeIntegerForKey:@"userId"]
                           url:[aDecoder decodeObjectForKey:@"URL"]];
    
    return self;
}

- (NSUInteger)hash
{
    return self.title.hash * self.feedId;
}

@end
