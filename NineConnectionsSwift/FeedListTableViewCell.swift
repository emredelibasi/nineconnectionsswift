//
//  FeedListTableViewCell.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 9/16/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class FeedListTableViewCell: UITableViewCell {

    @IBOutlet weak var rowTextLabel: UILabel!
    @IBOutlet weak var unseenArticlesImageView: UIImageView!
    @IBOutlet weak var unseenArticlesView: NCUnseenAmountView!
    @IBOutlet weak var unseenTrendsImageView: UIImageView!
    @IBOutlet weak var unseenTrendsView: NCUnseenAmountView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
