//
//  AccountConnectViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/14/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import NXOAuth2Client
import TOMSMorphingLabel
import JGActionSheet

class AccountConnectViewController: UIViewController, JGActionSheetDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var linkedInButton: NCConnectButton!
    @IBOutlet weak var twitterButton: NCConnectButton!
    var twitterAccount: NCSocialAccount?
    var linkedInAccount: NCSocialAccount?
    var connectButtons: [NCConnectButton]?
    var notification1: Notification?
    var notification2: Notification?
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.companySecondaryBackground()
        self.navigationController?.navigationBar.barTintColor = UIColor.companyInnerNavBar()
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        self.navigationController!.navigationBar.topItem!.title = ""
        self.navigationItem.createCustomTitle(NSLocalizedString("CONNECTED_ACCOUNTS", comment: ""), fontSize: oldIphone ? 17 : 20)
        
        self.twitterButton.addTarget(self, action: #selector(connectButtonPressed(_:)), for: .touchUpInside)
        self.linkedInButton.addTarget(self, action: #selector(connectButtonPressed(_:)), for: .touchUpInside)
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let accs = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
        if accs != nil {
            for account in accs! {
                if account.service == "twitter" {
                    self.twitterAccount = account
                } else {
                    self.linkedInAccount = account
                }
            }
            DispatchQueue.main.async(execute: {
                
                self.updateConnectButtons()
            })
        }
    }
    
    func updateConnectButtons() {
        self.twitterAccount != nil ? self.twitterButton.accountConnected(NSLocalizedString("CONNECTED_TEXT", comment: ""), animated: false) : self.twitterButton.accountDisconnected(withDuration: 0)
        self.linkedInAccount != nil ? self.linkedInButton.accountConnected(NSLocalizedString("CONNECTED_TEXT", comment: ""), animated: false) : self.linkedInButton.accountDisconnected(withDuration: 0)
        self.twitterButton.disconnectLabel.text = self.twitterAccount != nil ? "@\(self.twitterAccount!.displayName)" : ""
        self.linkedInButton.disconnectLabel.text = self.linkedInAccount != nil ? "\(self.linkedInAccount!.displayName)" : ""
    }
    
    func connectButtonPressed(_ sender: NCConnectButton) {
        
        if (!sender.isConnected) {
            switch(sender.buttonType) {
            case SocialNetwork.twitter:
                if NCDataStoreSwift.sharedStore.loggedInUser != nil && NCDataStoreSwift.sharedStore.connectedToNetwork() {
                    self.performSegue(withIdentifier: "ShowAuthPage", sender: URL(string: "\(NCRESTAPI)/oauth/twitter?access_token=\(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken)"))
                } else if NCDataStoreSwift.sharedStore.connectedToNetwork() == false {
                    showNetworkWarning()
                }
                break
            case SocialNetwork.linkedIn:
                if NCDataStoreSwift.sharedStore.loggedInUser != nil && NCDataStoreSwift.sharedStore.connectedToNetwork() {
                    self.performSegue(withIdentifier: "ShowAuthPage", sender: URL(string: "\(NCRESTAPI)/oauth/linkedin?access_token=\(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken)"))
                } else if NCDataStoreSwift.sharedStore.connectedToNetwork() == false {
                    showNetworkWarning()
                }
                break
            default:
                break
            }
        } else {

            sender.accountDisconnected(withDuration: 0.2)
            
            switch (sender.buttonType) {
            case SocialNetwork.twitter:
                NCDataStoreSwift.sharedStore.removeSocialAccountWithService(twitterAccount!.service, withSuccess: { (succeded: Bool, error: Error?) in
                    if succeded {
                        let accounts = NCDataStoreSwift.sharedStore.userAccounts.filter({ (account: NCSocialAccount) -> Bool in
                            account.service != "twitter"
                        })
                        self.twitterAccount = nil
                        NCDataStoreSwift.sharedStore.storeSocialAccountsLocally(accounts)
                    }
                })
                break
            case SocialNetwork.linkedIn:
                
                for c in HTTPCookieStorage.shared.cookies! {
                    HTTPCookieStorage.shared.deleteCookie(c)
                }
                
                NCDataStoreSwift.sharedStore.removeSocialAccountWithService(linkedInAccount!.service, withSuccess: { (succeded: Bool, error: Error?) in
                    if succeded {
                        let accounts = NCDataStoreSwift.sharedStore.userAccounts.filter({ (account: NCSocialAccount) -> Bool in
                            account.service != "linkedin"
                        })
                        self.linkedInAccount = nil
                        NCDataStoreSwift.sharedStore.storeSocialAccountsLocally(accounts)
                    }
                })
                break
            default:
                break
            }
        }
    }
    
    func showNetworkWarning() {
        
        let alert = UIAlertController(title: NSLocalizedString("NETWORK_ERROR_TITLE", comment: ""), message: NSLocalizedString("NETWORK_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showConnectionWarning() {
        
        let alert = UIAlertController(title: NSLocalizedString("CONNECTION_ERROR_TITLE", comment: ""), message: NSLocalizedString("CONNECTION_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowAuthPage") {
            if let webVC = segue.destination as? WebViewController {
                if let url = sender as? URL {
                    webVC.alternateURL = url
                    webVC.pushedFromACVC = true
                    webVC.pushedFromMVC = false
                    webVC.callback = {(accounts: [NCSocialAccount], error: Error?) -> Void in
                        
                        if accounts.count > 0 {
                            for account in accounts {
                                if account.service == "twitter" {
                                    self.twitterAccount = account
                                } else {
                                    self.linkedInAccount = account
                                }
                            }
                            DispatchQueue.main.async(execute: { 
                                self.updateConnectButtons()
                                NCDataStoreSwift.sharedStore.identifyUser()
                            })
                        } else if error != nil {
                            self.showConnectionWarning()
                        }
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

//    func showAccountPickerActionSheet() {
//        
//        var accountButtonTitles = [AnyObject]()
//        if self.accounts != nil {
//            for acc in self.accounts! {
//                accountButtonTitles.append(acc.id.uppercaseString)
//            }
//        }
//    
//        let accountsSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: accountButtonTitles, buttonStyle: JGActionSheetButtonStyle.Default)
//        let cancelSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: ["Cancel".uppercaseString], buttonStyle: JGActionSheetButtonStyle.Cancel)
//        
//        let accountActionSheet = JGActionSheet(sections: [accountsSection, cancelSection])
//        accountActionSheet.delegate = self
//        accountActionSheet.showInView(UIApplication.sharedApplication().keyWindow?.subviews.first, animated: true)
//        accountActionSheet.outsidePressBlock = { (sheet: JGActionSheet!) -> Void in
//            sheet.dismissAnimated(true)
//        }
//    }

//    func actionSheet(actionSheet: JGActionSheet!, pressedButtonAtIndexPath indexPath: NSIndexPath!) {
//        
//        if (indexPath.section == 1) {
//        } else {
//            if let thisAccount = self.accounts[indexPath.row] as? String {
//                NCDataStoreSwift.sharedStore.storeTwitterAccountLocally(thisAccount)
//                //NCDataStoreSwift.sharedStore.authenticateTwitterLogin()
//                self.twitterButton.accountConnected(thisAccount.accountDescription, animated: true)
//            }
//        }
//        actionSheet.dismissAnimated(true)
//    }
    
//    func presentLinkedInAuthViewController(notification: NSNotification) {
//        
//        if (self.presentedViewController == nil) {
//            if let authVC = notification.object as? UINavigationController {
//                self.presentViewController(authVC, animated: true, completion: { () -> Void in
//                })
//            }
//        }
//    }
//    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
//        NSNotificationCenter.defaultCenter().removeObserver(self, name: NXOAuth2AccountStoreAccountsDidChangeNotification, object: NXOAuth2AccountStore.sharedStore())
    }
}
