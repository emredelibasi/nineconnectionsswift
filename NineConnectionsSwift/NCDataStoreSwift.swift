//
//  NCDataStoreSwift.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/8/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import Darwin
import Analytics
import NXOAuth2Client
import Crashlytics
import Alamofire

class NCDataStoreSwift: NSObject, URLSessionDelegate {
    
    static let sharedStore = NCDataStoreSwift()
    typealias FetchSharesBlock = ([NCUserShare], Error?) -> Void
    typealias SuccessBlock = (Bool, Error?) -> Void
    typealias PostReturnBlock = (Bool, Error?, HTTPURLResponse?) -> Void
    typealias UserReturnBlock = (NCUser?, Error?, HTTPURLResponse?) -> Void
    typealias TokenReturnBlock = (NCAuthorizationToken?, Error?, HTTPURLResponse?) -> Void
    typealias FeedsReturnBlock = ([NCFeed], Error?) -> Void
    typealias HeadlinesReturnBlock = ([NCHeadline], Error?) -> Void
    typealias AccountsReturnBlock = ([NCSocialAccount], Error?) -> Void
    typealias StatsReturnBlock = ([NCSocialStat], Error?) -> Void
    
    var headlinesPrivate: [NCHeadline]
    var allStats: [NCSocialStat]
    var allFeeds: [NCFeed]
    var allHeadlines: [NCHeadline]
    var allShares: [NCUserShare]
    var postedHeadlines: [NCHeadline]
    var bookmarkedHeadlines: [NCHeadline]
    var seenHeadlinesByFeed: [Int: [NCHeadline]]
    var linkedInPresenter: UIViewController?
    var itemDateFormatter: DateFormatter
    var loggedInUser: NCUser?
    var userAccounts: [NCSocialAccount]    
    var seqSeenHeadlines: [NCHeadline]?
    var sequentialSeenHeadlines: [NCHeadline] {
        get {
            if (seqSeenHeadlines?.count == 0) {
                for key in self.seenHeadlinesByFeed.keys {
                    seqSeenHeadlines = Array(Set(seqSeenHeadlines!).union(Set(self.seenHeadlinesByFeed[key]!)))
                }
                seqSeenHeadlines?.sort(by: { $0.id > $1.id })
            }
            return seqSeenHeadlines!
        } set {
            seqSeenHeadlines = newValue
        }
    }
    
    override fileprivate init() {
        self.headlinesPrivate = []
        self.allStats = []
        self.allHeadlines = []
        self.allFeeds = []
        self.allShares = []
        self.postedHeadlines = []
        self.bookmarkedHeadlines = []
        self.userAccounts = []
        self.seenHeadlinesByFeed = [Int:[NCHeadline]]()
        self.itemDateFormatter = DateFormatter()
        self.itemDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        super.init()
        self.sequentialSeenHeadlines = [NCHeadline]()
    }
    
    // MARK: Data Fetching Operations
    
    func fetchAllFeedsWithBlock(_ block:@escaping FeedsReturnBlock) {
        
        self.allFeeds.removeAll()
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/channels")!)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
        
        let userChannelInfoTask = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if let httpResponse = response as? HTTPURLResponse , (httpResponse.statusCode == 200) {
                do {
                    if let userFeeds = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[AnyHashable: Any]] {
                        for userFeed in userFeeds {
                            if let feedId = userFeed["id"] as? Int,
                                let feedName = userFeed["name"] as? String {
                                let feed = NCFeed(id: feedId, name: feedName)
                                self.allFeeds.append(feed)
                            }
                        }
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        block(self.allFeeds, error)
                    }
                } catch {
                    block([], error)
                    print("error serializing json")
                }
            } else {
                block([], error)
            }
        }
        userChannelInfoTask.resume()
    }
    
    func fetchNewHeadlinesFromFeed(_ feed: NCFeed, withBlock block: @escaping HeadlinesReturnBlock) {
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/channels/\(feed.id)/items?limit=20")!)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")

        let headlinesTask: URLSessionDataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let httpResponse = response as? HTTPURLResponse , (httpResponse.statusCode == 200) && data != nil {
                do {
                    if let headlines = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[AnyHashable: Any]] {
                        self.headlinesPrivate.removeAll()
                        for fetchedHeadline in headlines {
                            if let headlineId = fetchedHeadline["id"] as? Int,
                            let headlineTitle = fetchedHeadline["title"] as? String,
                            let createdDate = fetchedHeadline["created"] as? String,
                            let headlineURL = fetchedHeadline["url"] as? String , headlineURL.hasPrefix("http") {
                                if URL(string: headlineURL) == nil {break}
                                
                                let headline = NCHeadline(id: headlineId, title: headlineTitle, url: URL(string: headlineURL)!)
                                
                                if let tags = fetchedHeadline["tags"] as? NSString {
                                    let data = tags.data(using: String.Encoding.utf8.rawValue)
                                    if data != nil {
                                        if let serializedTags = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String] {
                                            headline.tags = serializedTags
                                        }
                                    }
                                }
                                
                                if let trends = fetchedHeadline["trends"] as? NSString {
                                    let data = trends.data(using: String.Encoding.utf8.rawValue)
                                    if data != nil {
                                        if let serializedTrends = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String] {
                                            headline.trends = serializedTrends
                                        }
                                    }
                                }
                                
                                headline.date = self.itemDateFormatter.date(from: createdDate)
                                if headline.url != nil {
                                    self.headlinesPrivate.append(headline)
                                    self.allHeadlines.append(headline)
                                }
                                
                                
                            }
                        }
                        
                        var trendCount = 0
                        for headline in self.headlinesPrivate {
                            if headline.trends.count > 0 {
                                trendCount += 1
                            }
                        }
                        feed.trendCount = trendCount
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        block(NCDataStoreSwift.sharedStore.sortHeadlinesById(self.headlinesPrivate), error)
                    }
                } catch { print("error serializing json") }
            }
        })
        headlinesTask.resume()
    }
    
    func fetchFullArticleTextFromHeadline(_ headline: NCHeadline, inFeed feed:NCFeed, withSuccess block:@escaping SuccessBlock) {
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/items/\(headline.id)")!)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
        
        let articleTask: URLSessionDataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let httpResponse = response as? HTTPURLResponse , (httpResponse.statusCode == 200) && data != nil {
                do {
                    if let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String:AnyObject] {
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        if let description = jsonData["description"] as? String {
                            headline.articleText = description
                            block(true, error)
                        } else {
                            block(false, error)
                        }
                    }
                } catch {
                    block(false, error)
                    print("Error getting item description \(error)")
                }
            } else {
                block(false, error)
            }
        })
        articleTask.resume()
    }
    
    func fetchStatsForUser(_ user: NCUser, withBlock block:@escaping StatsReturnBlock) {
        
        self.allStats = []
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/shared/items/stats")!)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
        
        let statsTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let httpResponse = response as? HTTPURLResponse , httpResponse.statusCode == 200 {
                do {
                    if let statsJSONData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[AnyHashable: Any]] {
                        for statData in statsJSONData {
                            if let statId = statData["id"] as? Int,
                                let statTitle = statData["title"] as? String,
                                let statType = statData["type"] as? String,
                                let statAmount = statData["value"] as? Int {
                                
                                var type = InteractionType.bitlyClick
                                
                                switch(statType) {
                                case "bitly click":
                                    type = InteractionType.bitlyClick
                                    break
                                case "twitter favorite":
                                    type = InteractionType.twitterFavorite
                                    break
                                case "twitter retweet":
                                    type = InteractionType.twitterRetweet
                                    break
                                case "twitter reply":
                                    type = InteractionType.twitterReply
                                    break
                                case "linkedin like":
                                    type = InteractionType.linkedInLike
                                    break
                                case "linkedin comment":
                                    type = InteractionType.linkedInComment
                                    break
                                default:
                                    break
                                }
                                
                                let stat = NCSocialStat(id: statId, headlineTitle: statTitle, interaction: type, amount: statAmount)
                                self.allStats.append(stat)
                            }
                        }
                        self.storeSocialStatsLocally(self.allStats)
                        block(self.allStats, error)
                    }
                } catch {
                    block([], error)
                }
            } else {
                block([], error)
            }
        }) 
        statsTask.resume()
    }
    
    func prepareForPosting(_ msg: String, toNetwork network: SocialNetwork, fromHeadline headline: NCHeadline, inChannel channel: String, immediately immediate: Bool, withSuccess success:@escaping PostReturnBlock) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let shareMethod: String
        var shareMessage = msg
        var optionalBitlyURL: String?
        
        if msg.isEmpty {
            shareMessage = headline.title
            shareMethod = SEG_Share_Method_Drag
        } else {
            shareMethod = SEG_Share_Method_Modal
        }
        
        shareMessage = shareMessage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        let urlForBitlink = URL(string: "\(headline.url!)?user_id=\(NCDataStoreSwift.sharedStore.loggedInUser!.id)&utm_source=\(network == SocialNetwork.twitter ? "twitter" : "linkedin")&utm_medium=social&utm_campaign=nine_connections&network=\(network == SocialNetwork.twitter ? "twitter" : "linkedin")")!

        let bitlyApi = "http://api.bit.ly/shorten?longUrl=\((urlForBitlink as NSURL).encodedStringFromURL()!)&login=o_e7jifk6cs&apiKey=R_44be9c9c588f7cf35892096bf7517895&format=text"
        
        do {
            optionalBitlyURL = try String(contentsOf: URL(string: bitlyApi)!, encoding: String.Encoding.utf8)
            
            if (optionalBitlyURL?.hasPrefix("http") == false) {
                optionalBitlyURL = nil //"\(headline.articleURL)"
                print("optional bitly url \(optionalBitlyURL)")
            }
        } catch {
            optionalBitlyURL = nil
            print("error encoding string bitlyApi")
        }
        
        var twitterAccount: NCSocialAccount?
        var linkedInAccount: NCSocialAccount?
        let userAccounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
        if userAccounts != nil {
            for acc in userAccounts! {
                if acc.service == "twitter" {
                    twitterAccount = acc
                } else {
                    linkedInAccount = acc
                }
            }
        }
        
        let finalMsgUrl = optionalBitlyURL != nil ? optionalBitlyURL! : "\(headline.url!)"
        //let fullMessage = "\(tweetMessage.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) \()"
        let body = "title=\(shareMessage.trimmingCharacters(in: CharacterSet.whitespaces))&url=\(finalMsgUrl)"
        
        if network == SocialNetwork.twitter {
            
            var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/items/\(headline.id)/twitter?now=\(immediate)")!)
            request.httpMethod = "POST"
            request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = body.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false

                if let httpResponse = response as? HTTPURLResponse , httpResponse.statusCode == 202 {
                    
                    headline.sharedInstantlyOnTwitter = immediate
                    headline.sharedWithScheduleOnTwitter = !immediate
                    headline.sharedAt = Date()
                    
                    NCDataStoreSwift.sharedStore.saveHeadlinePostLocally(headline)
                    
                    var shareProperties: [String : Any] = ["Network" : "Twitter", SEG_Share_Method_Key : shareMethod, "Article" : headline.title, "ArticleID" : headline.id, "Shared on Channel" : channel, "Shared immediately" : immediate, "Url" : headline.url!, "Username" : twitterAccount != nil ? twitterAccount!.displayName : "", "Date" : Date()]
                    if optionalBitlyURL != nil { shareProperties["Bitlink"] = optionalBitlyURL! }
                    
                    SEGAnalytics.shared().track(SEG_Article_Shared, properties: shareProperties)
                    success(true, error, httpResponse)
                } else {
                    success(false, error, response as? HTTPURLResponse)
                }
            })
            task.resume()
        } else if network == SocialNetwork.linkedIn {
            
            var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/items/\(headline.id)/linkedin?now=\(immediate)")!)
            request.httpMethod = "POST"
            request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = body.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false

                if let httpResponse = response as? HTTPURLResponse , httpResponse.statusCode == 202 {
                    
                    headline.sharedInstantlyOnLinkedIn = immediate
                    headline.sharedWithScheduleOnLinkedIn = !immediate
                    headline.sharedAt = Date()
                    
                    NCDataStoreSwift.sharedStore.saveHeadlinePostLocally(headline)
                    
                    var shareProperties: [String:Any] = ["Network" : "LinkedIn", SEG_Share_Method_Key : shareMethod, "Article" : headline.title, "ArticleID" : headline.id, "Shared on Channel" : channel, "Shared immediately" : immediate, "Url" : headline.url!, "Username" : linkedInAccount != nil ? linkedInAccount!.displayName : "", "Date" : Date()]
                    if optionalBitlyURL != nil { shareProperties["Bitlink"] = optionalBitlyURL! }
                    
                    SEGAnalytics.shared().track(SEG_Article_Shared, properties: shareProperties)
                    success(true, error, httpResponse)
                } else {
                    success(false, error, response as? HTTPURLResponse)
                }
            })
            task.resume()
        }
    }
    
    func sortHeadlinesByDate(_ headlines: [NCHeadline]) -> [NCHeadline] {
        return headlines.sorted(by: { $0.date!.compare($1.date! as Date) == .orderedDescending })
    }
    
    func sortHeadlinesById(_ headlines: [NCHeadline]) -> [NCHeadline] {
        return headlines.sorted(by: { $0.id > $1.id })
    }
    
    // MARK: NC User Authentication
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func extractUserFromToken(_ token: NCAuthorizationToken, withBlock block: UserReturnBlock) {
        
        let splittedToken = token.accessToken.components(separatedBy: ".")
        var base64String = splittedToken[1] as String
        if base64String.characters.count % 4 != 0 {
            let padlen = 4 - base64String.characters.count % 4
            base64String += String(repeating: "=", count: padlen)
        }
        
        if let data = Data(base64Encoded: base64String, options: []) {
            do {
                if let jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [AnyHashable: Any] {
                    if let username = jsonData["username"] as? String,
                        let userId = jsonData["id"] as? Int,
                        let userFullName = jsonData["name"] as? String {
                        let user = NCUser(userId: userId, userName: username, fullName: userFullName, authorizationToken: token)
                        
                        if let company = jsonData["company"] as? [String: AnyObject] {
                            if let companyId = company["id"] as? Int,
                                let companyName = company["name"] as? String,
                                let companyCode = company["code"] as? String {
                                let company = NCCompany(id: companyId, name: companyName, code: companyCode)
                                if company.code == "unit4" {
                                    company.mainNavBarColor = UIColor("#7C878E")
                                    company.innerNavBarColor = UIColor("#7C878E")
                                    company.headerColor = UIColor("#AFC836")
                                    company.rowColor = UIColor.white
                                    company.rowTextColor = UIColor("#7C878E")
                                    company.rowSeparatorColor = UIColor("#E1E1E1")
                                    company.rowSelectedColor = UIColor("#F3F3F3")
                                    company.rowSelectedTextColor = UIColor("#F68D2E")
                                    company.unreadCountColor = UIColor("#F68D2E")
                                    company.unreadCountSelectedColor = UIColor("#F68D2E")
                                    company.contactColor = UIColor.white
                                    company.contactTextColor = UIColor("#7C878E")
                                    company.logoutColor = UIColor("#AFC836")
                                    company.logoutTextColor = UIColor.white
                                    company.backgroundColor = UIColor("#AFC836")
                                    company.secondaryBackgroundColor = UIColor("#626F77")
                                    company.logoUrlString = "http://cdn.nineconnections.com.s3-eu-west-1.amazonaws.com/assets/unit4.png"
                                    company.logo = UIImage(named: "unit4logo.png")
                                } else if company.code == "telegraaf" {
                                    company.logo = UIImage(named: "de_telegraaf_logo_white_2.png")
                                } else if company.code == "volkskrant" {
                                    company.logo = UIImage(named: "volkskrant_logo")
                                } else if company.code == "hartstichting" {
                                    company.mainNavBarColor = UIColor("#DF2025")
                                    company.innerNavBarColor = UIColor("#DF2025")
                                    company.headerColor = UIColor("#333333")
                                    company.rowColor = UIColor.white
                                    company.rowTextColor = UIColor("#333333")
                                    company.rowSelectedColor = UIColor("#F3F3F3")
                                    company.rowSelectedTextColor = UIColor("#333333")
                                    company.unreadCountColor = UIColor("#DF2025")
                                    company.unreadCountSelectedColor = UIColor("#DF2025")
                                    company.contactColor = UIColor("#DF2025")
                                    company.contactTextColor = UIColor.white
                                    company.logoutColor = UIColor("#333333")
                                    company.logoutTextColor = UIColor.white
                                    company.backgroundColor = UIColor("#333333")
                                    company.secondaryBackgroundColor = UIColor("#333333")
                                    company.logo = UIImage(named: "hartstichting_logo")
                                } else if company.code == "nnbank" {
                                    UIApplication.shared.setStatusBarStyle(.default, animated: false)
                                    company.mainNavBarColor = UIColor.white
                                    company.innerNavBarColor = UIColor("#E34D0D")
                                    company.headerColor = UIColor("#E34D0D")
                                    company.rowColor = UIColor.white
                                    company.rowTextColor = UIColor("#3F4D61")
                                    company.rowSelectedColor = UIColor("#F9F9F9")
                                    company.rowSeparatorColor = UIColor("#3F4D61")
                                    company.rowSelectedTextColor = UIColor.white
                                    company.unreadCountColor = UIColor("#3F4D61")
                                    company.unreadCountSelectedColor = UIColor("#3F4D61")
                                    company.contactColor = UIColor.white
                                    company.contactTextColor = UIColor("#E34D0D")
                                    company.logoutColor = UIColor("#E34D0D")
                                    company.logoutTextColor = UIColor.white
                                    company.backgroundColor = UIColor("#E34D0D")
                                    company.secondaryBackgroundColor = UIColor.white
                                    company.logo = UIImage(named: "nnbanklogo")
                                }
                                
                                user.company = company
                                SKTUser.current()?.addProperties([ "company" : companyName, "company-id" : companyId, "premiumUser" : true])
                            }
                        } else {
                            SKTUser.current()?.addProperties([ "company" : "No Company Information", "company-id" : "N/A", "premiumUser" : true])
                        }
                        
                        self.loggedInUser = user
                        self.storeUserLocally(user)
                        UserDefaults.standard.set(self.loggedInUser!.username.lowercased(), forKey: "NineConnections username")
                        self.identifyUser()
                        
                        SKTUser.current()?.firstName = userFullName
                        SKTUser.current()?.lastName = ""
                        SKTUser.current()?.signedUpAt = Date()
                        SKTUser.current()?.addProperties(["user-id" : self.loggedInUser!.id])
                        Smooch.login(String(self.loggedInUser!.id), jwt: nil)
                        
                        block(user, nil, nil)
                    }
                }
                
            } catch {
                block(nil, error, nil)
                print("Error authenticating user: \(error)")
            }
        }
    }
    
    func loginAtNineConnections(_ credentials: [String], withBlock block: @escaping UserReturnBlock) {
        
        let parameters = ["username" : credentials[0], "password" : credentials[1]]
        let requestURL = URL(string: "\(NCRESTAPI)/auth")!
        
        Alamofire.request(requestURL,method: .post, parameters: parameters).response { response in
        
            if response.response?.statusCode == 200, response.data != nil {
                do {
                    if let jsonData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [AnyHashable: Any] {
                        if let tokenType = jsonData["token_type"] as? String,
                            let refreshToken = jsonData["refresh_token"] as? String,
                            let accessToken = jsonData["access_token"] as? String {
                            let returnToken = NCAuthorizationToken(tokenType: tokenType, refreshToken: refreshToken, accessToken: accessToken)

                            self.extractUserFromToken(returnToken, withBlock: { (user: NCUser?, error: Error?, response: HTTPURLResponse?) in
                                self.loggedInUser = user
                                self.storeUserLocally(self.loggedInUser!)

                                NCDataStoreSwift.sharedStore.checkConnectedAccounts(returnToken, withBlock: { (accounts:[NCSocialAccount]!, error: Error?) in
                                    if error == nil {
                                        block(self.loggedInUser, nil, response)
                                    }
                                })
                            })
                        }
                    }
                } catch {
                    block(nil, response.error, response.response)
                }
            } else {
                block(nil, response.error, response.response)
                print("FAILED WITH STATUS CODE \(response.response?.statusCode)")
            }
        }
    }
    
    func checkTokenValidity(_ authorizationToken: NCAuthorizationToken, withBlock block: @escaping TokenReturnBlock) {
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/auth/refresh")!)
        request.httpMethod = "POST"
        let body = "access_token=\(authorizationToken.accessToken)&refresh_token=\(authorizationToken.refreshToken)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        let tokenTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            let httpResponse = response as? HTTPURLResponse
            if httpResponse?.statusCode == 200, data != nil {
                do {
                    if let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [AnyHashable: Any] {
                        if let tokenType = jsonData["token_type"] as? String,
                            let refreshToken = jsonData["refresh_token"] as? String,
                            let accessToken = jsonData["access_token"] as? String {
                            let returnToken = NCAuthorizationToken(tokenType: tokenType, refreshToken: refreshToken, accessToken: accessToken)
                            
                            self.extractUserFromToken(returnToken, withBlock: { (user: NCUser?, error: Error?, response: HTTPURLResponse?) in
                                self.loggedInUser = user
                                self.storeUserLocally(self.loggedInUser!)
                                block(returnToken, error, httpResponse)
                            })
                        }
                    }
                } catch {
                    block(nil, error, httpResponse)
                    print(error.localizedDescription)
                }
            } else {
                block(nil, error, httpResponse)
            }
        }) 
        tokenTask.resume()
        
    }
    
    func checkConnectedAccounts(_ authorizationToken: NCAuthorizationToken, withBlock block:@escaping AccountsReturnBlock) {
        
        var accounts: [NCSocialAccount] = []
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/access")!)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
        
        let accountsTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let httpResponse = response as? HTTPURLResponse , httpResponse.statusCode == 200, data != nil {
                do {
                    if let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[AnyHashable: Any]] {
                        for fetchedAccount in jsonData {
                            if let id = fetchedAccount["id"] as? String,
                                let credentials = fetchedAccount["credentials"] as? [AnyHashable: Any],
                                let displayName = fetchedAccount["display_name"] as? String,
                                let ownerId = fetchedAccount["owner_id"] as? String,
                                let service = fetchedAccount["service"] as? String {
                                let account = NCSocialAccount(id: id, displayName: displayName, credentials: credentials, ownerId: ownerId, service: service)
                                accounts.append(account)
                            }
                        }
                        self.userAccounts = accounts
                        self.storeSocialAccountsLocally(self.userAccounts)
                        block(accounts, error)
                    }
                } catch {
                    block([], error)
                }
            } else {
                block([], error)
            }
        }) 
        accountsTask.resume()
    }
    
    func removeSocialAccountWithService(_ service: String, withSuccess success: @escaping SuccessBlock) {
        
        var request = URLRequest(url: URL(string: "\(NCRESTAPI)/app/access/\(service)")!)
        request.httpMethod = "DELETE"
        request.addValue("Bearer \(self.loggedInUser!.authorizationToken!.accessToken)", forHTTPHeaderField: "Authorization")
        
        let removalTask = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let httpResponse = response as? HTTPURLResponse , httpResponse.statusCode == 204 {
                success(true, error)
            } else {
                success(false, error)
            }
        }) 
        removalTask.resume()
    }
    
    func identifyUser() {
        
        var twitterAccount: NCSocialAccount?
        var linkedInAccount: NCSocialAccount?
        
        for account in self.userAccounts {
            if account.service == "twitter" { twitterAccount = account } else {linkedInAccount = account }
        }

        let username = self.loggedInUser?.username != nil ? self.loggedInUser!.username : UserDefaults.standard.string(forKey: "NineConnections username")
        SEGAnalytics.shared().identify(username, traits: ["Company Name": self.loggedInUser?.company?.name != nil ? self.loggedInUser!.company!.name : "No Company", "Company Id": self.loggedInUser?.company?.id != nil ? "\(self.loggedInUser!.company!.id)" : "", "Twitter account": twitterAccount != nil ? twitterAccount!.displayName : "NoAccount", "LinkedIn account": linkedInAccount != nil ? linkedInAccount!.displayName : "NoAccount", "Twitter followers" : UserDefaults.standard.integer(forKey: "Twitter followers"), "LinkedIn connections" : UserDefaults.standard.integer(forKey: "LinkedIn connections")])//, "$ios_devices" : NSUserDefaults.standardUserDefaults().objectForKey("Push token") != nil ? NSUserDefaults.standardUserDefaults().objectForKey("Push token")! : ""])
        
        Crashlytics.sharedInstance().setUserName(self.loggedInUser?.username != nil ? self.loggedInUser!.username : "")
        Crashlytics.sharedInstance().setUserIdentifier(self.loggedInUser?.id != nil ? String(self.loggedInUser!.id) : "")
        
    }
    
    func logoutAtNineConnections() {
        
        for c:HTTPCookie in HTTPCookieStorage.shared.cookies! {
            HTTPCookieStorage.shared.deleteCookie(c)
        }
        
        for path in [NCDataStoreSwift.headlinesArchivePath(), NCDataStoreSwift.seenHeadlineArchivePath(), NCDataStoreSwift.feedsPath(), NCDataStoreSwift.userArchivePath(), NCDataStoreSwift.accountsArchivePath()] {
            
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                } catch {
                    print("val: \(error)")
                }
            }
        }
        
        self.loggedInUser = nil
        self.userAccounts = []
        self.allStats = []
        self.postedHeadlines.removeAll()
        self.bookmarkedHeadlines.removeAll()
        self.headlinesPrivate.removeAll()
        self.allFeeds.removeAll()
        self.sequentialSeenHeadlines.removeAll()
        self.seenHeadlinesByFeed.removeAll()
        
        UserDefaults.standard.resetAppDefaults()
    }
    
    // MARK: Storage
    
    func storeUserLocally(_ user: NCUser) {
        
        self.loggedInUser = user
        NSKeyedArchiver.setClassName("NCUser", for: NCUser.self)
        NSKeyedArchiver.archiveRootObject(self.loggedInUser!, toFile: NCDataStoreSwift.userArchivePath())
        
    }
    
    func storeSocialAccountsLocally(_ accounts: [NCSocialAccount]) {
        
        self.userAccounts = accounts
        NSKeyedArchiver.setClassName("NCSocialAccount", for: NCSocialAccount.self)
        NSKeyedArchiver.archiveRootObject(self.userAccounts, toFile: NCDataStoreSwift.accountsArchivePath())
        
    }
    
    func storeSocialStatsLocally(_ stats: [NCSocialStat]) {
        
        self.allStats = stats
        NSKeyedArchiver.setClassName("NCSocialStat", for: NCSocialStat.self)
        NSKeyedArchiver.archiveRootObject(self.allStats, toFile: NCDataStoreSwift.statsArchivePath())
        
    }
    
    func saveHeadlinePostLocally(_ headline: NCHeadline) {
        
        NSKeyedArchiver.setClassName("NCHeadline", for: NCHeadline.self)
        if self.postedHeadlines.contains(headline) == false {
            self.postedHeadlines.append(headline)
        }
        NSKeyedArchiver.archiveRootObject(self.postedHeadlines, toFile: NCDataStoreSwift.headlinesArchivePath())
    }
    
    func saveHeadlineBookmarkLocally(_ headline: NCHeadline) {
        
        NSKeyedArchiver.setClassName("NCHeadline", for: NCHeadline.self)
        if self.bookmarkedHeadlines.contains(headline) == false {
            self.bookmarkedHeadlines.append(headline)
        }
        NSKeyedArchiver.archiveRootObject(self.bookmarkedHeadlines, toFile: NCDataStoreSwift.bookmarksArchivePath())
    }
    
    func saveSeenHeadlinesLocally() {
        
        NSKeyedArchiver.setClassName("NCHeadline", for: NCHeadline.self)
        let dictionaryToBeSaved = self.seenHeadlinesByFeed
        let maxCountPerFeed = 20
        
        for key in dictionaryToBeSaved.keys {
            if var headlinesInFeed = self.seenHeadlinesByFeed[key] {
                if headlinesInFeed.count > maxCountPerFeed {
                    var setToBeSaved = [NCHeadline]()
                    let count = 0
                    
                    for headline in headlinesInFeed {
                        setToBeSaved.append(headline)
                        if (count == maxCountPerFeed) {break}
                    }
                    
                    headlinesInFeed.removeAll()
                    headlinesInFeed.append(contentsOf: setToBeSaved)
                }
            }
        }
        
        NSKeyedArchiver.archiveRootObject(dictionaryToBeSaved, toFile: NCDataStoreSwift.seenHeadlineArchivePath())
    }
    
    func saveFeedsLocally() {
        
        NSKeyedArchiver.setClassName("NCFeed", for: NCFeed.self)
        NSKeyedArchiver.archiveRootObject(self.allFeeds, toFile: NCDataStoreSwift.feedsPath())
    }
    
    // MARK: Retrieval
    
    func retrievePostedHeadlines() -> [NCHeadline] {
        
        NSKeyedUnarchiver.setClass(NCHeadline.self, forClassName: "NCHeadline")
        if self.postedHeadlines.count == 0 {
            if let archiveContents = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.headlinesArchivePath()) as? [NCHeadline] {
                if !archiveContents.isEmpty {
                    for h in archiveContents {
                        self.postedHeadlines.append(h)
                    }
                }
            }
        }
        return self.postedHeadlines
    }
        
    func retrieveBookmarkedHeadlines() -> [NCHeadline] {
        
        NSKeyedUnarchiver.setClass(NCHeadline.self, forClassName: "NCHeadline")
        if self.bookmarkedHeadlines.count == 0 {
            if let archiveContents = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.bookmarksArchivePath()) as? [NCHeadline] {
                if !archiveContents.isEmpty {
                    for h in archiveContents {
                        self.bookmarkedHeadlines.append(h)
                    }
                }
            }
        }
        return self.bookmarkedHeadlines
    }
    
    func retrieveStoredFeeds() -> [NCFeed] {

        NSKeyedUnarchiver.setClass(NCFeed.self, forClassName: "NCFeed")
        var contents: [NCFeed] = []
        if let archiveContents = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.feedsPath()) as? [NCFeed] {
            contents = archiveContents
            return contents
        }
        return contents
    }
    
    func retrieveSeenHeadlines() -> [Int: [NCHeadline]] {
        
        NSKeyedUnarchiver.setClass(NCHeadline.self, forClassName: "NCHeadline")
        let archiveContents = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.seenHeadlineArchivePath())
        self.seenHeadlinesByFeed = [Int: [NCHeadline]]()
        if archiveContents != nil {
            if let contents = archiveContents as? [Int:[NCHeadline]] {
                self.seenHeadlinesByFeed = contents
            }
        }
        return self.seenHeadlinesByFeed
    }
    
    func retrieveUser() -> NCUser? {
        
        NSKeyedUnarchiver.setClass(NCUser.self, forClassName: "NCUser")
        if self.loggedInUser == nil {
            if let archiveContent = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.userArchivePath()) as? NCUser {
                self.loggedInUser = archiveContent
                return self.loggedInUser!
            }
        }
        return self.loggedInUser
        
    }
    
    func retrieveSocialAccounts() -> [NCSocialAccount]? {
        
        NSKeyedUnarchiver.setClass(NCSocialAccount.self, forClassName: "NCSocialAccount")
        if let archiveContent = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.accountsArchivePath()) as? [NCSocialAccount] {
            self.userAccounts = archiveContent
            return self.userAccounts
        }
        return self.userAccounts
    }
    
    func retrieveSocialStats() -> [NCSocialStat] {
        
        NSKeyedUnarchiver.setClass(NCSocialStat.self, forClassName: "NCSocialStat")
        if let archiveContent = NSKeyedUnarchiver.unarchiveObject(withFile: NCDataStoreSwift.statsArchivePath()) as? [NCSocialStat] {
            self.allStats = archiveContent
            return self.allStats
        }
        return self.allStats
    }
    
    // MARK: Archive Operations
    
    class func userArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("currentusersep21.archive")
        return stringToTransform
    }
    
    class func accountsArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("socialaccountssep21.archive")
        return stringToTransform
    }
    
    class func headlinesArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("sharedheadlinesoct11.archive")
        return stringToTransform
    }
    
    class func bookmarksArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("bookmarkedheadlinesoct11.archive")
        return stringToTransform
    }
    
    class func seenHeadlineArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("seenheadlinesbyfeedsep21.archive")
        return stringToTransform
    }
    
    class func feedsPath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("feedssep21.archive")
        return stringToTransform
    }
    
    class func statsArchivePath() -> String {
        let string = NCDataStoreSwift.documentDirectory()
        let stringToTransform = (string as NSString).appendingPathComponent("statssep21.archive")
        return stringToTransform
    }
    
    class func documentDirectory() -> String {
        let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectory = documentDirectories.first
        return documentDirectory!
    }
    
    class func sharedDocumentDirectory() -> URL {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.kimengi.ios.NineConnections")!
    }

}

