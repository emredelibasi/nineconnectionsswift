//
//  NCConnectButton.m
//  NineConnections
//
//  Created by Floris van der Grinten on 15-07-14.
//
//

#import "NCConnectButton.h"


@implementation NCConnectButton {
    NSString *network; /// UDRA. Set in storyboard.
    UIColor *initialBackgroundColor;
    CGRect titleLabelInitialFrame;
    CGRect titleLabelLoggedInFrame;
    //UILabel *disconnectLabel;
    UIFont *titleLabelFont;
    UIImageView *imageView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    CGFloat iconTopPadding = 19.0;
    CGFloat iconLeftPadding = 22.0;
    
    BOOL backgroundColorIsLight = NO;
    if ([network isEqualToString:@"Twitter"]) {
        initialBackgroundColor = [UIColor twitterColor];
        self.buttonType = SocialNetworkTwitter;
    }
    else if ([network isEqualToString:@"LinkedIn"]) {
        initialBackgroundColor = [UIColor linkedInColor];
        self.buttonType = SocialNetworkLinkedIn;
        iconTopPadding -= 2;
    }
    else if ([network isEqualToString:@"Buffer"]) {
        initialBackgroundColor = [UIColor whiteColor];
        backgroundColorIsLight = YES;
        iconTopPadding--;
        self.buttonType = SocialNetworkBuffer;
    }

    self.layer.cornerRadius = 4;
    
    CGFloat labelLeftPadding = 66.0;
    titleLabelInitialFrame = CGRectMake(labelLeftPadding, 0, self.frame.size.width-labelLeftPadding-10.0, 54);
    titleLabelLoggedInFrame = CGRectOffset(titleLabelInitialFrame, -12.0, -9);
    
    titleLabelFont = [UIFont fontWithName:@"ProximaNova-Regular" size:19.0];
    self.titleLabel = [[TOMSMorphingLabel alloc] initWithFrame:titleLabelInitialFrame];
    self.titleLabel.font = titleLabelFont;
    self.titleLabel.textColor = backgroundColorIsLight ? [UIColor blackColor] : [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.animationDuration = 0.45;
    //self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    
    self.disconnectLabel = [UILabel new];
    self.disconnectLabel.text = @"Disconnect profile.";
    self.disconnectLabel.font = [UIFont fontWithName:@"ProximaNova-LightIt" size:15.0];
    self.disconnectLabel.textColor = [UIColor whiteColor];
    self.disconnectLabel.textAlignment = NSTextAlignmentCenter;
    [self.disconnectLabel sizeToFit];
    self.disconnectLabel.layer.anchorPoint = CGPointMake(0, 0.5);
    self.disconnectLabel.frame = CGRectOffset(titleLabelLoggedInFrame, 0, 18);
    self.disconnectLabel.transform = CGAffineTransformMakeScale(0, 1.0);
    [self addSubview:self.disconnectLabel];

    
    CGFloat iconSize = 22.0;
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(iconLeftPadding, iconTopPadding, iconSize, iconSize)];
    imageView.image = [[UIImage imageNamed:[NSString stringWithFormat:@"%@_share", [network lowercaseString]]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.tintColor = backgroundColorIsLight ? [UIColor blackColor] : [UIColor whiteColor];

    [self addSubview:imageView];

    self.backgroundColor = initialBackgroundColor;
    
}

- (void)accountConnected:(NSString *)accountString animated:(BOOL)animated
{
    self.connected = YES;
    self.titleLabel.text = accountString ? accountString : @"";
    CGFloat duration = animated ? 0.4 : 0;
    [UIView animateWithDuration:duration animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        self.titleLabel.frame = titleLabelLoggedInFrame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:duration/2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.disconnectLabel.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } completion:^(BOOL finished) {
        }];
    }];
 
    if (self.buttonType == SocialNetworkBuffer) {
        if (accountString.length > 16) self.titleLabel.font = [UIFont fontWithName:self.titleLabel.font.fontName size:16.0];
        self.titleLabel.textColor = [UIColor whiteColor];
        imageView.tintColor = [UIColor whiteColor];
    }
}

- (void)accountDisconnectedWithDuration:(CGFloat) duration
{
    [UIView animateWithDuration:duration animations:^{
        self.disconnectLabel.transform = CGAffineTransformMakeScale(0, 1.0);
        self.titleLabel.frame = titleLabelInitialFrame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 delay:0.2 options:0 animations:^{
            self.backgroundColor = initialBackgroundColor;
        } completion:nil];
        self.connected = NO;
    }];
    self.titleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Connect to %@", ""), network];
    
    if (self.buttonType == SocialNetworkBuffer) {
        self.titleLabel.font = titleLabelFont;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.titleLabel.textColor = [UIColor blackColor];
        });
        imageView.tintColor = [UIColor blackColor];
    }

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    if (!self.isConnected) {
        UIColor *highlightedColor = [self.backgroundColor darkenColor];
        if (!highlightedColor) highlightedColor = [UIColor colorWithWhite:0.94 alpha:1.0];
        
        self.backgroundColor = highlightedColor;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (!self.isConnected) {
        self.backgroundColor = initialBackgroundColor;
    }
}
    //- (void)setValue:(id)value forKey:(NSString *)key
//{
//    if ([key isEqualToString:@"network"]) {
//        network = [self valueForKey:@"network"];
//        [self buttonAppearance];
//    }
//}
//
//- (void)buttonAppearance
//{
//    UIColor *backgroundColor;
//    
//    if ([network isEqualToString:@"Twitter"]) {
//        backgroundColor = [UIColor twitterColor];
//    }
//    else if ([network isEqualToString:@"LinkedIn"]) {
//        backgroundColor = [UIColor linkedInColor];
//    }
//    
//    self.backgroundColor = backgroundColor;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
