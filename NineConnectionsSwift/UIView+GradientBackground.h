//
//  UIView+GradientBackground.h
//  NineConnections
//
//  Created by Floris van der Grinten on 18-07-14.
//
//

#import <UIKit/UIKit.h>

@interface UIView (GradientBackground)

- (void)gradientBackgroundFrom:(UIColor *)firstColor to:(UIColor *)secondColor;

@end
