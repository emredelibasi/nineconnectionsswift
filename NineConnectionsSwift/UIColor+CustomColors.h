//
//  UIColor+CustomColors.h
//  NineConnections
//
//  Created by Floris van der Grinten on 13-05-14.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)
+ (UIColor *)backgroundGreyColor;
+ (UIColor *)mainBlueColor;
+ (UIColor *)mainBlueLighterColor;
+ (UIColor *)lightGradientColor;
+ (UIColor *)darkGradientColor;
+ (UIColor *)linkedInColor;
+ (UIColor *)twitterColor;
+ (UIColor *)linkedInDarkerColor;
+ (UIColor *)twitterDarkerColor;
+ (UIColor *)successColor;
- (UIColor *)darkenColor;
@end
