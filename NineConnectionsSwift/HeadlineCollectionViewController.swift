//
//  HeadlineCollectionViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 2/10/17.
//  Copyright © 2017 Emre Delibasi. All rights reserved.
//

import UIKit
import JGActionSheet
import MessageUI
import KVOController
import CoreData
import Analytics
import UIColor_Hex_Swift

private let reuseIdentifier = "HeadlineCell"

class HeadlineCollectionViewController: UIViewController, UIScrollViewDelegate, UIViewControllerTransitioningDelegate, JGActionSheetDelegate, UIDynamicAnimatorDelegate, MFMailComposeViewControllerDelegate, ShareButtonDelegate, UICollectionViewDelegate, UICollectionViewDataSource, ArticleReaderFlowDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var archiveMode = false
    var sharedMode = false
    var smartSharedMode = false
    var bookmarkedMode = false
    var dynamicAnimator: UIDynamicAnimator?
    var instantShareHighlightView: UIView?
    var instantShareWarningScheduled: NCInstantShareLabel?
    var instantShareWarningNow: NCInstantShareLabel?
    var instantShareWarningNDescription: UILabel?
    var instantShareWarningSDescription: UILabel?
    var instantShareWarningImageView: UIImageView?
    var urlOptionsActionSheet: JGActionSheet?
    var currentFeed: NCFeed?
    var userCompany: NCCompany?
    let loggedInUser = NCDataStoreSwift.sharedStore.loggedInUser
    var allStats = [NCSocialStat]()
    var fetchedHeadlines = [NCHeadline]()
    var shareButtons: [NCShareButton]?
    var shareButtonY: CGFloat?
    var windowWidth: CGFloat?
    var windowHeight: CGFloat?
    var shareMessage = ""
    var shareInstant = false
    var shareFeed: NCFeed?
    var shareNetwork: SocialNetwork?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        if let layout = collectionView.collectionViewLayout as? ArticleReaderFlowLayout {
            layout.delegate = self
        }
        
        windowWidth = self.view.bounds.size.width
        windowHeight = self.view.frame.size.height
        shareButtonY = UIScreen.main.bounds.size.height - 140
        
        self.dynamicAnimator = UIDynamicAnimator(referenceView: self.view)
        self.dynamicAnimator?.delegate = self
 
        customAppearance()
        self.initializeShareButtons()
        
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)

        if self.navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) == true {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
    
        self.collectionView.register(HeadlineCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.register(UINib(nibName: "HeadlineCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkIfShared), name: NSNotification.Name(rawValue: "MessagePosted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showConnectWarning(_:)), name: NSNotification.Name(rawValue: "NoAccount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNetworkWarning(_:)), name: NSNotification.Name(rawValue: "NoInternet"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showServerWarning(_:)), name: NSNotification.Name(rawValue: "ServerError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showWebPageOnLinkedInError(_:)), name: NSNotification.Name(rawValue: "LinkedInError"), object: nil)
        
        self.allStats = NCDataStoreSwift.sharedStore.retrieveSocialStats()
        
        refreshHeadlines { (refreshed: Bool) in
            startInterfaceSetup({ (finished:Bool) in
                self.collectionView.reloadData()
                checkIfShared()
            })
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.checkIfShared()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        customAppearance()
    }
    
    func customAppearance() {
        
        self.view.isUserInteractionEnabled = true
        self.collectionView.backgroundColor = UIColor.companySecondaryBackground()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.companyInnerNavBar()
        
        if !(self.navigationItem.titleView is NCSeenCountView) {
            let title: String
            if self.archiveMode == true {title = NSLocalizedString("MAIN_NAV_BAR_SEEN_TITLE", comment: "")}
            else if self.sharedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_SHARED_TITLE", comment: "")}
            else if self.smartSharedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_SMART_SHARED_TITLE", comment: "")}
            else if self.bookmarkedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_BOOKMARKED_TITLE", comment: "")}
            else {title = self.currentFeed!.name}
            
            self.navigationItem.createCustomTitle(title, fontSize: OLD_IPHONE ? 17 : 20)
        }
    }
    
    // MARK: Data Operations
    
    func refreshHeadlines(_ refreshed: ((Bool) -> Void)) {
        
        if self.archiveMode == true {
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.sequentialSeenHeadlines {
                if !fetchedHeadlines.contains(h) && !h.shared {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.seenAt.timeIntervalSince1970 > $1.seenAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            refreshed(true)
            
        } else if self.sharedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.retrievePostedHeadlines()  {
                if !fetchedHeadlines.contains(h) && ((h.sharedInstantlyOnTwitter == true || h.sharedInstantlyOnLinkedIn == true) && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.sharedAt.timeIntervalSince1970 > $1.sharedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))

            refreshed(true)
            
        } else if self.smartSharedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.retrievePostedHeadlines() {
                if !fetchedHeadlines.contains(h) && ((h.sharedWithScheduleOnTwitter == true || h.sharedWithScheduleOnLinkedIn == true) && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.sharedAt.timeIntervalSince1970 > $1.sharedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            refreshed(true)
            
        } else if self.bookmarkedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            for h in NCDataStoreSwift.sharedStore.retrieveBookmarkedHeadlines() {
                if !fetchedHeadlines.contains(h) && (h.isBookmarked == true && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.bookmarkedAt.timeIntervalSince1970 > $1.bookmarkedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            refreshed(true)
            
        } else {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let seenHeadlines = NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id]
            if seenHeadlines != nil {
                for seen in seenHeadlines! {
                    self.fetchedHeadlines = self.fetchedHeadlines.filter({ (headline: NCHeadline) -> Bool in
                        headline.id != seen.id
                    })
                }
                self.fetchedHeadlines = NCDataStoreSwift.sharedStore.sortHeadlinesById(self.fetchedHeadlines)
            } else {
                self.fetchedHeadlines = NCDataStoreSwift.sharedStore.sortHeadlinesById(self.fetchedHeadlines)
            }
            
            refreshed(true)
        }
    }
    
    // MARK: Initialization
    
    func startInterfaceSetup(_ finished: ((Bool) -> Void)) {
        
        if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
            for h in fetchedHeadlines {
                h.title = h.title.correctArticleTitle()
            }
        }
        
        if fetchedHeadlines.count > 0 {
            if self.navigationItem.titleView is UILabel {
                if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
                    self.initializeSeenCounter()
                }
            }
        }
        
        for b in shareButtons! {
            b.shareInteractionEnabled = true
        }
        
        self.instantShareHighlightView = UIView(frame: CGRect(x: 0, y: 0, width: windowWidth!, height: UIScreen.main.bounds.size.height-220))
        self.instantShareHighlightView?.backgroundColor = UIColor.white
        
        self.instantShareWarningNow = NCInstantShareLabel()
        self.instantShareWarningNow?.frame = CGRect(x: 11, y: 11, width: windowWidth!-22, height: (2/5)*instantShareHighlightView!.frame.height)
        self.instantShareWarningNow?.text = NSLocalizedString("SHARE_NOW_TEXT", comment: "")
        self.instantShareWarningNow?.textAlignment = .center
        self.instantShareWarningNow?.backgroundColor = UIColor.white
        self.instantShareWarningNow?.addDashedBorder(color: UIColor("#3F4D61"))
        
        self.instantShareWarningNDescription = UILabel()
        self.instantShareWarningNDescription?.text = NSLocalizedString("SHARE_NOW_DESCRIPTION_TEXT", comment: "")
        self.instantShareWarningNDescription?.textAlignment = .center
        self.instantShareWarningNDescription?.textColor = UIColor.white
        self.instantShareWarningNDescription?.backgroundColor = UIColor.clear
        self.instantShareWarningNDescription?.frame = CGRect(x: 70, y: 5, width: windowWidth!-140, height: 50)
        self.instantShareWarningNDescription?.center = CGPoint(x: windowWidth!/2, y: (1/5)*instantShareHighlightView!.frame.height + (OLD_IPHONE ? 25 : 15))
        self.instantShareWarningNDescription?.font = UIFont(name: "ProximaNova-Regular", size: OLD_IPHONE ? 14 : 15)
        self.instantShareWarningNDescription?.numberOfLines = 4
        
        self.instantShareWarningScheduled = NCInstantShareLabel()
        self.instantShareWarningScheduled?.frame = CGRect(x: 11, y: (2/5)*instantShareHighlightView!.frame.height + 19, width: windowWidth!-22, height: (3/5)*instantShareHighlightView!.frame.height)
        self.instantShareWarningScheduled?.text = NSLocalizedString("SHARE_SMART_TEXT", comment: "")
        self.instantShareWarningScheduled?.textAlignment = .center
        self.instantShareWarningScheduled?.backgroundColor = UIColor.white
        self.instantShareWarningScheduled?.addDashedBorder(color: UIColor("#3F4D61"))
        
        let shareImage = UIImage(named: "smart_share")
        self.instantShareWarningImageView = UIImageView(image: shareImage)
        self.instantShareWarningImageView?.frame = CGRect(x: windowWidth!/2 - 28.5, y: (2/5)*instantShareHighlightView!.frame.height + 54, width: 57, height: 32)
        
        self.instantShareWarningSDescription = UILabel()
        self.instantShareWarningSDescription?.text = NSLocalizedString("SHARE_SMART_DESCRIPTION_TEXT", comment: "")
        self.instantShareWarningSDescription?.textAlignment = .center
        self.instantShareWarningSDescription?.textColor = UIColor.white
        self.instantShareWarningSDescription?.backgroundColor = UIColor.clear
        self.instantShareWarningSDescription?.frame = CGRect(x: 70, y: (3.5/5)*instantShareHighlightView!.frame.height + 10, width: windowWidth!-140, height: 80)
        self.instantShareWarningSDescription?.center = CGPoint(x: windowWidth!/2, y: (3.5/5)*instantShareHighlightView!.frame.height + (OLD_IPHONE ? 33 : 23))
        self.instantShareWarningSDescription?.font = UIFont(name: "ProximaNova-Regular", size: OLD_IPHONE ? 14 : 15)
        self.instantShareWarningSDescription?.numberOfLines = 4
    
        finished(true)

    }
    
    func initializeSeenCounter() {
        
        let counterView = NCSeenCountView(articleCount: fetchedHeadlines.count-1, excistingTitle: self.navigationItem, fontSize: OLD_IPHONE ? 12.5 : 14)
        
        if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
            self.kvoController.observe(counterView, keyPath: "lowestCount", options: .new, block: { (observer: Any?, countView: Any, change: [String : Any]) in
                if let count = change["new"] as? Int {
                    let lowestCount = count
                    
                    let newKey = self.fetchedHeadlines.count - lowestCount - 1
                    let thisHeadline = self.fetchedHeadlines[newKey]
                    var possibleFeedHeadlines = NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id]
                    
                    if (possibleFeedHeadlines != nil) { possibleFeedHeadlines!.insert(thisHeadline, at: 0)
                        NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id] = possibleFeedHeadlines
                    }
                    else {
                        var newHeadlinesSetForCurrentFeed = [NCHeadline]()
                        newHeadlinesSetForCurrentFeed.append(thisHeadline)
                        NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id] = newHeadlinesSetForCurrentFeed
                    }
                    
                    if NCDataStoreSwift.sharedStore.sequentialSeenHeadlines.contains(thisHeadline) == false {
                        NCDataStoreSwift.sharedStore.sequentialSeenHeadlines.insert(thisHeadline, at: 0)
                    }
                    
                    self.currentFeed!.unseenAmount = lowestCount
                    for feed in NCDataStoreSwift.sharedStore.allFeeds {
                        if feed.hash == self.currentFeed!.hash {
                            feed.unseenAmount = self.currentFeed!.unseenAmount
                        }
                    }
                }
            })
        }
        counterView?.count = fetchedHeadlines.count - 1
        let navTitleStaticFrame:CGRect = self.navigationItem.titleView!.frame
        counterView?.frame = navTitleStaticFrame
        counterView?.countLabel.textColor = /*self.company?.mainColor != nil ?  self.company?.mainColor! : */UIColor.white
        self.navigationItem.titleView = counterView
    }

    func initializeShareButtons() {
        
        shareButtons = [NCShareButton]()
        
        let twitterShareButton = NCShareButton(buttonType: .twitter)
        self.view.addSubview(twitterShareButton!)
        shareButtons?.append(twitterShareButton!)
        
        let linkedInShareButton = NCShareButton(buttonType: .linkedIn)
        self.view.addSubview(linkedInShareButton!)
        shareButtons?.append(linkedInShareButton!)
        
        let amountOfShareButtons = shareButtons!.count
        for i in 0 ..< amountOfShareButtons {
            let thisButton = shareButtons![i]
            thisButton.delegate = self
            
            let spaceBetweenShareButtons:CGFloat = (windowWidth! + thisButton.frame.width) / CGFloat(amountOfShareButtons+1)
            thisButton.centerPosition = CGPoint(x: (spaceBetweenShareButtons * CGFloat(i+1)) - (thisButton.frame.width/2), y: shareButtonY!)
            thisButton.center = thisButton.centerPosition
            thisButton.dynamicsAnimator = self.dynamicAnimator
            thisButton.addShareButtonsSnappingBehavior()
        }
    }
    
    
    // MARK: Share Button Methods
    
    var shareActivationBegan = false
    
    func instantShareActivationBegan() {
        
        shareActivationBegan = true
        
        DispatchQueue.main.async {
            self.view.insertSubview(self.instantShareHighlightView!, aboveSubview: self.collectionView!)
            self.view.insertSubview(self.instantShareWarningScheduled!, aboveSubview: self.instantShareHighlightView!)
            self.view.insertSubview(self.instantShareWarningNow!, aboveSubview: self.instantShareHighlightView!)
            self.view.insertSubview(self.instantShareWarningImageView!, aboveSubview: self.instantShareWarningScheduled!)
            self.view.insertSubview(self.instantShareWarningSDescription!, aboveSubview: self.instantShareWarningScheduled!)
            self.view.insertSubview(self.instantShareWarningNDescription!, aboveSubview: self.instantShareWarningNow!)
            
            
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.instantShareHighlightView?.alpha = 1.0
                self.instantShareWarningNow?.alpha = 0.5
                self.instantShareWarningScheduled?.alpha = 0.5
                self.instantShareWarningNDescription?.alpha = 0.5
                self.instantShareWarningSDescription?.alpha = 0.5
                self.instantShareWarningImageView?.alpha = 0.5
            })
        }
    }
    
    func shareButtonMoving(_ sender: Any!) {
        
        //  iPhone 6
        let y1 = Double(2)
        let y2 = Double(instantShareWarningScheduled!.frame.height + 6)
        
        if let button = sender as? NCShareButton {
            
            DispatchQueue.main.async {
                
                if button.translationDetect <=  y1 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningNow?.backgroundColor = UIColor.white
                        self.instantShareWarningScheduled?.backgroundColor = UIColor.white
                        self.instantShareWarningNow?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningScheduled?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningSDescription?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningNDescription?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        
                    })
                    
                } else if button.translationDetect > y1 && button.translationDetect < y2 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningScheduled?.alpha = 1.0
                        self.instantShareWarningNow?.alpha = 0.5
                        self.instantShareWarningNDescription?.alpha = 0.5
                        self.instantShareWarningSDescription?.alpha = 1
                        self.instantShareWarningImageView?.alpha = 1
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = button.socialMediaColor.cgColor
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        
                    })
                    
                } else if button.translationDetect >= y2 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningScheduled?.alpha = 0.5
                        self.instantShareWarningNow?.alpha = 1.0
                        self.instantShareWarningNDescription?.alpha = 1
                        self.instantShareWarningSDescription?.alpha = 0.5
                        self.instantShareWarningImageView?.alpha = 0
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = button.socialMediaColor.cgColor
                        
                    })
                    
                } else {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningNow?.alpha = 0
                        self.instantShareWarningScheduled?.alpha = 0
                        self.instantShareWarningNDescription?.alpha = 0
                        self.instantShareWarningSDescription?.alpha = 0
                        self.instantShareWarningImageView?.alpha = 0
                        
                    })
                }
            }
        }
    }
    
    func instantShareActivationEnded(_ finished: Bool, toShareInstantly instantly: Bool, to network: SocialNetwork, sender: Any!) {
        
        shareActivationBegan = false
        var canProceed = false
        let accounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
        var service = ""
        
        switch network {
        case .linkedIn:
            service = "linkedin"
            break
        case .twitter:
            service = "twitter"
            break
        default:
            break
        }
        
        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
            account.service == service
        })
        
        if finished == true {
            if canProceed {
                let currentHeadline = fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
                DispatchQueue.global(qos: .default).async(execute: { () -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    NCDataStoreSwift.sharedStore.prepareForPosting("", toNetwork: network, fromHeadline: currentHeadline, inChannel: self.currentFeed?.id != nil ? String(self.currentFeed!.id) : "Shared from Seen Headlines", immediately: instantly, withSuccess: { (succeeded: Bool, error: Error?, response: HTTPURLResponse?) -> Void in
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        DispatchQueue.main.async(execute: { () -> Void in
                            if (error == nil && succeeded == true) {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                self.checkIfShared()
                            } else if (error as? NSError)?.code == -1009  {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "NoInternet"), object: self)
                            } else {
                                if network == .twitter {
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ServerError"), object: self)
                                } else {
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LinkedInError"), object: nil, userInfo: ["message": "", "network" : network, "channel" : (self.currentFeed?.id != nil ? String(self.currentFeed!.id) : "Shared from Seen Headlines"), "immediate" : instantly])
                                }
                            }
                        })
                    })
                })
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NoAccount"), object: self)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        
        DispatchQueue.main.async { () -> Void in
            
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.instantShareHighlightView?.alpha = 0
                self.instantShareWarningScheduled?.alpha = 0
                self.instantShareWarningNow?.alpha = 0
                self.instantShareWarningSDescription?.alpha = 0
                self.instantShareWarningNDescription?.alpha = 0
                self.instantShareWarningImageView?.alpha = 0
                
            }, completion: nil)
        }
    }
    
    func shareButtonTapped(_ sender: Any!) {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.45 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            var canProceed = false
            let accounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
            if accounts != nil {
                if let button = sender as? NCShareButton {
                    switch (button.buttonType) {
                    case .twitter:
                        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
                            account.service == "twitter"
                        })
                        break
                    case .linkedIn:
                        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
                            account.service == "linkedin"
                        })
                        break
                    default:
                        break
                    }
                }
            }
            
            if self.presentedViewController == nil {
                if canProceed == true {
                    self.performSegue(withIdentifier: "showMessageModalSegue", sender: sender)
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NoAccount"), object: self)
                }
            }
        }
    }
    
    // MARK: UICollectionView Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fetchedHeadlines.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HeadlineCollectionViewCell
        
        
        cell.headline = self.fetchedHeadlines[indexPath.item]
        
        cell.titleLabel.text = cell.headline!.title
        
        //        let headlineHasTags = self.headline!.tags.count > 0 || self.headline!.trends.count > 0
        //        self.titleView!.center = CGPoint(x: self.windowWidth!/2, y: self.frame.midY - /*(headlineHasTags ? 142 :*/ 86)
        
        let hostWithoutWWW = cell.headline!.url!.host!.replacingOccurrences(of: "www.", with: "")
        
        cell.urlButton.setTitle("\(hostWithoutWWW)", for:.normal)
        
        cell.urlButton.isHidden = UserDefaults.standard.bool(forKey: DEFAULTS_REMOVE_ARTICLE_LINKS)
        //            self.urlButton.titleLabel.font = UIFont(name: "ProximaNova-Regular", size: OLD_IPHONE ? 15 : 17)
                
        if cell.headline!.isBookmarked {
            cell.bookmarkedImageView.image = #imageLiteral(resourceName: "bookmarked_icon")
            cell.bookmarkedImageView.image = cell.bookmarkedImageView.image?.withRenderingMode(.alwaysTemplate)
            cell.bookmarkedImageView.tintColor = UIColor.companyInnerNavBar()
        } else {
            cell.bookmarkedImageView.image = #imageLiteral(resourceName: "unbookmarked_icon")
        }
        
        let cardHoldRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onCardHold(_:)))
        cardHoldRecognizer.minimumPressDuration = 0.8
        cell.addGestureRecognizer(cardHoldRecognizer)
        
        let linkHoldRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(showURLOptionsActionSheet(_:)))
        linkHoldRecognizer.minimumPressDuration = 0.4
        cell.urlButton?.addGestureRecognizer(linkHoldRecognizer)
        cell.gestureBlock = {
            self.showFullArticleText(self)
        }
        
        return cell
    }
    
    func collectionView(contentSizeForCollectionView collectionView: UICollectionView) -> CGSize {
        
       return CGSize(width: self.collectionView.frame.width * CGFloat(self.fetchedHeadlines.count), height: self.collectionView.frame.height)

    }
    
    func collectionView(sizeForItemIn collectionView: UICollectionView) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for b in shareButtons! {
            b.shareInteractionEnabled = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for b in shareButtons! { b.shareInteractionEnabled = true }
        self.checkIfShared()
    }
    
    // MARK: Other methods
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func checkIfShared() {
        
        DispatchQueue.main.async { () -> Void in
            
            if (self.presentedViewController == nil && self.dynamicAnimator?.isRunning == false && self.fetchedHeadlines.count > 0 && self.collectionView.visibleCells.count > 0) {
                
                let headline = self.fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
                let seenProperties = ["Article" : headline.title, "ArticleID" : headline.id, "Seen on Channel" : self.currentFeed != nil ? String(self.currentFeed!.id) : "", "Url" : "\(headline.url!)", "Date" : "\(Date())"] as [String : Any]
                
                if !headline.isSeen {
                    SEGAnalytics.shared().track(SEG_Article_Seen, properties: seenProperties as [AnyHashable: Any])
                    headline.isSeen = true
                    headline.seenAt = Date()
                }
                
                let postedHeadlines = NCDataStoreSwift.sharedStore.retrievePostedHeadlines()
                
                let postedContainsArticle = postedHeadlines.contains {$0.title == headline.title}
                
                var headlineSharedInstantlyOnTwitter = false
                var headlineSharedInstantlyOnLinkedIn = false
                var headlineSharedWithScheduleOnTwitter = false
                var headlinesharedWithScheduleOnLinkedIn = false
                
                // Implemented for articles that might be in 2 or more feeds
                
                if postedContainsArticle {
                    for h1 in postedHeadlines {
                        if h1.title == headline.title {
                            headlineSharedInstantlyOnTwitter = h1.sharedInstantlyOnTwitter ? true : false
                            headlineSharedInstantlyOnLinkedIn = h1.sharedInstantlyOnLinkedIn ? true : false
                            headlineSharedWithScheduleOnTwitter = h1.sharedWithScheduleOnTwitter ? true : false
                            headlinesharedWithScheduleOnLinkedIn = h1.sharedWithScheduleOnLinkedIn ? true : false
                        }
                    }
                } else {
                    headlineSharedInstantlyOnTwitter = headline.sharedInstantlyOnTwitter ? true : false
                    headlineSharedInstantlyOnLinkedIn = headline.sharedInstantlyOnLinkedIn ? true : false
                    headlineSharedWithScheduleOnTwitter = headline.sharedWithScheduleOnTwitter ? true : false
                    headlinesharedWithScheduleOnLinkedIn = headline.sharedWithScheduleOnLinkedIn ? true : false
                }
                
                // Checks and updates UI if the article is shared
                
                if (headlineSharedInstantlyOnTwitter == true || headlineSharedInstantlyOnLinkedIn == true || headlineSharedWithScheduleOnTwitter == true || headlinesharedWithScheduleOnLinkedIn == true) {
                    let twitterButton = self.shareButtons![0]
                    let linkedInButton = self.shareButtons![1]
                    
                    if (headlineSharedInstantlyOnLinkedIn == true || headlinesharedWithScheduleOnLinkedIn == true) {
                        linkedInButton.isScheduled = headlinesharedWithScheduleOnLinkedIn
                        linkedInButton.animateSharedState(true)
                    } else {linkedInButton.animateSharedState(false)}
                    
                    if (headlineSharedInstantlyOnTwitter == true || headlineSharedWithScheduleOnTwitter == true) {
                        twitterButton.isScheduled = headlineSharedWithScheduleOnTwitter
                        twitterButton.animateSharedState(true)
                    } else {twitterButton.animateSharedState(false) }
                } else {
                    for b in self.shareButtons! {
                        b.animateSharedState(false)
                    }
                }
                
                if let countView = self.navigationItem.titleView as? NCSeenCountView {
                    if countView.responds(to: #selector(setter: NCSeenCountView.count)) {
                        countView.count = self.fetchedHeadlines.getUnread().count
                    }
                }
            }
        }
    }
    
    @IBAction func showFullArticleText(_ sender: Any) {
        
        let visibleCell = self.collectionView.visibleCells[0] as? HeadlineCollectionViewCell
        
        if visibleCell?.headline?.articleText == nil {
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            if self.currentFeed != nil && visibleCell?.headline != nil {
                NCDataStoreSwift.sharedStore.fetchFullArticleTextFromHeadline(visibleCell!.headline!, inFeed: self.currentFeed!, withSuccess: { (succeeded: Bool, error: Error?) -> Void in
                    
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                        if (succeeded && visibleCell!.headline!.articleText!.characters.count >= 400) {
                            self.performSegue(withIdentifier: "showArticleReaderSegue", sender: sender)
                        } else if (error as? NSError)?.code == -1009 {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "NoInternet"), object: self)
                        } else {
                            self.performSegue(withIdentifier: "showWebPageSegue", sender: sender)
                        }
                    }
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.performSegue(withIdentifier: "showWebPageSegue", sender: sender)
                })
            }
        } else if visibleCell!.headline!.articleText!.characters.count < 400 {
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSegue(withIdentifier: "showWebPageSegue", sender: sender)
            })
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSegue(withIdentifier: "showArticleReaderSegue", sender: sender)
            })
        }
    }
    
    func showURLOptionsActionSheet(_ gesture: UILongPressGestureRecognizer) {
        
        if gesture.state == UIGestureRecognizerState.began {
            if self.urlOptionsActionSheet == nil {
                
                let actionsSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: [NSLocalizedString("OPEN_WEBPAGE_TEXT", comment: ""), NSLocalizedString("OPEN_URL_IN_SAFARI_TEXT", comment: ""), NSLocalizedString("EMAIL_URL_TEXT", comment: ""), NSLocalizedString("COPY_URL_TEXT", comment: ""), NSLocalizedString("MORE_SHARING_OPTIONS_TEXT", comment: "")], buttonStyle: JGActionSheetButtonStyle.default)
                let cancelSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: [NSLocalizedString("CANCEL_TEXT", comment: "")], buttonStyle: JGActionSheetButtonStyle.cancel)
                self.urlOptionsActionSheet = JGActionSheet(sections: [actionsSection!, cancelSection!])
                self.urlOptionsActionSheet?.outsidePressBlock = {(sheet: JGActionSheet?) in
                    sheet?.dismiss(animated: true)
                }
                self.urlOptionsActionSheet?.delegate = self
            }
            self.urlOptionsActionSheet?.show(in: self.navigationController?.view, animated: true)
        }
    }
    
    func onCardHold(_ gesture: UILongPressGestureRecognizer) {
        
        if let view = gesture.view as? NCHeadlineContainerView {
            let h = view.headline
            let msg = "\(h!.title) \(h!.url!.description)"
            self.showShareSheetWithItems([msg as AnyObject])
        }
    }
    
    
    func showShareSheetWithItems(_ items: [AnyObject]) {
        
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.completionWithItemsHandler = { (activityType: UIActivityType?, completed: Bool, items: [Any]?, error: Error?) -> Void in
            
            var shareOption = ""
            let visibleCell = self.collectionView.visibleCells[0] as? HeadlineCollectionViewCell
            
            if visibleCell?.headline != nil {
                
                let headline = visibleCell!.headline!
                
                switch activityType!.rawValue {
                    
                case "com.apple.UIKit.activity.Message":
                    shareOption = "Message"
                    
                case "com.apple.UIKit.activity.Mail":
                    shareOption = "Mail"
                    
                case "com.apple.mobilenotes.SharingExtension":
                    shareOption = "Add to Notes"
                    
                case "com.apple.UIKit.activity.PostToTwitter":
                    shareOption = "Twitter"
                    
                case "com.linkedin.LinkedIn.ShareExtension":
                    shareOption = "LinkedIn"
                    
                case "com.apple.UIKit.activity.PostToFacebook":
                    shareOption = "Facebook"
                    
                case "com.google.Gmail.ShareExtension":
                    shareOption = "Gmail"
                    
                case "com.tinyspeck.chatlyio.share":
                    shareOption = "Slack"
                    
                case "ph.telegra.Telegraph.Share":
                    shareOption = "Telegram"
                    
                case "net.whatsapp.WhatsApp.ShareExtension":
                    shareOption = "WhatsApp"
                    
                case "com.fogcreek.trello.trelloshare":
                    shareOption = "Trello"
                    
                default:
                    shareOption = "Other: \(activityType!.rawValue)"
                }
                
                let trackedProperties = ["Selected Share Option" : shareOption, "Completed" : completed, "Article" : headline.title, "ArticleID" : headline.id, "Seen on Channel" : self.currentFeed != nil ? String(self.currentFeed!.id) : "", "Url" : "\(headline.url!)", "Date" : "\(Date())"] as [String : Any]
                
                SEGAnalytics.shared().track(SEG_Share_Option_Selected, properties: trackedProperties as [AnyHashable: Any])
            }
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func actionSheet(_ actionSheet: JGActionSheet!, pressedButtonAt indexPath: IndexPath!) {
        
        let visibleCell = self.collectionView.visibleCells[0] as? HeadlineCollectionViewCell
        
        if indexPath.section == 0 {
            
            switch(indexPath.row) {
                
            case 0:
                self.performSegue(withIdentifier: "showWebPageSegue", sender: self)
                break
            case 1:
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                    UIApplication.shared.openURL(visibleCell!.headline!.url! as URL)
                })
                break
            case 2:
                let mailComposer = MFMailComposeViewController()
                mailComposer.mailComposeDelegate = self
                mailComposer.setSubject(visibleCell!.headline!.title)
                mailComposer.setMessageBody("\(visibleCell!.headline!.url!)", isHTML: false)
                
                if MFMailComposeViewController.canSendMail() {
                    self.present(mailComposer, animated: true, completion: nil)
                } else {
                    let sendMailErrorAlert = UIAlertController(title: NSLocalizedString("COULD_NOT_SEND_EMAIL_TITLE", comment: ""), message: NSLocalizedString("COULD_NOT_SEND_EMAIL_MESSAGE", comment: ""), preferredStyle: .alert)
                    let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
                    sendMailErrorAlert.addAction(action)
                    self.present(sendMailErrorAlert, animated: true, completion: nil)
                }
                
                break
            case 3:
                UIPasteboard.general.url = visibleCell!.headline!.url as URL?
                break
            case 4:
                self.showShareSheetWithItems([visibleCell!.headline!.url! as AnyObject])
                break
            default:
                break
            }
        }
        actionSheet.dismiss(animated: true)
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        
        if shareActivationBegan == false {
            DispatchQueue.main.async {
                self.instantShareHighlightView?.removeFromSuperview()
                self.instantShareWarningScheduled?.removeFromSuperview()
                self.instantShareWarningNow?.removeFromSuperview()
                self.instantShareWarningSDescription?.removeFromSuperview()
                self.instantShareWarningNDescription?.removeFromSuperview()
                self.instantShareWarningImageView?.removeFromSuperview()
            }
        }
        
        self.checkIfShared()
    }
    
    //MARK: State preservation
    
    override func encodeRestorableState(with coder: NSCoder) {
        coder.encode(self.currentFeed, forKey: "Current feed")
        coder.encode(self.sharedMode, forKey: "Shared mode")
        coder.encode(self.archiveMode, forKey: "Archive mode")
        coder.encode(self.smartSharedMode, forKey: "Smart shared mode")
        coder.encode(self.bookmarkedMode, forKey: "Bookmarked mode")
        super.encodeRestorableState(with: coder)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        
        self.currentFeed = coder.decodeObject(forKey: "Current feed") as? NCFeed
        self.sharedMode = coder.decodeBool(forKey: "Shared mode")
        self.archiveMode = coder.decodeBool(forKey: "Archive mode")
        self.smartSharedMode = coder.decodeBool(forKey: "Smart shared mode")
        self.bookmarkedMode = coder.decodeBool(forKey: "Bookmarked mode")
        super.decodeRestorableState(with: coder)
    }
    
    // MARK: Transition/Segue Methods
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transitionAnimator = ModalMessageTransition()
        return transitionAnimator
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transitionAnimator = ModalMessageTransition()
        transitionAnimator.isPresenting = true
        return transitionAnimator
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showArticleReaderSegue" {
            
            if let articleViewController = segue.destination as? ArticleViewController {
                articleViewController.currentHeadline = fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
            }
        } else if segue.identifier == "showMessageModalSegue" {
            
            if let messageModalViewController = segue.destination as? MessageModalViewController {
                messageModalViewController.modalPresentationStyle = UIModalPresentationStyle.custom
                messageModalViewController.transitioningDelegate = self
                messageModalViewController.headline = fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
                messageModalViewController.feed = self.currentFeed
                
                if let button = sender as? NCShareButton {
                    messageModalViewController.network = button.buttonType
                }
            }
        } else if segue.identifier == "showWebPageSegue" {
            
            if let webViewController = segue.destination as? WebViewController {
                webViewController.headline = fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
                webViewController.pushedFromACVC = false
                webViewController.pushedFromMVC = false
            }
        } else if segue.identifier == "showAuthSegue" {
            if let webVC = segue.destination as? WebViewController {
                if let url = sender as? URL {
                    
                    webVC.headline = fetchedHeadlines[self.collectionView!.indexPathsForVisibleItems[0].item]
                    webVC.alternateURL = url
                    webVC.message = shareMessage
                    webVC.channel = shareFeed != nil ? String(shareFeed!.id) : "Shared from seen headlines"
                    webVC.immediate = shareInstant
                    webVC.network = shareNetwork
                    webVC.pushedFromMVC = true
                    webVC.callback = {(accounts: [NCSocialAccount], error: Error?) -> Void in
                        
                        if accounts.count > 0 {
                            DispatchQueue.main.async(execute: {
                                NCDataStoreSwift.sharedStore.identifyUser()
                                self.checkIfShared()
                            })
                        } else if error != nil {
                            self.showConnectionWarning()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Error Handling
    
    func showConnectWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_TITLE", comment: ""), message: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action:UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "showAccountsSegue", sender: self)
        }
        let cancelaction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(cancelaction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNetworkWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("NETWORK_ERROR_TITLE", comment: ""), message: NSLocalizedString("NETWORK_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showServerWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("SERVER_ERROR_TITLE", comment: ""), message: NSLocalizedString("SERVER_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showConnectionWarning() {
        
        let alert = UIAlertController(title: NSLocalizedString("CONNECTION_ERROR_TITLE", comment: ""), message: NSLocalizedString("CONNECTION_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showWebPageOnLinkedInError(_ notification: Notification) {
        
        shareMessage = notification.userInfo?["message"] != nil ? (notification.userInfo?["message"] as! String) : ""
        shareInstant = notification.userInfo?["immediate"] as! Bool
        shareNetwork = notification.userInfo?["network"] as? SocialNetwork
        shareFeed = notification.userInfo?["channel"] as? NCFeed
        
        if NCDataStoreSwift.sharedStore.loggedInUser != nil && NCDataStoreSwift.sharedStore.connectedToNetwork() {
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.performSegue(withIdentifier: "showAuthSegue", sender: URL(string: "\(NCRESTAPI)/oauth/linkedin?access_token=\(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken)"))
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NCDataStoreSwift.sharedStore.saveSeenHeadlinesLocally()
        UserDefaults.standard.synchronize()
    }
}
