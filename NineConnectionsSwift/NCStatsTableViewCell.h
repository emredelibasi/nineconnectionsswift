//
//  NCStatsTableViewCell.h
//  NineConnections
//
//  Created by Floris van der Grinten on 19-06-14.
//
//

#import <UIKit/UIKit.h>

@interface NCStatsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UIImageView *interactionIcon;

+ (CGFloat)rowHeight;

@end
