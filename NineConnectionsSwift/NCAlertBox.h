//
//  NCAlertBox.h
//  NineConnections
//
//  Created by Floris van der Grinten on 18-07-14.
//
//

#import <UIKit/UIKit.h>
#import "UIView+GradientBackground.h"
#import "UIView+RoundedCorners.h"
#import "Constants.h"

@interface NCAlertBox : UIView

+ (instancetype)alertBoxOfType:(AlertBoxType)type;
- (void)show;

@end
