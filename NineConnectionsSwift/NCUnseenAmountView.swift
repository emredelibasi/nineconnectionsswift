//
//  NCUnseenAmountView.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 10/25/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCUnseenAmountView: UIView {
    
    var highlighted = false
    var amountLabel = UILabel()
    var badgeColor = UIColor.white

    init(withAmount amount: Int, color: UIColor, fontSize: CGFloat) {
        
        self.badgeColor = color
        super.init(frame: CGRect(x: 0, y: 0, width: 20, height: 12))
        
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.frame.height/2//CGRectGetHeight(self.frame)/2;
        
        //self.amountLabel = UILabel()
        self.amountLabel.text = amount > 0 ? "\(amount)" : " "
        self.amountLabel.font = UIFont(name: "ProximaNova-Regular", size: fontSize)
        self.amountLabel.sizeToFit()
        self.amountLabel.textColor = self.badgeColor
        self.amountLabel.textAlignment = .left
        
        self.addSubview(self.amountLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setHighlighted(highlighted: Bool) {
        
        self.highlighted = highlighted
        if (self.highlighted) {
            self.backgroundColor = UIColor.clear
            self.amountLabel.textColor = UIColor.companyUnreadCountSelected()
        }
        else {
            self.backgroundColor = UIColor.clear
            self.amountLabel.textColor = self.badgeColor
        }
    }

}
