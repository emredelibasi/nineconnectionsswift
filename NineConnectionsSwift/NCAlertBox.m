//
//  NCAlertBox.m
//  NineConnections
//
//  Created by Floris van der Grinten on 18-07-14.
//
//

#import "NCAlertBox.h"

@implementation NCAlertBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (instancetype)alertBoxOfType:(AlertBoxType)type
{
    CGFloat boxHeight = 26.0;
    
    CGRect boxFrame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, boxHeight);
    //CGRect preAnimationBoxFrame = CGRectOffset(boxFrame, 0, -boxHeight);
    
    NCAlertBox *alertBox = [[NCAlertBox alloc] initWithFrame:boxFrame];

    
    [alertBox gradientBackgroundFrom:[UIColor redColor] to:[UIColor colorWithRed:235.0/255.0 green:121.0/255.0 blue:45.0/255.0 alpha:0.9]];
   // [alertBox gradientBackgroundFrom:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:01.0] to:[UIColor whiteColor]];
    
    CGFloat radius = 5.0;
    alertBox.layer.cornerRadius = radius;
    
    UILabel *alertMessageLabel = [UILabel new];

    switch (type) {
        case AlertBoxTypeConnectionErrorTwitter:
            
            break;
            
        case AlertBoxTypePostingMessage:
            
            break;
        default:
            break;
    }
    
    alertMessageLabel.text = @"Error posting message..";
    alertMessageLabel.textColor = [UIColor whiteColor];
    alertMessageLabel.font = [UIFont fontWithName:@"Lato-Italic" size:15.0];
    [alertMessageLabel sizeToFit];
    //alertMessageLabel.frame = CGRectOffset(alertMessageLabel.frame, 30, 2);
    alertMessageLabel.center = alertBox.center;
    [alertBox addSubview:alertMessageLabel];
    //[alertBox setRoundedCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radius:radius];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alertBox disappear];
    });
    
    return alertBox;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self disappear];
}

- (void)show
{
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:self];
}

- (void)disappear
{
    CGRect boxFrame = self.frame;

    [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.3 initialSpringVelocity:0.7 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //self.alpha = 0;
        self.frame = CGRectOffset(boxFrame, 0, -boxFrame.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
