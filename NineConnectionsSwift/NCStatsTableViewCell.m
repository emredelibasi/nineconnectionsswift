//
//  NCStatsTableViewCell.m
//  NineConnections
//
//  Created by Floris van der Grinten on 19-06-14.
//
//

#import "NCStatsTableViewCell.h"

@implementation NCStatsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.title.textColor = [UIColor whiteColor];
    self.subtitle.textColor = [UIColor whiteColor];
    
    self.title.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    self.subtitle.font = [UIFont fontWithName:@"ProximaNova-Light" size:13.0];
    self.backgroundColor = [UIColor clearColor];

    self.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)rowHeight
{
    return 70.0;
}

@end
