//
//  NCSocialStat.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/3/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

enum InteractionType: Int {
    case twitterRetweet
    case twitterReply
    case twitterFavorite
    case linkedInLike
    case linkedInComment
    case bitlyClick
}

class NCSocialStat: NSObject, NSCoding {
    
    var id: Int
    var headlineTitle: String
    var interactionType: InteractionType
    var amount: Int
    var icon: UIImage?
    var displayText: String?
    
    init(id: Int, headlineTitle: String, interaction: InteractionType, amount: Int) {
        
        self.id = id
        self.headlineTitle = headlineTitle
        self.interactionType = interaction
        self.amount = amount
        super.init()
        self.generateIconsForStats()
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "statId")
        aCoder.encode(self.headlineTitle, forKey: "headlineTitle")
        aCoder.encode(self.interactionType.rawValue, forKey: "interactionType")
        aCoder.encode(self.amount, forKey: "amount")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let headlineTitle = aDecoder.decodeObject(forKey: "headlineTitle") as? String else {
                return nil
        }
        
        let id = aDecoder.decodeInteger(forKey: "statId")
        let rawValue = aDecoder.decodeInteger(forKey: "interactionType")
        let amount = aDecoder.decodeInteger(forKey: "amount")
        self.init(id: id, headlineTitle: headlineTitle, interaction: InteractionType(rawValue: rawValue)!, amount: amount)

        self.generateIconsForStats()
        
    }
    
    func generateIconsForStats() {
        var imageName: String
        var message:[String] = []
        
        switch(self.interactionType) {
            
            
        case .twitterFavorite:
            imageName = "favorite-icon"
            message = [NSLocalizedString("FAVORITE_TEXT", comment: ""), NSLocalizedString("FAVORITES_TEXT", comment: "")]
            break
        case .twitterReply:
            imageName = "reply-icon"
            message = [NSLocalizedString("REPLY_TEXT", comment: ""), NSLocalizedString("REPLIES_TEXT", comment: "")]
            break
        case .twitterRetweet:
            imageName = "retweet-icon"
            message = [NSLocalizedString("RETWEET_TEXT", comment: ""), NSLocalizedString("RETWEETS_TEXT", comment: "")]
            break
        case .linkedInComment:
            imageName = "comment-icon"
            message = [NSLocalizedString("COMMENT_TEXT", comment: ""), NSLocalizedString("COMMENTS_TEXT", comment: "")]
            break
        case .linkedInLike:
            imageName = "like-icon"
            message = [NSLocalizedString("LIKE_TEXT", comment: ""), NSLocalizedString("LIKES_TEXT", comment: "")]
            break
        case .bitlyClick:
            imageName = "click"
            message = [NSLocalizedString("CLICK_TEXT", comment: ""), NSLocalizedString("CLICKS_TEXT", comment: "")]
        }
        
        self.icon = UIImage(contentsOfFile: Bundle.main.path(forResource: imageName, ofType: "png")!)
        self.displayText = "\(self.amount) \(message[min(self.amount, 2)-1])"
    }

}
