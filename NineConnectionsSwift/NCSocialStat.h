//
//  NCSocialStat.h
//  NineConnections
//
//  Created by Floris van der Grinten on 28-05-14.
//
//

#import <Foundation/Foundation.h>
#import "NCHeadline.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, InteractionType) {
    TwitterInteractionTypeRetweet,
    TwitterInteractionTypeReply,
    TwitterInteractionTypeFavorite,
    LinkedInInteractionTypeLike,
    LinkedInInteractionTypeComment
};

@interface NCSocialStat : NSObject

@property (nonatomic, strong) NCHeadline *headline;
@property (nonatomic, assign) InteractionType interactionType;
@property (nonatomic, assign) NSInteger amount;
@property (nonatomic, assign) NSInteger postId;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) NSString *displayText;

- (instancetype)initWithHeadline:(NCHeadline *)headline thatHas:(NSInteger)amount ofInteractionType:(InteractionType)interaction;

@end
