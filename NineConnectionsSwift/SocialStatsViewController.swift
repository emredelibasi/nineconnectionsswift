//
//  SocialStatsViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/15/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

class SocialStatsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var allStats: [NCSocialStat] = []
    let loggedInUser = NCDataStoreSwift.sharedStore.loggedInUser
    var operationComplete: Bool = false
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        self.view.backgroundColor = UIColor.companySecondaryBackground()
        self.navigationItem.createCustomTitle(NSLocalizedString("SOCIAL_STATS_TEXT", comment: "").uppercased(), fontSize: oldIphone ? 17 : 20)
        tableView.separatorColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText().withAlphaComponent(0.2) : UIColor.white.withAlphaComponent(0.2)
        
        self.refreshStats()

    }

    func refreshStats() {
        
        self.allStats = []
        
        NCDataStoreSwift.sharedStore.fetchStatsForUser(self.loggedInUser!) { (stats: [NCSocialStat]?, error: Error?) in
            
            self.allStats = stats != nil ? stats! : []
            self.operationComplete = true
            self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if NCDataStoreSwift.sharedStore.retrieveSocialStats().count == 0 {
            self.refreshStats()
        } else {
            self.allStats = NCDataStoreSwift.sharedStore.retrieveSocialStats()
            self.operationComplete = true
            self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return max(self.allStats.count, 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatsCell", for: indexPath)
        let titleLabel = cell.viewWithTag(11) as! UILabel
        let subLabel = cell.viewWithTag(12) as! UILabel
        let imageView = cell.viewWithTag(13) as! UIImageView
        
        titleLabel.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white
        subLabel.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white
        imageView.tintColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white

        if operationComplete == false {
            
            titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 19.0)
            titleLabel.text = NSLocalizedString("PLEASE_WAIT_TEXT", comment: "")
            subLabel.text = NSLocalizedString("LOADING_STATS_TEXT", comment: "")
            
        } else {
            if (self.allStats.count > 0) {
                
                let thisStat = self.allStats[(indexPath as NSIndexPath).row]
                titleLabel.text = thisStat.displayText
                titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 19.0)
                subLabel.text = thisStat.headlineTitle
                imageView.image = thisStat.icon!.withRenderingMode(.alwaysTemplate)
            } else {
                if (NCDataStoreSwift.sharedStore.userAccounts.count > 0) {
                    titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 19.0)
                    titleLabel.text = NSLocalizedString("NO_STATS_MESSAGE", comment: "")
                    titleLabel.isUserInteractionEnabled = false
                    subLabel.text = NSLocalizedString("NO_STATS_SUBMESSAGE", comment: "")
                } else {
                    self.navigationController?.navigationBar.isUserInteractionEnabled = false
                    let alertController = UIAlertController(title: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_TITLE_SSVC", comment: ""), message: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_MESSAGE_SSVC", comment: ""), preferredStyle: .alert)
                    let action = UIAlertAction(title: NSLocalizedString("TAKE_ME_THERE", comment: ""), style: .default, handler: { (action: UIAlertAction) in
                        self.performSegue(withIdentifier: "showSettingsFromStats", sender: self)
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true

                    })
                    let cancel = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler: { (action: UIAlertAction) in
                        titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 19.0)
                        titleLabel.text = NSLocalizedString("NO_STATS_MESSAGE", comment: "")
                        subLabel.text = NSLocalizedString("NO_CONNECTED_ACCOUNTS", comment: "")
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    })
                    alertController.addAction(cancel)
                    alertController.addAction(action)
                    
                    if self.navigationController?.visibleViewController is SocialStatsViewController {
                        self.present(alertController, animated: true, completion: nil)

                    }
                }
            }
        }
        
        return cell
    }
}
