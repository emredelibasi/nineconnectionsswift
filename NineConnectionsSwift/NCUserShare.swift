//
//  NCShare.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 3/17/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCUserShare: NSObject/*, NSCoding */{
    
    var shareId: Int?
    let headlineTitle: String
    let shareDate: Date
    let shareMedium: String
    let postId: String
    let url: String
    let throughBuffer: Bool
    var socialStats: [NCSocialStat] = []
    
    
    init(sharedHeadlineTitle: String, sharedOn: String, withUrl url: String, withPostId postId: String, onDate date: Date, throughBuffer: Bool) {
        self.headlineTitle = sharedHeadlineTitle
        self.shareMedium = sharedOn
        self.postId = postId
        self.shareDate = date
        self.url = url
        self.throughBuffer = throughBuffer
    }
    
//    func encodeWithCoder(aCoder: NSCoder) {
//        
//        aCoder.encodeObject(self.userName, forKey: "userName")
//        aCoder.encodeObject(self.headline, forKey: "headline")
//        aCoder.encodeObject(self.shareDate, forKey: "shareDate")
//        aCoder.encodeObject(self.shareMedium, forKey: "shareMedium")
//        aCoder.encodeObject(self.postId, forKey: "postId")
//        aCoder.encodeObject(self.shareId, forKey: "shareId")
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        
//        self.userName = aDecoder.decodeObjectForKey("userName") as! String
//        self.headline = aDecoder.decodeObjectForKey("headline") as! NCHeadline
//        self.shareDate = aDecoder.decodeObjectForKey("shareDate") as! NSDate
//        self.shareMedium = aDecoder.decodeObjectForKey("shareMedium") as! String
//        self.postId = aDecoder.decodeObjectForKey("postId") as! String
//        self.shareId = aDecoder.decodeObjectForKey("shareId") as? Int
//    }

}
