//
//  SettingsTableViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/3/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var pagingAnimationSwitch: UISwitch!
    @IBOutlet weak var otherAnimationsSwitch: UISwitch!
    @IBOutlet weak var articleLinksSwitch: UISwitch!
    @IBOutlet weak var removePagingLabel: UILabel!
    @IBOutlet weak var goEasyLabel: UILabel!
    @IBOutlet weak var removeArticleSourceLabel: UILabel!
    let loggedInUser = NCDataStoreSwift.sharedStore.loggedInUser
    
    var defaults: UserDefaults?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaults = UserDefaults.standard
        
        self.pagingAnimationSwitch.isOn = defaults!.bool(forKey: DEFAULTS_REMOVE_PAGING_ANIMATION)
        self.otherAnimationsSwitch.isOn = defaults!.bool(forKey: DEFAULTS_REMOVE_ANIMATIONS)
        self.articleLinksSwitch.isOn = defaults!.bool(forKey: DEFAULTS_REMOVE_ARTICLE_LINKS)

        self.pagingAnimationSwitch.addTarget(self, action: #selector(SettingsTableViewController.toggleSwitch(_:)), for: .valueChanged)
        self.otherAnimationsSwitch.addTarget(self, action: #selector(SettingsTableViewController.toggleSwitch(_:)), for: .valueChanged)
        self.articleLinksSwitch.addTarget(self, action: #selector(SettingsTableViewController.toggleSwitch(_:)), for: .valueChanged)

        self.tableView.separatorColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText().withAlphaComponent(0.2) : UIColor.white.withAlphaComponent(0.2)

        removePagingLabel.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white
        goEasyLabel.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white
        removeArticleSourceLabel.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func toggleSwitch(_ sender: UISwitch) {
        
        var toggledKey: String?
        
        if (sender == self.pagingAnimationSwitch) {
            toggledKey = DEFAULTS_REMOVE_PAGING_ANIMATION
        } else if (sender == self.otherAnimationsSwitch) {
            toggledKey = DEFAULTS_REMOVE_ANIMATIONS
        } else if (sender == self.articleLinksSwitch) {
            toggledKey = DEFAULTS_REMOVE_ARTICLE_LINKS
        }
        
        defaults?.set(sender.isOn, forKey: toggledKey!)
        defaults?.synchronize()
    }
    
    func toggleCheckmark(_ sender: UITableViewCell, forDefaultsKey key: String) {
        
        let thisCell = sender
        
        switch (thisCell.accessoryType) {
        case UITableViewCellAccessoryType.none:
            thisCell.accessoryType = UITableViewCellAccessoryType.checkmark
            defaults?.set(true, forKey: key)
            break
        case UITableViewCellAccessoryType.checkmark:
            thisCell.accessoryType = UITableViewCellAccessoryType.none
            defaults?.set(false, forKey: key)
            break
        default:
            break
        }
        defaults?.synchronize()
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        
//        let rowsCount = self.tableView.numberOfRowsInSection(0)
//        let tapToShareInstantlySelected = indexPath.row == rowsCount - 1
//        let dragToShareInstantlySelected = indexPath.row == rowsCount - 2
//        
//        if (tapToShareInstantlySelected || dragToShareInstantlySelected) {
//            let thisCell = tableView.cellForRowAtIndexPath(indexPath)
//            var key: String?
//            
//            if (dragToShareInstantlySelected) { key = DEFAULTS_DRAG_TO_SHARE_INSTANTLY }
//            else if (tapToShareInstantlySelected) { key = DEFAULTS_TAP_TO_SHARE_INSTANTLY }
//            print("\(key) is now \(thisCell)")
//            self.toggleCheckmark(thisCell!, forDefaultsKey: key!)
//        }
        return false
    }
    
}
