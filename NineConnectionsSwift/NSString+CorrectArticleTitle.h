//
//  NSString+CorrectArticleTitle.h
//  NineConnections
//
//  Created by Floris van der Grinten on 18-06-14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (CorrectArticleTitle)

- (NSString *)correctArticleTitle;

@end
