//
//  ArticleReaderFlowLayout.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 2/14/17.
//  Copyright © 2017 Emre Delibasi. All rights reserved.
//

import UIKit

protocol ArticleReaderFlowDelegate {
    
    func collectionView(contentSizeForCollectionView collectionView: UICollectionView) -> CGSize
    
    func collectionView(sizeForItemIn collectionView: UICollectionView) -> CGSize
    
}

class ArticleReaderFlowLayout: UICollectionViewFlowLayout {
    
    var delegate: ArticleReaderFlowDelegate!
//    var dynamicAnimator: UIDynamicAnimator!
    var currentHeadlineNumber = 0
    var windowWidth: CGFloat = 0.0
    var currentTransformFactor:CGFloat = 1.0
    
    private var cache = [UICollectionViewLayoutAttributes]()
    
    override func prepare() {
        
//        dynamicAnimator = UIDynamicAnimator(collectionViewLayout: self)
        windowWidth = collectionView!.frame.width
        
//        let contentSize = self.collectionView!.contentSize
        
//        let items = super.layoutAttributesForElements(in: CGRect(x: 0, y: 0, width: contentSize.width, height: contentSize.height))
        
        if cache.isEmpty {
            
            var xOffset =  [CGFloat](repeating: 0, count: 1)
            let itemWidth = delegate.collectionView(sizeForItemIn: collectionView!).width
            let itemHeight = delegate.collectionView(sizeForItemIn: collectionView!).height
            
            for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
                
                let indexPath = IndexPath(item: item, section: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset[item], y: 0, width: itemWidth, height: itemHeight)
                cache.append(attributes)
                
                xOffset.append(itemWidth * CGFloat(item + 1))
            }
        }
//        
//        if self.dynamicAnimator.behaviors.count == 0 {
//            
//            for (_, item) in items!.enumerated() {
//                let behaviour = UIAttachmentBehavior(item: item, attachedToAnchor: item.center)
//                
//                behaviour.length = 0
//                behaviour.damping = 0.8
//                behaviour.frequency = 1
//                
//                self.dynamicAnimator.addBehavior(behaviour)
//                
//            }
//            
//        }
        
    }
    
    override var collectionViewContentSize: CGSize {
        return delegate.collectionView(contentSizeForCollectionView: collectionView!)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var layoutAttributes = [UICollectionViewLayoutAttributes]()

        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        
        return layoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        var layoutAttributes: UICollectionViewLayoutAttributes?
        
        if !cache.isEmpty {
            layoutAttributes = cache[indexPath.item]
        }

        return layoutAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        
        let contentOffset: CGFloat = collectionView!.contentOffset.x - CGFloat(self.currentHeadlineNumber) * windowWidth
        
        if (collectionView!.contentOffset.x > 0 && windowWidth * CGFloat(collectionView!.numberOfItems(inSection: 0)) > collectionView!.contentOffset.x) {
            
            let indexPath = IndexPath(item: currentHeadlineNumber, section: 0)
            let attributes = layoutAttributesForItem(at: indexPath)
            
            if (contentOffset/windowWidth < CGFloat(self.currentHeadlineNumber + 1)) {
                currentTransformFactor =  1.0 - (contentOffset/windowWidth * 0.4)
                attributes!.transform = CGAffineTransform(scaleX: min(currentTransformFactor, 1.0), y: min(currentTransformFactor, 1.0))
                cache[currentHeadlineNumber] = attributes!
            }
            self.currentHeadlineNumber = Int(round(collectionView!.contentOffset.x / windowWidth))
        }
        
        return true
        
    }
}

