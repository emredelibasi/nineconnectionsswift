//
//  InstantShareLabel.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 7/20/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import Foundation
import UIKit
import UIColor_Hex_Swift


class NCInstantShareLabel: UILabel {
    
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    var dashedLayer: CAShapeLayer?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        self.numberOfLines = 3
        self.font = UIFont(name: "ProximaNova-Semibold", size: oldIphone ? 26 : 29)
        self.textColor = UIColor("#3F4D61")
        //self.setProperties(1.0, borderColor:UIColor.blackColor())
        self.sizeToFit()
        
    }
    
    func setProperties(_ borderWidth: Float, borderColor: UIColor) {
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
        
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: -rect.height/4, left: 20, bottom: 0 , right: 20)
        let ad = UIEdgeInsetsInsetRect(rect, insets)
        super.drawText(in: ad)
    }
    
    func addDashedBorder(color: UIColor) {
        
        let color = color.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 1, y: 1, width: frameSize.width-2, height: frameSize.height-2)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 4).cgPath
        
        self.dashedLayer = shapeLayer
        
        self.layer.addSublayer(self.dashedLayer!)
        
    }

    
}



