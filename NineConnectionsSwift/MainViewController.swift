//
//  MainViewController.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/8/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import JGActionSheet
import MessageUI
import KVOController
import CoreData
import Analytics
import UIColor_Hex_Swift

class MainViewController: UIViewController, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate, JGActionSheetDelegate, UIDynamicAnimatorDelegate, MFMailComposeViewControllerDelegate, ShareButtonDelegate {
    
    var currentFeed: NCFeed?
    var archiveMode = false
    var sharedMode = false
    var smartSharedMode = false
    var bookmarkedMode = false
    var pagingAnimationEnabled = false
    var headlineScrollView: UIScrollView?
    var animator: UIDynamicAnimator?
    var instantShareHighlightView: UIView?
    var instantShareWarningScheduled: NCInstantShareLabel?
    var instantShareWarningNow: NCInstantShareLabel?
    var instantShareWarningNDescription: UILabel?
    var instantShareWarningSDescription: UILabel?
    var instantShareWarningImageView: UIImageView?
    var currentHeadlineContainerView: NCHeadlineContainerView?
    var currentHeadlineNumber = 0
    var endingMotivationalMessage: UILabel?
    var statsLabel: UILabel?
    var endingMotivationalMessageFrame: CGRect?
    var statsLabelFrame: CGRect?
    var scrollViewFrame: CGRect?
    var URLOptionsActionSheet: JGActionSheet?
    var statsTableView: UITableView?
    var allStats: [NCSocialStat] = []
    var windowWidth: CGFloat?
    var windowHeight: CGFloat?
    var currentTransformFactor: CGFloat?
    var shareButtonY: CGFloat?
    var shareButtons: [NCShareButton]?
    var headlineContainerViews: [NCHeadlineContainerView]?
    var noAvatarImage: UIImage?
    var headlineContainerViewHeight: CGFloat?
    var fetchedHeadlines: [NCHeadline] = []
    var endingMotivationalMessageCenter: CGPoint?
    var accountScreenRemainingBackgroundColor: UIColor?
    var company: NCCompany?
    let loggedInUser = NCDataStoreSwift.sharedStore.loggedInUser
    //share properties
    var shareMessage = ""
    var shareInstant = false
    var shareFeed: NCFeed?
    var shareNetwork: SocialNetwork?
    var offsetForPushNotification = CGPoint(x: 0, y: 0)
    
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Constants
        
        currentTransformFactor = 1.0
        windowWidth = self.view.bounds.size.width
        windowHeight = self.view.frame.size.height
        accountScreenRemainingBackgroundColor = UIColor.companyBackground()
        self.animator = UIDynamicAnimator(referenceView: self.view)
        self.animator?.delegate = self
        
        // Launch operations
        
        headlineContainerViews = [NCHeadlineContainerView]()
        headlineContainerViewHeight = NCHeadlineContainerView.headlineContainerHeight()
        shareButtonY = headlineContainerViewHeight! - 80

        self.customAppearance()
        self.initializeShareButtons()
        self.checkDefaults()
        
        DispatchQueue.once(token: "once") {
            for b in self.shareButtons! { b.isUserInteractionEnabled = false }
            self.headlineScrollView?.isUserInteractionEnabled = false
            if self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false {
                self.onLaunchUsecueAnimation()
            } else {
                self.shareButtonShake()
            }
        }

        NotificationCenter.default.addObserver(self, selector: #selector(checkDefaults), name: UserDefaults.didChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkIfShared), name: NSNotification.Name(rawValue: "MessagePosted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showConnectWarning(_:)), name: NSNotification.Name(rawValue: "NoAccount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNetworkWarning(_:)), name: NSNotification.Name(rawValue: "NoInternet"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showServerWarning(_:)), name: NSNotification.Name(rawValue: "ServerError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showWebPageOnLinkedInError(_:)), name: NSNotification.Name(rawValue: "LinkedInError"), object: nil)
        
        self.view.clipsToBounds = true
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        
        if self.navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) == true {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
        
        self.refreshHeadlines()
        self.allStats = [NCSocialStat]()
        self.allStats = NCDataStoreSwift.sharedStore.retrieveSocialStats()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.checkIfShared()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.barTintColor = UIColor.companyInnerNavBar()
        
        if !(self.navigationItem.titleView is NCSeenCountView) {
            let title: String
            if self.archiveMode == true {title = NSLocalizedString("MAIN_NAV_BAR_SEEN_TITLE", comment: "")}
            else if self.sharedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_SHARED_TITLE", comment: "")}
            else if self.smartSharedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_SMART_SHARED_TITLE", comment: "")}
            else if self.bookmarkedMode == true {title =  NSLocalizedString("MAIN_NAV_BAR_BOOKMARKED_TITLE", comment: "")}
            else {title = self.currentFeed!.name}
            
            self.navigationItem.createCustomTitle(title, fontSize: oldIphone ? 17 : 20)
        }
    }
    
    //MARK: Initialization & launch
    
    func initializeHeadlineContainers() {
        
        for b in shareButtons! {
            b.shareInteractionEnabled = true
        }

        self.headlineScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: windowWidth!, height: headlineContainerViewHeight!))
        self.scrollViewFrame = self.headlineScrollView?.frame
        self.headlineScrollView?.isPagingEnabled = true
        self.headlineScrollView?.showsHorizontalScrollIndicator = false
        self.headlineScrollView?.showsVerticalScrollIndicator = false
        self.view.insertSubview(self.headlineScrollView!, at: 0)
        self.headlineScrollView?.contentSize = CGSize(width: windowWidth! * CGFloat(fetchedHeadlines.count + 1), height: headlineContainerViewHeight!)
        self.headlineScrollView?.autoresizingMask = .flexibleBottomMargin
        self.headlineScrollView?.delegate = self
        self.headlineScrollView?.bounces = true
        self.headlineScrollView?.clipsToBounds = true
        
        for headline in fetchedHeadlines {
            if headline.url != nil {
                
                let headlineView = NCHeadlineContainerView(withHeadline: headline , width: windowWidth!)
                
                headlineContainerViews?.append(headlineView)
                headlineView.frame = CGRect(x: windowWidth!*CGFloat(self.headlineScrollView!.subviews.count), y: 0, width: headlineView.frame.width, height: headlineView.frame.height)
                
                let cardHoldRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onCardHold(_:)))
                cardHoldRecognizer.minimumPressDuration = 0.8
                headlineView.addGestureRecognizer(cardHoldRecognizer)
                
                let linkHoldRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(showURLOptionsActionSheet(_:)))
                linkHoldRecognizer.minimumPressDuration = 0.4
                headlineView.urlButton?.addGestureRecognizer(linkHoldRecognizer)
                headlineView.gestureBlock = {
                    self.showFullArticleText(self)
                }
                self.headlineScrollView?.addSubview(headlineView)
            }
        }
        
        self.instantShareHighlightView = UIView(frame: CGRect(x: 0, y: 0, width: windowWidth!, height: headlineContainerViewHeight!-160))
        self.instantShareHighlightView?.backgroundColor = UIColor.white

        self.instantShareWarningNow = NCInstantShareLabel()
        self.instantShareWarningNow?.frame = CGRect(x: 11, y: 11, width: windowWidth!-22, height: (2/5)*instantShareHighlightView!.frame.height)
        self.instantShareWarningNow?.text = NSLocalizedString("SHARE_NOW_TEXT", comment: "")
        self.instantShareWarningNow?.textAlignment = .center
        self.instantShareWarningNow?.backgroundColor = UIColor.white
        self.instantShareWarningNow?.addDashedBorder(color: UIColor("#3F4D61"))
        
        self.instantShareWarningNDescription = UILabel()
        self.instantShareWarningNDescription?.text = NSLocalizedString("SHARE_NOW_DESCRIPTION_TEXT", comment: "")
        self.instantShareWarningNDescription?.textAlignment = .center
        self.instantShareWarningNDescription?.textColor = UIColor.white
        self.instantShareWarningNDescription?.backgroundColor = UIColor.clear
        self.instantShareWarningNDescription?.frame = CGRect(x: 70, y: 5, width: windowWidth!-140, height: 50)
        self.instantShareWarningNDescription?.center = CGPoint(x: windowWidth!/2, y: (1/5)*instantShareHighlightView!.frame.height + (oldIphone ? 25 : 15))
        self.instantShareWarningNDescription?.font = UIFont(name: "ProximaNova-Regular", size: oldIphone ? 14 : 15)
        self.instantShareWarningNDescription?.numberOfLines = 4

        self.instantShareWarningScheduled = NCInstantShareLabel()
        self.instantShareWarningScheduled?.frame = CGRect(x: 11, y: (2/5)*instantShareHighlightView!.frame.height + 19, width: windowWidth!-22, height: (3/5)*instantShareHighlightView!.frame.height)
        self.instantShareWarningScheduled?.text = NSLocalizedString("SHARE_SMART_TEXT", comment: "")
        self.instantShareWarningScheduled?.textAlignment = .center
        self.instantShareWarningScheduled?.backgroundColor = UIColor.white
        self.instantShareWarningScheduled?.addDashedBorder(color: UIColor("#3F4D61"))
        
        let shareImage = UIImage(named: "smart_share")
        self.instantShareWarningImageView = UIImageView(image: shareImage)
        self.instantShareWarningImageView?.frame = CGRect(x: windowWidth!/2 - 28.5, y: (2/5)*instantShareHighlightView!.frame.height + 54, width: 57, height: 32)
        
        self.instantShareWarningSDescription = UILabel()
        self.instantShareWarningSDescription?.text = NSLocalizedString("SHARE_SMART_DESCRIPTION_TEXT", comment: "")
        self.instantShareWarningSDescription?.textAlignment = .center
        self.instantShareWarningSDescription?.textColor = UIColor.white
        self.instantShareWarningSDescription?.backgroundColor = UIColor.clear
        self.instantShareWarningSDescription?.frame = CGRect(x: 70, y: (3.5/5)*instantShareHighlightView!.frame.height + 10, width: windowWidth!-140, height: 80)
        self.instantShareWarningSDescription?.center = CGPoint(x: windowWidth!/2, y: (3.5/5)*instantShareHighlightView!.frame.height + (oldIphone ? 33 : 23))
        self.instantShareWarningSDescription?.font = UIFont(name: "ProximaNova-Regular", size: oldIphone ? 14 : 15)
        self.instantShareWarningSDescription?.numberOfLines = 4
        
        self.headlineScrollView?.contentOffset = offsetForPushNotification
    }
    
    func initializeShareButtons() {
        
        shareButtons = [NCShareButton]()
        
        let twitterShareButton = NCShareButton(buttonType: .twitter)
        self.view.addSubview(twitterShareButton!)
        shareButtons?.append(twitterShareButton!)
        
        let linkedInShareButton = NCShareButton(buttonType: .linkedIn)
        self.view.addSubview(linkedInShareButton!)
        shareButtons?.append(linkedInShareButton!)
        
        let amountOfShareButtons = shareButtons!.count
        for i in 0 ..< amountOfShareButtons {
            let thisButton = shareButtons![i]
            thisButton.delegate = self
            
            let spaceBetweenShareButtons:CGFloat = (windowWidth! + thisButton.frame.width) / CGFloat(amountOfShareButtons+1)
            thisButton.centerPosition = CGPoint(x: (spaceBetweenShareButtons * CGFloat(i+1)) - (thisButton.frame.width/2), y: shareButtonY!)
            thisButton.center = thisButton.centerPosition
            thisButton.dynamicsAnimator = self.animator
            thisButton.shareInteractionEnabled = false
        }
    }
    
    func initializeSeenCounter() {
        
        let counterView = NCSeenCountView(articleCount: fetchedHeadlines.count-1, excistingTitle: self.navigationItem, fontSize: oldIphone ? 12.5 : 14)
        
        if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
            self.kvoController.observe(counterView, keyPath: "lowestCount", options: .new, block: { (observer: Any?, countView: Any, change: [String : Any]) in
                if let count = change["new"] as? Int {
                    let lowestCount = count
                    
                    let newKey = self.fetchedHeadlines.count - lowestCount - 1
                    let thisHeadline = self.fetchedHeadlines[newKey]
                    var possibleFeedHeadlines = NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id]
                    
                    if (possibleFeedHeadlines != nil) { possibleFeedHeadlines!.insert(thisHeadline, at: 0)
                        NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id] = possibleFeedHeadlines
                    }
                    else {
                        var newHeadlinesSetForCurrentFeed = [NCHeadline]()
                        newHeadlinesSetForCurrentFeed.append(thisHeadline)
                        NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id] = newHeadlinesSetForCurrentFeed
                    }
                    
                    if NCDataStoreSwift.sharedStore.sequentialSeenHeadlines.contains(thisHeadline) == false {
                        NCDataStoreSwift.sharedStore.sequentialSeenHeadlines.insert(thisHeadline, at: 0)
                    }
                    
                    self.currentFeed!.unseenAmount = lowestCount
                    for feed in NCDataStoreSwift.sharedStore.allFeeds {
                        if feed.hash == self.currentFeed!.hash {
                            feed.unseenAmount = self.currentFeed!.unseenAmount
                        }
                    }
                }
            })
        }
        counterView?.count = fetchedHeadlines.count - 1
        let navTitleStaticFrame:CGRect = self.navigationItem.titleView!.frame
        counterView?.frame = navTitleStaticFrame
        counterView?.countLabel.textColor = /*self.company?.mainColor != nil ?  self.company?.mainColor! : */UIColor.white
        self.navigationItem.titleView = counterView
    }
    
    func initializeStatsTableView() {
        
        self.statsTableView = UITableView(frame: CGRect(x: CGFloat(fetchedHeadlines.count) * windowWidth!, y: self.statsLabel!.frame.maxY+2, width: windowWidth!, height: windowHeight!-self.statsLabel!.frame.origin.y - 86), style: .plain)
        self.headlineScrollView?.addSubview(self.statsTableView!)
        self.statsTableView!.backgroundColor = UIColor.clear
        self.statsTableView!.separatorColor = UIColor(white: 1.0, alpha: 0.2)
        let cellNib = UINib(nibName: "NCStatsTableViewCell", bundle: Bundle.main)
        self.statsTableView!.register(cellNib, forCellReuseIdentifier: "EndStatsCell")
        self.statsTableView!.rowHeight = NCStatsTableViewCell.rowHeight()
        self.statsTableView?.separatorColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText().withAlphaComponent(0.2) : UIColor.white.withAlphaComponent(0.2)
        self.statsTableView!.delegate = self
        self.statsTableView!.dataSource = self
        self.statsTableView!.reloadData()
    }
    
    func startInterfaceSetup() {
        
        if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
            for h in fetchedHeadlines {
                h.title = h.title.correctArticleTitle()
            }
        }
        self.initializeHeadlineContainers()
        self.setupEndScreen()
        self.initializeStatsTableView()
        
        if fetchedHeadlines.count > 0 {
            accountScreenRemainingBackgroundColor = UIColor.lightGradient()
            self.currentHeadlineContainerView = headlineContainerViews?[self.currentHeadlineNumber]
            
            if self.navigationItem.titleView is UILabel {
                if (self.archiveMode == false && self.sharedMode == false && self.smartSharedMode == false && self.bookmarkedMode == false) {
                    self.initializeSeenCounter()
                }
            }
        }
        self.checkIfShared()
    }
    
    func refreshHeadlines() {
        
        if self.archiveMode == true {
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.sequentialSeenHeadlines {
                if !fetchedHeadlines.contains(h) && !h.shared {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.seenAt.timeIntervalSince1970 > $1.seenAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            self.startInterfaceSetup()
            
        } else if self.sharedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.retrievePostedHeadlines()  {
                if !fetchedHeadlines.contains(h) && ((h.sharedInstantlyOnTwitter == true || h.sharedInstantlyOnLinkedIn == true) && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.sharedAt.timeIntervalSince1970 > $1.sharedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))

            self.startInterfaceSetup()
            
        } else if self.smartSharedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            
            for h in NCDataStoreSwift.sharedStore.retrievePostedHeadlines() {
                if !fetchedHeadlines.contains(h) && ((h.sharedWithScheduleOnTwitter == true || h.sharedWithScheduleOnLinkedIn == true) && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.sharedAt.timeIntervalSince1970 > $1.sharedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            self.startInterfaceSetup()
            
        } else if self.bookmarkedMode == true {
            
            fetchedHeadlines = [NCHeadline]()
            for h in NCDataStoreSwift.sharedStore.retrieveBookmarkedHeadlines() {
                if !fetchedHeadlines.contains(h) && (h.isBookmarked == true && h.url != nil) {
                    fetchedHeadlines.append(h)
                }
            }
            
            fetchedHeadlines = fetchedHeadlines.sorted(by: { $0.bookmarkedAt.timeIntervalSince1970 > $1.bookmarkedAt.timeIntervalSince1970 })
            fetchedHeadlines = Array(fetchedHeadlines.prefix(40))
            
            self.startInterfaceSetup()
            
        } else {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let seenHeadlines = NCDataStoreSwift.sharedStore.seenHeadlinesByFeed[self.currentFeed!.id]
            if seenHeadlines != nil {
                for seen in seenHeadlines! {
                    self.fetchedHeadlines = self.fetchedHeadlines.filter({ (headline: NCHeadline) -> Bool in
                        headline.id != seen.id
                    })
                }
                self.fetchedHeadlines = NCDataStoreSwift.sharedStore.sortHeadlinesById(self.fetchedHeadlines)
            } else {
                self.fetchedHeadlines = NCDataStoreSwift.sharedStore.sortHeadlinesById(self.fetchedHeadlines)
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.startInterfaceSetup()
            })
        }
    }
    
    func checkDefaults() {
        
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: DEFAULTS_REMOVE_PAGING_ANIMATION) {
            self.pagingAnimationEnabled = false
        } else {
            self.pagingAnimationEnabled = true
        }
    }
    
    func customAppearance() {
        
        self.view.backgroundColor = UIColor.companySecondaryBackground()
        self.navigationItem.createCustomTitle(self.navigationItem.title, fontSize: oldIphone ? 17 : 20)
    }
    
    func shareButtonShake() {
        
        var a = 0.0
        for b in shareButtons! {
            a += 10.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.01 * a * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    b.center = CGPoint(x: b.centerPosition.x - 25 + CGFloat(a), y: b.centerPosition.y - CGFloat(a))
                    }, completion: { (finished: Bool) -> Void in
                        b.addShareButtonsSnappingBehavior()
                })
            })
        }
    }
    
    func setupEndScreen() {
        
        self.endingMotivationalMessage = UILabel()
        let endingMsgPadding: CGFloat = 40.0
        self.endingMotivationalMessage!.frame = CGRect(x: 0, y: 50, width: self.windowWidth!-endingMsgPadding, height: 0)
        self.endingMotivationalMessage!.numberOfLines = 0
        
        let attributes = [NSFontAttributeName: UIFont(name: "ProximaNova-Regular", size: 24)!, NSForegroundColorAttributeName : (loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white)]
        if self.archiveMode == true {
            self.endingMotivationalMessage!.attributedText = NSAttributedString(string: self.fetchedHeadlines.count > 0 ? NSLocalizedString("SEEN_ALL_READ_ARTICLES_MOTIVATINAL_MESSAGE", comment: ""): NSLocalizedString("NO_MORE_NEWS_MOTIVATINAL_MESSAGE", comment: ""), attributes: attributes)
        } else if self.sharedMode == true || self.smartSharedMode == true {
            self.endingMotivationalMessage!.attributedText = NSAttributedString(string: self.fetchedHeadlines.count > 0 ? NSLocalizedString("SEEN_ALL_SHARED_ARTICLES_MOTIVATINAL_MESSAGE", comment: "") : NSLocalizedString("NO_SHARED_ARTICLES_MOTIVATINAL_MESSAGE", comment: ""), attributes: attributes)
        }  else if self.bookmarkedMode == true {
            self.endingMotivationalMessage!.attributedText = NSAttributedString(string: self.fetchedHeadlines.count > 0 ? NSLocalizedString("SEEN_ALL_BOOKMARKED_ARTICLES_MOTIVATINAL_MESSAGE", comment: "") : NSLocalizedString("NO_BOOKMARKED_ARTICLES_MOTIVATINAL_MESSAGE", comment: ""), attributes: attributes)
        } else {
            self.endingMotivationalMessage!.attributedText = NSAttributedString(string: self.fetchedHeadlines.count > 0 ? NSLocalizedString("SEEN_ALL_ARTICLES_IN_CHANNEL_MOTIVATINAL_MESSAGE", comment: "") : NSLocalizedString("NO_MORE_NEWS_MOTIVATINAL_MESSAGE", comment: ""), attributes: attributes)
        }
        
        self.endingMotivationalMessage!.sizeToFit()
        self.endingMotivationalMessageFrame = CGRect(x: self.windowWidth! * CGFloat(self.fetchedHeadlines.count) + endingMsgPadding/2, y: self.endingMotivationalMessage!.frame.minY, width: self.windowWidth!-endingMsgPadding, height: self.endingMotivationalMessage!.frame.height)
        
        var beforeAnimationFrame: CGRect = self.endingMotivationalMessageFrame!
        beforeAnimationFrame.origin.x = self.windowWidth! * CGFloat(self.fetchedHeadlines.count)
        self.endingMotivationalMessage?.frame = beforeAnimationFrame
        self.headlineScrollView?.addSubview(self.endingMotivationalMessage!)
        
        self.statsLabel = UILabel()
        self.statsLabel!.frame = CGRect(x: 0, y: self.endingMotivationalMessage!.frame.maxY+50, width: self.windowWidth!-endingMsgPadding, height: 0)
        self.statsLabel!.numberOfLines = 1
        self.statsLabel!.font = UIFont(name: "ProximaNova-Light", size: 16.0)
        self.statsLabel!.textColor = loggedInUser?.company?.code == "nnbank" ? UIColor.companyRowText() : UIColor.white

        self.statsLabel!.text = NSLocalizedString("SOCIAL_STATS_TEXT", comment: "").uppercased()
        self.statsLabel!.sizeToFit()
        self.statsLabelFrame = CGRect(x: self.windowWidth! * CGFloat(self.fetchedHeadlines.count) + endingMsgPadding/2, y: self.statsLabel!.frame.minY, width: self.windowWidth!-endingMsgPadding, height: self.statsLabel!.frame.height)
        
        var beforeAnimationStatsLabelFrame: CGRect = self.statsLabelFrame!
        beforeAnimationStatsLabelFrame.origin.x = self.windowWidth! * CGFloat(self.fetchedHeadlines.count + 1)
        
        self.statsLabel?.frame = beforeAnimationStatsLabelFrame
        self.headlineScrollView?.addSubview(self.statsLabel!)
        
        if (self.fetchedHeadlines.count == 0) {
            self.endingMotivationalMessage?.frame = self.endingMotivationalMessageFrame!
            self.statsLabel?.frame = self.statsLabelFrame!
            for b in self.shareButtons! {b.isHidden = true}
            self.setScrollViewFrameForEndScreen()
        }
    }
    
    func onLaunchUsecueAnimation() {
    
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                self.headlineScrollView?.contentOffset = CGPoint(x: -20, y: 0)
                }, completion: { (finished: Bool) -> Void in
                    self.shareButtonShake()
                    
                    UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: .curveLinear, animations: { () -> Void in
                        self.headlineScrollView?.contentOffset = CGPoint(x: 0, y: 0)
                        }, completion: { (finished: Bool) -> Void in
                            for b in self.shareButtons! {b.shareInteractionEnabled = true }
                            for b in self.shareButtons! {b.isUserInteractionEnabled = true }
                            self.headlineScrollView?.isUserInteractionEnabled = true
                })
            })
        }
    }
    
    //MARK: State preservation
    
    override func encodeRestorableState(with coder: NSCoder) {
        coder.encode(self.currentFeed, forKey: "Current feed")
        coder.encode(self.sharedMode, forKey: "Shared mode")
        coder.encode(self.archiveMode, forKey: "Archive mode")
        coder.encode(self.smartSharedMode, forKey: "Smart shared mode")
        coder.encode(self.bookmarkedMode, forKey: "Bookmarked mode")
        super.encodeRestorableState(with: coder)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        
        self.currentFeed = coder.decodeObject(forKey: "Current feed") as? NCFeed
        self.sharedMode = coder.decodeBool(forKey: "Shared mode")
        self.archiveMode = coder.decodeBool(forKey: "Archive mode")
        self.smartSharedMode = coder.decodeBool(forKey: "Smart shared mode")
        self.bookmarkedMode = coder.decodeBool(forKey: "Bookmarked mode")
        super.decodeRestorableState(with: coder)
    }
    
    //MARK: Table View Configuration
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(self.allStats.count, 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EndStatsCell", for: indexPath) as! NCStatsTableViewCell

        if (self.allStats.count > 0) {
            let thisStat = self.allStats[(indexPath as NSIndexPath).row]
            cell.title.text = thisStat.displayText
            cell.subtitle.text = thisStat.headlineTitle
            cell.interactionIcon.image = thisStat.icon!.withRenderingMode(.alwaysTemplate)
            cell.interactionIcon.tintColor = UIColor.white
        } else {
            cell.title.font = UIFont(name: cell.title.font.fontName, size: 19.0)
            cell.title.text = NSLocalizedString("NO_STATS_MESSAGE", comment: "")
            cell.subtitle.text = NSLocalizedString("NO_STATS_SUBMESSAGE", comment: "")
        }
        
        return cell
    }
    
    //MARK: URL bar methods
    @IBAction func showFullArticleText(_ sender: Any) {
        
        if self.currentHeadlineContainerView?.headline?.articleText == nil {

            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            if self.currentFeed != nil && self.currentHeadlineContainerView?.headline != nil {
                NCDataStoreSwift.sharedStore.fetchFullArticleTextFromHeadline(self.currentHeadlineContainerView!.headline!, inFeed: self.currentFeed!, withSuccess: { (succeeded: Bool, error: Error?) -> Void in
                    
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false

                        if (succeeded && self.currentHeadlineContainerView!.headline!.articleText!.characters.count >= 400) {
                            self.performSegue(withIdentifier: "ShowFullArticle", sender: sender)
                        } else if (error as? NSError)?.code == -1009 {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "NoInternet"), object: self)
                        } else {
                            self.performSegue(withIdentifier: "ShowWebPage", sender: sender)
                        }
                    }
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.performSegue(withIdentifier: "ShowWebPage", sender: sender)
                })
            }
        } else if self.currentHeadlineContainerView!.headline!.articleText!.characters.count < 400 {
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSegue(withIdentifier: "ShowWebPage", sender: sender)
            })
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSegue(withIdentifier: "ShowFullArticle", sender: sender)
            })
        }
    }
    
    func showURLOptionsActionSheet(_ gesture: UILongPressGestureRecognizer) {
        
        if gesture.state == UIGestureRecognizerState.began {
            if self.URLOptionsActionSheet == nil {
                
                let actionsSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: [NSLocalizedString("OPEN_WEBPAGE_TEXT", comment: ""), NSLocalizedString("OPEN_URL_IN_SAFARI_TEXT", comment: ""), NSLocalizedString("EMAIL_URL_TEXT", comment: ""), NSLocalizedString("COPY_URL_TEXT", comment: ""), NSLocalizedString("MORE_SHARING_OPTIONS_TEXT", comment: "")], buttonStyle: JGActionSheetButtonStyle.default)
                let cancelSection = JGActionSheetSection(title: nil, message: nil, buttonTitles: [NSLocalizedString("CANCEL_TEXT", comment: "")], buttonStyle: JGActionSheetButtonStyle.cancel)
                self.URLOptionsActionSheet = JGActionSheet(sections: [actionsSection!, cancelSection!])
                self.URLOptionsActionSheet?.outsidePressBlock = {(sheet: JGActionSheet?) in
                    sheet?.dismiss(animated: true)
                }
                self.URLOptionsActionSheet?.delegate = self
            }
            self.URLOptionsActionSheet?.show(in: self.navigationController?.view, animated: true)
        }
    }
    
    func onCardHold(_ gesture: UILongPressGestureRecognizer) {
        
        if let view = gesture.view as? NCHeadlineContainerView {
            let h = view.headline
            let msg = "\(h!.title) \(h!.url!.description)"
            self.showShareSheetWithItems([msg as AnyObject])
        }
    }

    func showShareSheetWithItems(_ items: [AnyObject]) {
        
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.completionWithItemsHandler = { (activityType: UIActivityType?, completed: Bool, items: [Any]?, error: Error?) -> Void in
            
            var shareOption = ""
            
            if self.currentHeadlineContainerView?.headline != nil {
                
                let headline = self.currentHeadlineContainerView!.headline!
                
                switch activityType!.rawValue {
                    
                    case "com.apple.UIKit.activity.Message":
                        shareOption = "Message"
                        
                    case "com.apple.UIKit.activity.Mail":
                        shareOption = "Mail"
                        
                    case "com.apple.mobilenotes.SharingExtension":
                        shareOption = "Add to Notes"
                        
                    case "com.apple.UIKit.activity.PostToTwitter":
                        shareOption = "Twitter"
                        
                    case "com.linkedin.LinkedIn.ShareExtension":
                        shareOption = "LinkedIn"
                        
                    case "com.apple.UIKit.activity.PostToFacebook":
                        shareOption = "Facebook"
                        
                    case "com.google.Gmail.ShareExtension":
                        shareOption = "Gmail"
                        
                    case "com.tinyspeck.chatlyio.share":
                        shareOption = "Slack"
                        
                    case "ph.telegra.Telegraph.Share":
                        shareOption = "Telegram"
                        
                    case "net.whatsapp.WhatsApp.ShareExtension":
                        shareOption = "WhatsApp"
                        
                    case "com.fogcreek.trello.trelloshare":
                        shareOption = "Trello"
                        
                    default:
                        shareOption = "Other: \(activityType!.rawValue)"
                }
                
                let trackedProperties = ["Selected Share Option" : shareOption, "Completed" : completed, "Article" : headline.title, "ArticleID" : headline.id, "Seen on Channel" : self.currentFeed != nil ? String(self.currentFeed!.id) : "", "Url" : "\(headline.url!)", "Date" : "\(Date())"] as [String : Any]
                
                SEGAnalytics.shared().track(SEG_Share_Option_Selected, properties: trackedProperties as [AnyHashable: Any])
            }
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func actionSheet(_ actionSheet: JGActionSheet!, pressedButtonAt indexPath: IndexPath!) {
        
        if indexPath.section == 0 {
            
            switch(indexPath.row) {
                
                case 0:
                    self.performSegue(withIdentifier: "ShowWebPage", sender: self)
                    break
                case 1:
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                        UIApplication.shared.openURL(self.currentHeadlineContainerView!.headline!.url! as URL)
                    })
                    break
                case 2:
                    let mailComposer = MFMailComposeViewController()
                    mailComposer.mailComposeDelegate = self
                    mailComposer.setSubject(self.currentHeadlineContainerView!.headline!.title)
                    mailComposer.setMessageBody("\(self.currentHeadlineContainerView!.headline!.url!)", isHTML: false)

                    if MFMailComposeViewController.canSendMail() {
                        self.present(mailComposer, animated: true, completion: nil)
                    } else {
                        let sendMailErrorAlert = UIAlertController(title: NSLocalizedString("COULD_NOT_SEND_EMAIL_TITLE", comment: ""), message: NSLocalizedString("COULD_NOT_SEND_EMAIL_MESSAGE", comment: ""), preferredStyle: .alert)
                        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
                        sendMailErrorAlert.addAction(action)
                        self.present(sendMailErrorAlert, animated: true, completion: nil)
                    }
                    
                    break
                case 3:
                    UIPasteboard.general.url = self.currentHeadlineContainerView!.headline!.url as URL?
                    break
                case 4:
                    self.showShareSheetWithItems([self.currentHeadlineContainerView!.headline!.url! as AnyObject])
                    break
                default:
                    break
            }
        }
        actionSheet.dismiss(animated: true)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: ScrollView Delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for b in shareButtons! { b.shareInteractionEnabled = false }
        
        var contentOffset: CGFloat = scrollView.contentOffset.x - CGFloat(self.currentHeadlineNumber) * windowWidth!
        if (scrollView.contentOffset.x > 0 && windowWidth! * CGFloat(fetchedHeadlines.count) > scrollView.contentOffset.x) {
            let currentHeadlineContainer = headlineContainerViews![self.currentHeadlineNumber]
            
            if (contentOffset/windowWidth! < CGFloat(self.currentHeadlineNumber + 1)) {
                currentTransformFactor = self.pagingAnimationEnabled == true ? 1.0 - (contentOffset/windowWidth! * 0.4) : 1.0
                currentHeadlineContainer.transform = CGAffineTransform(scaleX: min(currentTransformFactor!, 1.0), y: min(currentTransformFactor!, 1.0))
            }
            self.currentHeadlineNumber = Int(scrollView.contentOffset.x / windowWidth!)
        }
        if (scrollView.contentOffset.x > windowWidth! * CGFloat(fetchedHeadlines.count - 1) && scrollView.contentOffset.x < windowWidth! * CGFloat(fetchedHeadlines.count)) {
            self.endingMotivationalMessage?.center = CGPoint(x: windowWidth! + self.endingMotivationalMessageFrame!.midX - contentOffset, y: self.endingMotivationalMessage!.center.y)
            
            self.statsLabel!.center = CGPoint(x: windowWidth!*1.6 + self.statsLabelFrame!.midX - contentOffset*1.6, y: self.statsLabel!.center.y)
            
            var j: CGFloat = 1
            if contentOffset > windowWidth! {
                contentOffset -= windowWidth!
            }
            for b in shareButtons! {
                b.dynamicsAnimator.removeAllBehaviors()
                b.center = CGPoint(x: b.centerPosition.x-(max(contentOffset-120,0)*1.0*max(1, j*0.4)), y: b.centerPosition.y+(max(contentOffset-100, 0)*0.65*max(j*0.8,1)))
                j += 3
            }
        }
        
        self.headlineScrollView?.clipsToBounds = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.headlineScrollView {
            self.headlineScrollView?.clipsToBounds = true
            
            for b in shareButtons! { b.shareInteractionEnabled = true }
            
            if self.currentHeadlineNumber >= 0 && headlineContainerViews!.count > 0 {
                self.currentHeadlineContainerView = headlineContainerViews![self.currentHeadlineNumber]
            }
            
            if (self.currentHeadlineNumber == fetchedHeadlines.count - 1) {
                self.allStats = NCDataStoreSwift.sharedStore.retrieveSocialStats()
                self.statsTableView?.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            if (scrollView.contentOffset.x > CGFloat(fetchedHeadlines.count - 1) * windowWidth!) {
                self.setScrollViewFrameForEndScreen()
                accountScreenRemainingBackgroundColor = UIColor.companyBackground()
            } else {
                scrollView.frame = self.scrollViewFrame!
                accountScreenRemainingBackgroundColor = UIColor.lightGradient()
            }
            if let countView = self.navigationItem.titleView as? NCSeenCountView {
                if countView.responds(to: #selector(setter: NCSeenCountView.count)) {
                    countView.count = fetchedHeadlines.count - self.currentHeadlineNumber - 1
                }
            }
            self.checkIfShared()
            self.currentHeadlineContainerView?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func setScrollViewFrameForEndScreen() {
        if headlineScrollView != nil {
            let endScrollViewFrame = CGRect(x: 0, y: 0, width: self.headlineScrollView!.frame.size.width, height: self.view.frame.size.height)
            self.headlineScrollView?.frame = endScrollViewFrame
        }
        
    }
    
    func checkIfShared() {
                
        DispatchQueue.main.async { () -> Void in
            
            if (self.presentedViewController == nil && self.animator?.isRunning == false && self.fetchedHeadlines.count > 0 ) {
                
                let headline = self.fetchedHeadlines[self.currentHeadlineNumber]
                let seenProperties = ["Article" : headline.title, "ArticleID" : headline.id, "Seen on Channel" : self.currentFeed != nil ? String(self.currentFeed!.id) : "", "Url" : "\(headline.url!)", "Date" : "\(Date())"] as [String : Any]
                
                if !headline.isSeen {
                    SEGAnalytics.shared().track(SEG_Article_Seen, properties: seenProperties as [AnyHashable: Any])
                    headline.isSeen = true
                    headline.seenAt = Date()
                }
                
                let postedHeadlines = NCDataStoreSwift.sharedStore.retrievePostedHeadlines()
                
                let postedContainsArticle = postedHeadlines.contains {$0.title == headline.title}
                
                var headlineSharedInstantlyOnTwitter = false
                var headlineSharedInstantlyOnLinkedIn = false
                var headlineSharedWithScheduleOnTwitter = false
                var headlinesharedWithScheduleOnLinkedIn = false
                
                // Implemented for articles that might be in 2 or more feeds
                
                if postedContainsArticle {
                    for h1 in postedHeadlines {
                        if h1.title == headline.title {
                            headlineSharedInstantlyOnTwitter = h1.sharedInstantlyOnTwitter ? true : false
                            headlineSharedInstantlyOnLinkedIn = h1.sharedInstantlyOnLinkedIn ? true : false
                            headlineSharedWithScheduleOnTwitter = h1.sharedWithScheduleOnTwitter ? true : false
                            headlinesharedWithScheduleOnLinkedIn = h1.sharedWithScheduleOnLinkedIn ? true : false
                        }
                    }
                } else {
                    headlineSharedInstantlyOnTwitter = self.fetchedHeadlines[self.currentHeadlineNumber].sharedInstantlyOnTwitter ? true : false
                    headlineSharedInstantlyOnLinkedIn = self.fetchedHeadlines[self.currentHeadlineNumber].sharedInstantlyOnLinkedIn ? true : false
                    headlineSharedWithScheduleOnTwitter = self.fetchedHeadlines[self.currentHeadlineNumber].sharedWithScheduleOnTwitter ? true : false
                    headlinesharedWithScheduleOnLinkedIn = self.fetchedHeadlines[self.currentHeadlineNumber].sharedWithScheduleOnLinkedIn ? true : false
                }
                
                // Checks and updates UI if the article is shared
                
                if (headlineSharedInstantlyOnTwitter == true || headlineSharedInstantlyOnLinkedIn == true || headlineSharedWithScheduleOnTwitter == true || headlinesharedWithScheduleOnLinkedIn == true) {
                    let twitterButton = self.shareButtons![0]
                    let linkedInButton = self.shareButtons![1]
                    
                    if (headlineSharedInstantlyOnLinkedIn == true || headlinesharedWithScheduleOnLinkedIn == true) {
                        linkedInButton.isScheduled = headlinesharedWithScheduleOnLinkedIn
                        linkedInButton.animateSharedState(true)
                    } else {linkedInButton.animateSharedState(false)}
                    
                    if (headlineSharedInstantlyOnTwitter == true || headlineSharedWithScheduleOnTwitter == true) {
                        twitterButton.isScheduled = headlineSharedWithScheduleOnTwitter
                        twitterButton.animateSharedState(true)
                    } else {twitterButton.animateSharedState(false) }
                } else {
                    for b in self.shareButtons! {
                        b.animateSharedState(false)
                    }
                }
            }
        }
    }
    
    //MARK: Share Button Methods
    var shareActivationBegan = false
    
    func instantShareActivationBegan() {
        
        shareActivationBegan = true

        DispatchQueue.main.async {
            self.view.insertSubview(self.instantShareHighlightView!, aboveSubview: self.headlineScrollView!)
            self.view.insertSubview(self.instantShareWarningScheduled!, aboveSubview: self.instantShareHighlightView!)
            self.view.insertSubview(self.instantShareWarningNow!, aboveSubview: self.instantShareHighlightView!)
            self.view.insertSubview(self.instantShareWarningImageView!, aboveSubview: self.instantShareWarningScheduled!)
            self.view.insertSubview(self.instantShareWarningSDescription!, aboveSubview: self.instantShareWarningScheduled!)
            self.view.insertSubview(self.instantShareWarningNDescription!, aboveSubview: self.instantShareWarningNow!)
            
            
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.instantShareHighlightView?.alpha = 1.0
                self.instantShareWarningNow?.alpha = 0.5
                self.instantShareWarningScheduled?.alpha = 0.5
                self.instantShareWarningNDescription?.alpha = 0.5
                self.instantShareWarningSDescription?.alpha = 0.5
                self.instantShareWarningImageView?.alpha = 0.5
            })
//            self.blurBackgroundHeadlineContainer()
        }
    }
    
    func shareButtonMoving(_ sender: Any!) {
        
        //  iPhone 6
        let y1 = Double(2)
        let y2 = Double(instantShareWarningScheduled!.frame.height + 6)        

        if let button = sender as? NCShareButton {

            DispatchQueue.main.async {
                
                if button.translationDetect <=  y1 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningNow?.backgroundColor = UIColor.white
                        self.instantShareWarningScheduled?.backgroundColor = UIColor.white
                        self.instantShareWarningNow?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningScheduled?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningSDescription?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningNDescription?.textColor = UIColor("#3F4D61")
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        
                    })
                    
                } else if button.translationDetect > y1 && button.translationDetect < y2 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningScheduled?.alpha = 1.0
                        self.instantShareWarningNow?.alpha = 0.5
                        self.instantShareWarningNDescription?.alpha = 0.5
                        self.instantShareWarningSDescription?.alpha = 1
                        self.instantShareWarningImageView?.alpha = 1
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = button.socialMediaColor.cgColor
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        
                    })
                    
                } else if button.translationDetect >= y2 {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningScheduled?.alpha = 0.5
                        self.instantShareWarningNow?.alpha = 1.0
                        self.instantShareWarningNDescription?.alpha = 1
                        self.instantShareWarningSDescription?.alpha = 0.5
                        self.instantShareWarningImageView?.alpha = 0
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = button.socialMediaColor.cgColor
                        
                    })
                    
                } else {
                    
                    UIView.animate(withDuration: 0.35, animations: {
                        
                        self.instantShareWarningNow?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningScheduled?.dashedLayer?.strokeColor = UIColor("#3F4D61").cgColor
                        self.instantShareWarningNow?.alpha = 0
                        self.instantShareWarningScheduled?.alpha = 0
                        self.instantShareWarningNDescription?.alpha = 0
                        self.instantShareWarningSDescription?.alpha = 0
                        self.instantShareWarningImageView?.alpha = 0
                        
                    })
                }
            }
        }
    }
    
    func instantShareActivationEnded(_ finished: Bool, toShareInstantly instantly: Bool, to network: SocialNetwork, sender: Any!) {
    
        shareActivationBegan = false
        var canProceed = false
        let accounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
        var service = ""
        
        switch network {
        case .linkedIn:
            service = "linkedin"
            break
        case .twitter:
            service = "twitter"
            break
        default:
            break
        }
        
        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
            account.service == service
        })

        if finished == true {
             if canProceed {
                let currentHeadline = fetchedHeadlines[self.currentHeadlineNumber]
                DispatchQueue.global(qos: .default).async(execute: { () -> Void in
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    NCDataStoreSwift.sharedStore.prepareForPosting("", toNetwork: network, fromHeadline: currentHeadline, inChannel: self.currentFeed?.id != nil ? String(self.currentFeed!.id) : "Shared from Seen Headlines", immediately: instantly, withSuccess: { (succeeded: Bool, error: Error?, response: HTTPURLResponse?) -> Void in
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        DispatchQueue.main.async(execute: { () -> Void in
                            if (error == nil && succeeded == true) {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                
                                self.checkIfShared()
                            } else if (error as? NSError)?.code == -1009  {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "NoInternet"), object: self)
                            } else {
                                if network == .twitter {
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ServerError"), object: self)
                                } else {
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LinkedInError"), object: nil, userInfo: ["message": "", "network" : network, "channel" : (self.currentFeed?.id != nil ? String(self.currentFeed!.id) : "Shared from Seen Headlines"), "immediate" : instantly])
                                }
                            }
                        })
                    })
                })
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NoAccount"), object: self)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        
        DispatchQueue.main.async { () -> Void in
//            UIView.animate(withDuration: 0.9, animations: { () -> Void in
//                }, completion: { (finished: Bool) -> Void in
//            })
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.instantShareHighlightView?.alpha = 0
                self.instantShareWarningScheduled?.alpha = 0
                self.instantShareWarningNow?.alpha = 0
                self.instantShareWarningSDescription?.alpha = 0
                self.instantShareWarningNDescription?.alpha = 0
                self.instantShareWarningImageView?.alpha = 0

                }, completion: nil)
//            self.deblurBackgroundHeadlineContainer()
        }
    }
    
    func shareButtonTapped(_ sender: Any!) {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.45 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            var canProceed = false
            let accounts = NCDataStoreSwift.sharedStore.retrieveSocialAccounts()
            if accounts != nil {
                if let button = sender as? NCShareButton {
                    switch (button.buttonType) {
                    case .twitter:
                        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
                            account.service == "twitter"
                        })
                        break
                    case .linkedIn:
                        canProceed = accounts!.contains(where: { (account: NCSocialAccount) -> Bool in
                            account.service == "linkedin"
                        })
                        break
                    default:
                        break
                    }
                }
            }

            if self.presentedViewController == nil {
                if canProceed == true {
                    self.performSegue(withIdentifier: "ShowMessageModal", sender: sender)
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NoAccount"), object: self)
                }
            }
        }
    }
    
    // MARK: Visual effects
    
//    func blurBackgroundHeadlineContainer () {
//        
//        blurredImageView?.image = self.currentHeadlineContainerView?.blurredImage
//        blurredImageView?.alpha = 0
//        self.view.insertSubview(blurredImageView!, belowSubview: self.instantShareHighlightView!)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            self.blurredImageView?.alpha = 1.0
//        }) 
//    }
//    
//    func deblurBackgroundHeadlineContainer() {
//        
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            self.blurredImageView?.alpha = 0.0
//            }, completion: { (finished: Bool) -> Void in
//        }) 
//    }
    

    func blurredSnapshotBackground() -> UIImage {
        
        UIGraphicsBeginImageContext(UIScreen.main.bounds.size)
        let window = UIApplication.shared.windows[0]
        self.view.superview?.drawHierarchy(in: window.bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let blurredSnapshotBackground = snapshotImage?.applyBlur(withRadius: 9.0, tintColor: nil, saturationDeltaFactor: 1.0, maskImage: nil)
        
        return blurredSnapshotBackground!
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        
        if shareActivationBegan == false {
            DispatchQueue.main.async {
                self.instantShareHighlightView?.removeFromSuperview()
                self.instantShareWarningScheduled?.removeFromSuperview()
                self.instantShareWarningNow?.removeFromSuperview()
                self.instantShareWarningSDescription?.removeFromSuperview()
                self.instantShareWarningNDescription?.removeFromSuperview()
                self.instantShareWarningImageView?.removeFromSuperview()
            }
        }

        self.checkIfShared()
    }
    
    //MARK: Transition/Segue Methods
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transitionAnimator = ModalMessageTransition()
        return transitionAnimator
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transitionAnimator = ModalMessageTransition()
        transitionAnimator.isPresenting = true
        return transitionAnimator
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowFullArticle" {
            
            if let articleViewController = segue.destination as? ArticleViewController {
                articleViewController.backgroundSnapshot = self.blurredSnapshotBackground()
                articleViewController.currentHeadline = self.currentHeadlineContainerView?.headline
            }
        } else if segue.identifier == "ShowMessageModal" {
            
            if let messageModalViewController = segue.destination as? MessageModalViewController {
                messageModalViewController.modalPresentationStyle = UIModalPresentationStyle.custom
                messageModalViewController.transitioningDelegate = self
                messageModalViewController.headline = self.currentHeadlineContainerView?.headline
                messageModalViewController.feed = self.currentFeed
                
                if let button = sender as? NCShareButton {
                    messageModalViewController.network = button.buttonType
                }
            }
        } else if segue.identifier == "ShowWebPage" {
            
            if let webViewController = segue.destination as? WebViewController {
                webViewController.headline = self.currentHeadlineContainerView?.headline
                webViewController.pushedFromACVC = false
                webViewController.pushedFromMVC = false
            }
        } else if segue.identifier == "ShowAuthFromMVC" {
            if let webVC = segue.destination as? WebViewController {
                if let url = sender as? URL {
                    
                    webVC.headline = self.currentHeadlineContainerView?.headline
                    webVC.alternateURL = url
                    webVC.message = shareMessage
                    webVC.channel = shareFeed != nil ? String(shareFeed!.id) : "Shared from seen headlines"
                    webVC.immediate = shareInstant
                    webVC.network = shareNetwork
                    webVC.pushedFromMVC = true
                    webVC.callback = {(accounts: [NCSocialAccount], error: Error?) -> Void in
                        
                        if accounts.count > 0 {
                            DispatchQueue.main.async(execute: {
                                NCDataStoreSwift.sharedStore.identifyUser()
                                self.checkIfShared()
                            })
                        } else if error != nil {
                            self.showConnectionWarning()
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Error Handling
    
    func showConnectWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_TITLE", comment: ""), message: NSLocalizedString("COULD_NOT_FIND_ACCOUNTS_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action:UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "directToAccountsSegue", sender: self)
        }
        let cancelaction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(cancelaction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNetworkWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("NETWORK_ERROR_TITLE", comment: ""), message: NSLocalizedString("NETWORK_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showServerWarning(_ notification: Notification) {
        
        let alert = UIAlertController(title: NSLocalizedString("SERVER_ERROR_TITLE", comment: ""), message: NSLocalizedString("SERVER_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showConnectionWarning() {
        
        let alert = UIAlertController(title: NSLocalizedString("CONNECTION_ERROR_TITLE", comment: ""), message: NSLocalizedString("CONNECTION_ERROR_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showWebPageOnLinkedInError(_ notification: Notification) {
        
        shareMessage = notification.userInfo?["message"] != nil ? (notification.userInfo?["message"] as! String) : ""
        shareInstant = notification.userInfo?["immediate"] as! Bool
        shareNetwork = notification.userInfo?["network"] as? SocialNetwork
        shareFeed = notification.userInfo?["channel"] as? NCFeed
        
        if NCDataStoreSwift.sharedStore.loggedInUser != nil && NCDataStoreSwift.sharedStore.connectedToNetwork() {
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.performSegue(withIdentifier: "ShowAuthFromMVC", sender: URL(string: "\(NCRESTAPI)/oauth/linkedin?access_token=\(NCDataStoreSwift.sharedStore.loggedInUser!.authorizationToken!.accessToken)"))
            })
        }
    }
    
    //MARK: Housekeeping
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cleanUpViews() {
        
        self.headlineScrollView?.removeFromSuperview()
        self.headlineScrollView = nil
        self.endingMotivationalMessage?.removeFromSuperview()
        self.endingMotivationalMessage = nil
        self.statsTableView?.removeFromSuperview()
        self.statsTableView = nil
        self.allStats = []
        fetchedHeadlines = []
        headlineContainerViews = nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NCDataStoreSwift.sharedStore.saveSeenHeadlinesLocally()
        UserDefaults.standard.synchronize()
    }

}
