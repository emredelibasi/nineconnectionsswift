//
//  ShareButton.m
//  NineConnections
//
//  Created by Floris van der Grinten on 16-05-14.
//
//

#import "NCShareButton.h"
#import "NineConnectionsSwift-Bridging-Header.h"

@interface NCShareButton ()

@property (nonatomic, strong, readwrite) UIColor *socialMediaColor;
@property (nonatomic, strong) UIImageView *socialIcon;
@property (nonatomic, strong) UIImageView *checkmark;
@property (nonatomic, strong) UIImage *checkmarkImage;
@property (nonatomic, strong) UIImage *bufferImage;
@property (nonatomic, strong) UISnapBehavior *snapBehavior;
//@property (nonatomic, strong) UILabel *dropLabel;

@end

@implementation NCShareButton {
    CGFloat yTranslationSum;
    CGFloat maxYTranslation;
    CGFloat maxYTranslationForInstantShare;
    CGFloat maxYTranslationForScheduledShare;
    CGFloat buttonDiameter;
    int i;
   // CGFloat distanceToFullIntersection;
    
}


- (instancetype)initWithButtonType:(SocialNetwork)buttonType
{
    buttonDiameter = 82.0;
    maxYTranslation = 60.0;
    
    
    self = [super initWithFrame:CGRectMake(0, 0, buttonDiameter, buttonDiameter)];
    if (self) {
       
        self.socialIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"twitter_share.png"]];
        self.buttonType = buttonType;
        
        if (self.buttonType == SocialNetworkTwitter) {
            self.socialMediaColor = [UIColor twitterColor];
            self.layer.borderColor = [UIColor twitterColor].CGColor;
            self.socialIcon.image = [UIImage imageNamed:@"twitter_share.png"];
        }
        else if (self.buttonType == SocialNetworkLinkedIn) {
            self.socialMediaColor = [UIColor linkedInColor];
            self.layer.borderColor = [UIColor linkedInColor].CGColor;
            self.socialIcon.image = [UIImage imageNamed:@"linkedin_share.png"];
        }
        
        self.backgroundColor = [UIColor clearColor];
    
        self.layer.cornerRadius = CGRectGetWidth(self.frame)/2;
        self.layer.borderWidth = 2;

        self.bufferImage = [[UIImage imageNamed:@"buffer_share.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.checkmarkImage = [[UIImage imageNamed:@"checkmark.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.checkmark = [[UIImageView alloc] initWithFrame:CGRectInset(self.frame, self.frame.size.width/2.95, self.frame.size.height/2.95)];
        self.checkmark.contentMode = UIViewContentModeScaleAspectFit;
        //self.checkmark.frame = self.frame;
        self.checkmark.tintColor = [UIColor whiteColor];

        self.scheduled = YES;
        [self addSubview:self.checkmark];
        self.checkmark.hidden = YES;
        self.checkmark.center = self.center;

        [self addSubview:self.socialIcon];
        
      //  self.socialIcon.center = CGPointMake(CGRectGetMidX(self.bounds),CGRectGetMidY(self.bounds));
        self.socialIcon.center = self.center;
        self.socialIcon.contentMode = UIViewContentModeBottom;

        UIPanGestureRecognizer *draggingRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragShareButton:)];
        [self addGestureRecognizer:draggingRecognizer];

    }
    return self;
}


- (void)dragShareButton:(UIPanGestureRecognizer *)gesture
{
    CGPoint translation = [gesture translationInView:self.superview];
    //CGFloat percentage =  - translation.y / windowHeight;
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self.dynamicsAnimator removeBehavior:self.snapBehavior];
        } break;
        case UIGestureRecognizerStateChanged:
        {
            [self.delegate shareButtonMoving:self];
            self.translationDetect = 0;
            yTranslationSum += -translation.y;
            if (self.shareInteractionEnabled) {
                if (yTranslationSum > maxYTranslation) {
                    self.translationDetect = yTranslationSum - maxYTranslation;
                    //NSLog(@"%f", self.translationDetect);
                    i++;
                    if (i==1) {
                        [self onInstantPostActivation];
                    }
                }
                else if (i > 1){
                    i=0;
                    [self deanimateShareButton];
                    [self.delegate instantShareActivationEnded:NO toShareInstantly:[self shouldShareInstantly] toNetwork:self.buttonType sender:self];
                }
            }
           // NSLog(@"%f",self.center.y);
            gesture.view.center = CGPointMake(gesture.view.center.x + translation.x, gesture.view.center.y + translation.y);
            [gesture setTranslation:CGPointZero inView:self.superview];
        } break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            [self addShareButtonsSnappingBehavior];
            
            if (self.shareInteractionEnabled) {
                if (yTranslationSum > maxYTranslation) {
                    if (self.delegate) {
                    
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [self. delegate instantShareActivationEnded:YES toShareInstantly:[self shouldShareInstantly] toNetwork:self.buttonType sender:self];
                        });
                    }
                }
                else {
                    if (self.delegate) [self.delegate instantShareActivationEnded:NO toShareInstantly:[self shouldShareInstantly] toNetwork:self.buttonType sender:self];
                }
            }
            yTranslationSum = 0;
            i = 0;

            [self deanimateShareButton];
            
        } break;
        
            
            
        default:
            break;
    }
}

- (void)onInstantPostActivation
{
//    [UIView animateWithDuration:0.2 animations:^{
//        self.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:29.0/255.0 alpha:1.0];
//    }];
    [UIView animateWithDuration:0.5 animations:^{
        self.transform = CGAffineTransformMakeScale(1.6, 1.6);
        self.backgroundColor = self.socialMediaColor;
        self.layer.borderColor = self.buttonType == SocialNetworkLinkedIn ? [UIColor linkedInDarkerColor].CGColor : [UIColor twitterDarkerColor].CGColor;
        self.layer.borderWidth = 4;
    }];
    
    [UIView transitionWithView:self.socialIcon
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.socialIcon.image = [UIImage imageNamed: self.buttonType == SocialNetworkLinkedIn ? @"linkedin_white" : @"twitter_white"];
                    } completion:nil];
    
    
    if (self.delegate) {
        [self.delegate instantShareActivationBegan];
    }

   // UIView *backBlurView
    //[self.superview addSubview:backBlurView];
}

- (BOOL)shouldShareInstantly
{

    if (IS_IPHONE_5) {
        if (self.translationDetect > 2 && self.translationDetect < 216) {
            return false;
        } else if (self.translationDetect >= 216) {
            return true;
        }
    } else if (IS_IPHONE_6P) {
        if (self.translationDetect > 2 && self.translationDetect < 315) {
            return false;
        } else if (self.translationDetect >= 315) {
            return true;
        }
    } else {
        if (self.translationDetect > 2 && self.translationDetect < 272) {
            return false;
        } else if (self.translationDetect >= 272) {
            return true;
        }
    }
    return false;
}
        

- (void)deanimateShareButton
{
    
    //[self.dropLabel removeFromSuperview];
    [UIView animateWithDuration:0.5 animations:^{
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.layer.borderWidth = 2;
        self.layer.borderColor = self.socialMediaColor.CGColor;
        
    }];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = self.checkmark.hidden ? [UIColor clearColor] : [UIColor successColor];
    }];
    
    
    [UIView transitionWithView:self.socialIcon
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.socialIcon.image = [UIImage imageNamed: self.buttonType == SocialNetworkLinkedIn ? @"linkedin_share" : @"twitter_share"];
                    } completion:nil];

}

- (void)morphShareButton
{
    NSLog(@"%i", [NSThread isMainThread]);
    self.transform = CGAffineTransformMakeScale(1, 1);
    CGFloat duration = 0.5;
    [UIView animateWithDuration:duration/4 delay:0 options:0 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1.4);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:duration/4 delay:0 options:0 animations:^{
            self.transform = CGAffineTransformMakeScale(1.2, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:duration/4 delay:0 options:0 animations:^{
                self.transform = CGAffineTransformMakeScale(0.9, 0.9);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:duration/4 delay:0 options:0 animations:^{
                    self.transform = CGAffineTransformMakeScale(1, 1);
                } completion:^(BOOL finished) {
                }];
            }];
        }];
    }];
}

- (void)addShareButtonsSnappingBehavior
{
    [self.dynamicsAnimator addBehavior:self.snapBehavior];
}

- (UISnapBehavior *)snapBehavior
{
    if (!_snapBehavior) {
        _snapBehavior = [[UISnapBehavior alloc] initWithItem:self snapToPoint:self.centerPosition];
        _snapBehavior.damping = 0.4;
    }
    return _snapBehavior;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //UITouch *touch = [touches anyObject];
    [super touchesEnded:touches withEvent:event];
    
    [self.dynamicsAnimator removeBehavior:_snapBehavior];
    
    [self morphShareButton];
    [UIView animateWithDuration:0.1 animations:^{
        self.center = self.centerPosition;
    }];
    
    if (self.shareInteractionEnabled) {
        
        [self.delegate shareButtonTapped:self];
    }

}

- (void)animateSharedState:(BOOL)shared;
{
    if (shared) {
        if (self.checkmark.hidden) {
            self.shareInteractionEnabled = NO;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self cache:YES];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
            [UIView commitAnimations];
            self.backgroundColor = [UIColor successColor];
            self.socialIcon.hidden = YES;
            self.checkmark.hidden = NO;
            self.userInteractionEnabled = NO;
            self.layer.borderColor = [UIColor successColor].CGColor;
        }
        self.checkmark.image = self.checkmarkImage;
    } else {
        if (!self.checkmark.hidden) {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.2];
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView commitAnimations];
            self.backgroundColor = [UIColor clearColor];
            self.socialIcon.hidden = NO;
            self.checkmark.hidden = YES;
            self.userInteractionEnabled = YES;
            self.layer.borderColor = self.socialMediaColor.CGColor;
        }
    }

}





@end
