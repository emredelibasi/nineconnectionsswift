//
//  NCSeenCountView.h
//  NineConnections
//
//  Created by Floris van der Grinten on 15-08-14.
//
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"

@interface NCSeenCountView : UIView

@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) UILabel *countLabel;

- (instancetype)initWithArticleCount:(NSInteger)count excistingTitle:(UINavigationItem *)navItem fontSize:(CGFloat)size;

@end
