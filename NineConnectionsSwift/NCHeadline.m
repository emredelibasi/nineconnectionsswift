//
//  Headline.m
//  NineConnections
//
//  Created by Floris van der Grinten on 14-05-14.
//
//

#import "NCHeadline.h"

@implementation NCHeadline

- (instancetype)initWithTitle:(NSString *)title articleText:(NSString *)text feed:(NCFeed *)feed
{
    self = [super init];
    if (self) {
        self.title = title;
        self.articleText = text;
        self.feedHash = feed.hash;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.linkedInId = [aDecoder decodeObjectForKey:@"linkedInId"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.articleURL = [aDecoder decodeObjectForKey:@"articleURL"];
        self.date = [aDecoder decodeObjectForKey:@"date"];
        self.twitterId = [aDecoder decodeObjectForKey:@"twitterId"];
        self.bufferLinkedInId = [aDecoder decodeObjectForKey:@"bufferLinkedInId"];
        self.bufferTwitterId = [aDecoder decodeObjectForKey:@"bufferTwitterId"];
        
        NSString *apiItemId = [aDecoder decodeObjectForKey:@"apiItemId"];
        self.apiItemId = apiItemId ? apiItemId : [NSString stringWithFormat:@"%zd", [aDecoder decodeIntegerForKey:@"apiItemId"]];
        NSLog(@"%@", self.apiItemId);
        self.feedHash = [[aDecoder decodeObjectForKey:@"feedHash"] unsignedIntegerValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.linkedInId forKey:@"linkedInId"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.articleURL forKey:@"articleURL"];
    [aCoder encodeObject:self.date forKey:@"date"];
    [aCoder encodeObject:self.bufferLinkedInId forKey:@"bufferLinkedInId"];
    [aCoder encodeObject:self.bufferTwitterId forKey:@"bufferTwitterId"];
    [aCoder encodeObject:self.twitterId forKey:@"twitterId"];
    [aCoder encodeObject:self.apiItemId forKey:@"apiItemId"];
    [aCoder encodeObject:@(self.feedHash) forKey:@"feedHash"];
}

- (BOOL)isEqual:(id)object
{
    if (!object) return NO;
    
    if ([object isKindOfClass:[NCHeadline class]]) {
        NCHeadline *comparedHeadline = (NCHeadline *)object;
        
        if (comparedHeadline.apiItemId && self.apiItemId)
            if ([comparedHeadline.apiItemId isEqualToString:self.apiItemId]) {
                if (!self.twitterId) self.twitterId = comparedHeadline.twitterId;
                if (!self.linkedInId) self.linkedInId = comparedHeadline.linkedInId;
                if (!self.bufferTwitterId) self.bufferTwitterId = comparedHeadline.bufferTwitterId;
                if (!self.bufferLinkedInId) self.bufferLinkedInId = comparedHeadline.bufferLinkedInId;
                return YES;
            }
        
        return ([comparedHeadline.articleURL isEqual:self.articleURL]);
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.apiItemId.hash;
}

@end
