//
//  NCHeadlineContainerView.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/7/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import UIKit
import TagListView
import Analytics


class NCHeadlineContainerView: UIView {

    var windowWidth: CGFloat?
    var currentTransformFactor: CGFloat = 1.0
    var headlineContainerViewHeight: CGFloat?
    var twitterButtonPosition: CGPoint?
    var linkedInButtonPosition: CGPoint?
    var headline: NCHeadline?
    var urlButton: UIButton?
    var backgroundImage: UIImageView?
    var titleView: UITextView?
    var bookmarkedImageView: UIImageView!
    var bookmarkButton: UIButton!
    var gestureBlock: (() -> ())?
    let oldIphone = UIDevice.current.modelName == "Simulator" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s"
    
    init(withHeadline headline:NCHeadline, width: CGFloat) {
        
        self.windowWidth = width
        self.headline = headline
        self.headlineContainerViewHeight = NCHeadlineContainerView.headlineContainerHeight()
        
        super.init(frame: CGRect(x: 0, y: 0, width: windowWidth!, height: headlineContainerViewHeight!))
        
        let bottomCenter = CGPoint(x: self.frame.maxX, y: self.frame.midY)
        self.layer.anchorPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.position = bottomCenter
        
        self.backgroundColor = UIColor.white
        
        self.addContainerContent()
        self.clipsToBounds = true
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addContainerContent() {
        
        self.titleView = UITextView()
        self.titleView!.isScrollEnabled = false
        
        self.titleView!.backgroundColor = UIColor.clear
        self.titleView!.textColor = UIColor("#3F4D61")
        self.titleView!.textAlignment = .center
        self.titleView!.isUserInteractionEnabled = false
        self.titleView!.font = UIFont(name: "ProximaNova-Regular", size: oldIphone ? 26 : 31)
        let titleViewMargin: CGFloat = 28
        self.titleView!.frame = CGRect(x: titleViewMargin, y: 0, width: self.frame.size.width - titleViewMargin*2, height: headlineContainerViewHeight!)
        
        // Text Size adjuster to fit tags
        self.titleView!.text = self.headline!.title
        self.titleView!.sizeToFit()
        
        let iphoneOldGen = (UIDevice.current.modelName == "iPhone 4" || UIDevice.current.modelName == "iPhone 4s" || UIDevice.current.modelName == "iPhone 5" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "Simulator")
        
        if iphoneOldGen {
            self.titleView!.font = UIFont(name: "ProximaNova-Regular", size: 25)
            self.titleView!.sizeToFit()

        }
        
        //let headlineHasTags = self.headline!.tags.count > 0 || self.headline!.trends.count > 0
        
        self.titleView!.center = CGPoint(x: self.windowWidth!/2, y: self.frame.midY - /*(headlineHasTags ? 142 :*/ 86)
        let yMarginURLBar: CGFloat = 7
        
        let urlBar = UIView(frame: CGRect(x: 20, y: self.titleView!.frame.maxY+yMarginURLBar, width: windowWidth!-20, height: 34))
        urlBar.backgroundColor = UIColor.white
        urlBar.layer.cornerRadius = 4
        
        self.addSubview(self.titleView!)
        
        if !UserDefaults.standard.bool(forKey: DEFAULTS_REMOVE_ARTICLE_LINKS) {
            
            self.urlButton = UIButton(type: .system)
            self.urlButton!.frame = CGRect(x: 12, y: -3, width: 0, height: 0)
            //let optionalQuery = self.headline!.url!.query != nil ? "\(self.headline!.url!.query!)" : ""
            let hostWithoutWWW = self.headline!.url!.host!.replacingOccurrences(of: "www.", with: "")

            self.urlButton!.setTitle("\(hostWithoutWWW)", for:.normal)
            self.urlButton!.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: oldIphone ? 15 : 17)
            self.urlButton!.tintColor = UIColor("#3F4D61")
            self.urlButton?.alpha = 0.6
            self.urlButton!.sizeToFit()
            self.urlButton!.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            self.urlButton!.frame = CGRect(x: self.urlButton!.frame.minX, y: self.urlButton!.frame.minY, width: windowWidth!-60, height: self.urlButton!.frame.height)
            self.urlButton!.clipsToBounds = true
            urlBar.sizeToFit()
            self.addSubview(urlBar)
            urlBar.addSubview(self.urlButton!)
        }
        
        bookmarkButton = UIButton(frame: CGRect(x: self.windowWidth!-80, y: 0, width: 80, height: 80))
        bookmarkButton.addTarget(self, action: #selector(bookmarkHeadline(_:)), for: .touchUpInside)
        self.addSubview(bookmarkButton)
        
        bookmarkedImageView = UIImageView(frame: CGRect(x: self.windowWidth!-46, y: 20, width: 26, height: 39))
        bookmarkedImageView.contentMode = .scaleAspectFit
        self.addSubview(bookmarkedImageView)
        
        if self.headline!.isBookmarked {
            bookmarkedImageView.image = #imageLiteral(resourceName: "bookmarked_icon")
            bookmarkedImageView.image = bookmarkedImageView.image?.withRenderingMode(.alwaysTemplate)
            bookmarkedImageView.tintColor = UIColor.companyInnerNavBar()
        } else {
            bookmarkedImageView.image = #imageLiteral(resourceName: "unbookmarked_icon")
        }
        
//        let tagsListView = TagListView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 70, height: 89))
//        tagsListView.center = CGPoint(x: self.windowWidth!/2, y: urlBar.frame.maxY + 70)
//        tagsListView.alignment = .center
//        
//        if self.headline!.trends.count < 6 {
//            for trend in self.headline!.trends {
//                tagsListView.addNLPTag(trend.lowercased(), withColor: UIColor.companyInnerNavBar())
//            }
//            
//            if self.headline!.tags.count > 0 {
//                for i in 0...(self.headline!.tags.count-1) {
//                    if i==6 {break}
//                    tagsListView.addKeywordTag(self.headline!.tags[i].lowercased())
//                }
//            }
//            
//        } else {
//            for i in 0...5 {
//                tagsListView.addNLPTag(self.headline!.trends[i].lowercased(), withColor: UIColor.companyInnerNavBar())
//            }
//            
//        }
//        
//        tagsListView.layer.masksToBounds = true
//        self.addSubview(tagsListView)
        
    }
    
    func bookmarkHeadline(_ sender: AnyObject) {
        
        self.headline!.isBookmarked = !self.headline!.isBookmarked
        
        if self.headline!.isBookmarked {
            bookmarkedImageView.image = #imageLiteral(resourceName: "bookmarked_icon")
            bookmarkedImageView.image = bookmarkedImageView.image?.withRenderingMode(.alwaysTemplate)
            bookmarkedImageView.tintColor = UIColor.companyInnerNavBar()
            
            let bookmarkProperties: [String:Any] = ["Network" : "LinkedIn", "Article" : self.headline!.title, "ArticleID" : self.headline!.id, "Url" : self.headline!.url!, "Date" : Date()]
            SEGAnalytics.shared().track(SEG_Article_Bookmarked, properties: bookmarkProperties)
            
            NCDataStoreSwift.sharedStore.saveHeadlineBookmarkLocally(self.headline!)
        } else {
            bookmarkedImageView.image = #imageLiteral(resourceName: "unbookmarked_icon")
        }
        
    }
    
    class func headlineContainerHeight() -> CGFloat {
        
        let screenHeight = UIScreen.main.bounds.size.height-63
        return screenHeight
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.gestureBlock!()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
    }

}
