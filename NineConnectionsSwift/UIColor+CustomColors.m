//
//  UIColor+CustomColors.m
//  NineConnections
//
//  Created by Floris van der Grinten on 13-05-14.
//
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

//+ (UIColor *)backgroundGreyColor
//{
//    CGFloat greyValue = 0.20;
//    return [UIColor colorWithRed:greyValue green:greyValue blue:greyValue alpha:1.0];
//}

+ (UIColor *)backgroundGreyColor
{
    return [UIColor colorWithRed:46.0/255.0 green:58.0/255.0 blue:74.0/255.0 alpha:1.0];
}

+ (UIColor *)mainBlueColor
{
    return [UIColor colorWithRed:74.0/255.0 green:144.0/255.0 blue:226.0/255.0 alpha:1.0];
    //return [UIColor orangeColor];
}

+ (UIColor *)mainBlueLighterColor
{
    return [UIColor colorWithRed:70.0/255.0 green:153.0/255.0 blue:245.0/255.0 alpha:1.0];
    //return [UIColor orangeColor];
}

+ (UIColor *)lightGradientColor
{
    return [UIColor colorWithRed:39.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
}

+ (UIColor *)darkGradientColor
{
    return [UIColor colorWithRed:40.0/255.0 green:118.0/255.0 blue:213.0/255.0 alpha:1.0];
}

+ (UIColor *)twitterColor
{
    return [UIColor colorWithRed:29.0/255.0 green:161.0/255.0 blue:242.0/255.0 alpha:1.0];
}

+ (UIColor *)twitterDarkerColor
{
    return [UIColor colorWithRed:87.0/255.0 green:174.0/255.0 blue:221.0/255.0 alpha:1.0];
}

+ (UIColor *)linkedInColor
{
    return [UIColor colorWithRed:0/255.0 green:119.0/255.0 blue:181.0/255.0 alpha:1.0];
}

+ (UIColor *)linkedInDarkerColor
{
    return [UIColor colorWithRed:0/255.0 green:98.0/255.0 blue:145.0/255.0 alpha:1.0];
}

+ (UIColor *)successColor
{
    return [UIColor colorWithRed:43.0/255.0 green:172.0/255.0 blue:152.0/255.0 alpha:1.0];
}

- (UIColor *)darkenColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:b * 0.95
                               alpha:a];
    return nil;
}

@end
