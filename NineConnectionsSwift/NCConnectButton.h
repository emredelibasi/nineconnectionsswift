//
//  NCConnectButton.h
//  NineConnections
//
//  Created by Floris van der Grinten on 15-07-14.
//
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "NCDataStore.h"
#import <TOMSMorphingLabel/TOMSMorphingLabel.h>


@interface NCConnectButton : UIControl

@property (nonatomic, assign) SocialNetwork buttonType;
@property (nonatomic, copy) void(^callback)(NSArray *accounts);
@property (nonatomic, strong) TOMSMorphingLabel *titleLabel;
@property (nonatomic, strong) UILabel *disconnectLabel;
@property (nonatomic, assign, getter = isConnected) BOOL connected;

- (void)accountConnected:(NSString *)accountString animated:(BOOL)animated;
- (void)accountDisconnectedWithDuration:(CGFloat) duration;

@end
