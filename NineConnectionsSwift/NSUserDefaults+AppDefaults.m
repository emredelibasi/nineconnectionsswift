//
//  NSUserDefaults+AppDefaults.m
//  NineConnections
//
//  Created by Floris van der Grinten on 28-08-14.
//
//

#import "NSUserDefaults+AppDefaults.h"

@implementation NSUserDefaults (AppDefaults)

- (void)resetAppDefaults
{
    NSArray *keysToBeRemoved = @[@"linked_expiration",
                                 @"linkedin_token",
                                 @"linkedin_token_created_at",
                                 @"avatarURL",
                                 @"TwitterAccountID",
                                 @"LinkedInAccountName",
                                 @"LastSeenHeadlineIDs",
                                 @"BufferAccountNames",
                                 @"BufferAccountIDs",
                                 @"Twitter followers",
                                 @"LinkedIn connections",
                                 @"NineConnections username",
                                 DEFAULTS_TAP_TO_SHARE_INSTANTLY,
                                 DEFAULTS_DRAG_TO_SHARE_INSTANTLY,
                                 DEFAULTS_AVATAR_URL,
                                 DEFAULTS_DISPLAY_AVATAR,
                                 DEFAULTS_REMOVE_ANIMATIONS,
                                 DEFAULTS_REMOVE_PAGING_ANIMATION,
                                 DEFAULTS_REMOVE_ARTICLE_LINKS];
    
    for (NSString *key in keysToBeRemoved) [self removeObjectForKey:key];
    [self synchronize];
    [self registerDefaultPreferences];
}

- (void)registerDefaultPreferences
{
    [self registerDefaults:@{
                             DEFAULTS_DISPLAY_AVATAR : @NO,
                             DEFAULTS_REMOVE_PAGING_ANIMATION : @NO,
                             DEFAULTS_AVATAR_URL : @"NoURL",
                             DEFAULTS_REMOVE_ANIMATIONS : @NO,
                             DEFAULTS_REMOVE_ARTICLE_LINKS: @NO,
                             DEFAULTS_DRAG_TO_SHARE_INSTANTLY : @NO,
                             DEFAULTS_TAP_TO_SHARE_INSTANTLY : @NO,
                             @"LastSeenHeadlineIDs" : [NSDictionary new],
                             @"Twitter followers" : @0,
                             @"LinkedIn connections" : @0
                             }];
    
    [self synchronize];
}

@end
