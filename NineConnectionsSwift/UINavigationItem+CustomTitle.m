//
//  UINavigationItem+CustomTitle.m
//  NineConnections
//
//  Created by Floris van der Grinten on 30-06-14.
//
//

#import "UINavigationItem+CustomTitle.h"

@implementation UINavigationItem (CustomTitle)

- (void)createCustomTitle:(NSString *)title fontSize:(CGFloat)size
{
    UILabel *titleLabel = [UILabel new];
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Semibold" size:size], NSForegroundColorAttributeName : [UIColor whiteColor], NSKernAttributeName : @-0.5 };
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",title] attributes:attributes];
    [titleLabel sizeToFit];
    self.titleView = titleLabel;
}

@end
