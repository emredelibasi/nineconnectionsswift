//
//  UINavigationItem+CustomTitle.h
//  NineConnections
//
//  Created by Floris van der Grinten on 30-06-14.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (CustomTitle)

- (void)createCustomTitle:(NSString *)title fontSize:(CGFloat)size;

@end
