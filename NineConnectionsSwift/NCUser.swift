//
//  NCUser.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 3/17/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCUser: NSObject, NSCoding {
    
    let id: Int
    let username: String
    var fullName: String?
    var authorizationToken: NCAuthorizationToken?
    var company: NCCompany?
    var sharedItems: [NCUserShare] = []
    
    init(userId: Int, userName: String, fullName: String, authorizationToken: NCAuthorizationToken) {
        self.id = userId
        self.username = userName
        self.fullName = fullName
        self.authorizationToken = authorizationToken
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "userId")
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.fullName, forKey: "fullName")
        aCoder.encode(self.company, forKey: "company")
        aCoder.encode(self.authorizationToken, forKey: "token")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
    
        guard let username = aDecoder.decodeObject(forKey: "username") as? String,
            let fullName = aDecoder.decodeObject(forKey: "fullName") as? String,
            let company = aDecoder.decodeObject(forKey: "company") as? NCCompany,
            let authorizationToken = aDecoder.decodeObject(forKey: "token") as? NCAuthorizationToken else { return nil }
        
        let id = aDecoder.decodeInteger(forKey: "userId")

        self.init(userId: id, userName: username, fullName: fullName, authorizationToken: authorizationToken)
        self.company = company
    }
}
