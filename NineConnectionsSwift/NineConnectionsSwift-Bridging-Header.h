//
//  NineConnectionsSwift-Bridging-Header.h
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/7/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

#import "Constants.h"
#import "NCHeadline.h"
#import "NCFeed.h"
#import <MailCore/MailCore.h>
//#import "NCDataStore.h"
#import "NCSocialStat.h"
#import "NSString+CorrectArticleTitle.h"
#import "UIColor+CustomColors.h"
#import "UIView+GradientBackground.h"
#import "UIImage+ImageEffects.h"
#import "NCStatsTableViewCell.h"
#import "NCHeadlineContainerView.h"
#import "NCAlertBox.h"
#import "NCSeenCountView.h"
//#import "NCUnseenAmountView.h"
#import "UIView+RoundedCorners.h"
#import "UINavigationItem+CustomTitle.h"
#import "NSURL+EncodeURL.h"
#import "NSUserDefaults+AppDefaults.h"
#import "NCConnectButton.h"
#import "NCShareButton.h"
#import "NCTwitterIntegration.h"
#import "NCBufferIntegration.h"
#import <Smooch/Smooch.h>





