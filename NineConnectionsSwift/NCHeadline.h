//
//  Headline.h
//  NineConnections
//
//  Created by Floris van der Grinten on 14-05-14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NCFeed.h"

@interface NCHeadline : NSObject <NSCoding>

@property (nonatomic) NSString *title;
@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *articleText;
@property (nonatomic) NSURL *articleURL;
@property (nonatomic) UIImage *articleImage;
@property (nonatomic, assign) NSUInteger feedHash;
@property (nonatomic, strong) NSString *apiItemId;
@property (nonatomic, strong) NSString *twitterId;
@property (nonatomic, strong) NSString *linkedInId;
@property (nonatomic, strong) NSString *bufferLinkedInId;
@property (nonatomic, strong) NSString *bufferTwitterId;


- (instancetype)initWithTitle:(NSString *)title articleText:(NSString *)text feed:(NCFeed *)feed;

@end
