//
//  NCBufferIntegration.h
//  NineConnections
//
//  Created by Floris van der Grinten on 01-09-14.
//
//

#import <Foundation/Foundation.h>
#import <NXOAuth2Client/NXOAuth2Account.h>
#import <NXOAuth2Client/NXOAuth2AccountStore.h>
#import <NXOAuth2Client/NXOAuth2AccessToken.h>
#import <Analytics/Analytics.h>
#import "NSURL+EncodeURL.h"
#import "Constants.h"

typedef void(^FetchReturnBlock)(NSArray *objects, NSError *error);
typedef void(^PostReturnBlock)(NSString *messageId, NSError *error);

@interface NCBufferIntegration : NSObject

@property (nonatomic, strong) NXOAuth2Account *loggedInAccount;
@property (nonatomic, strong) NSDictionary *profiles;

- (instancetype)init;

- (void)fetchUserWithBlock:(FetchReturnBlock)block;

- (void)postMessage:(NSString *)msg andURL:(NSString *)url toNetwork:(SocialNetwork)network withBlock:(PostReturnBlock)block;
- (void)fetchActivityUpdates:(NSArray *)posts withBlock:(FetchReturnBlock)block;

@end
