//
//  NSURL+EncodeURL.h
//  NineConnections
//
//  Created by Floris van der Grinten on 21-08-14.
//
//

#import <Foundation/Foundation.h>

@interface NSURL (EncodeURL)

- (NSString *)encodedStringFromURL;

@end
