//
//  NCSeenCountView.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 12/7/15.
//  Copyright © 2015 Emre Delibasi. All rights reserved.
//

import Foundation
import UIKit

class NCSeenCountView: UIView {
    
    var count: Int
    let countLabel: UILabel
    var lowestCount: Int
    
    init(withArticleCount count:Int, existingTitle navItem:UINavigationItem) {
        let length:CGFloat = 18
        self.count = count
        let originalTitleView = navItem.titleView as! UILabel
        
        let title = UILabel()
        title.text = "  \(originalTitleView.text)"
        title.font = originalTitleView.font
        title.textColor = originalTitleView.textColor
        title.sizeToFit()
        
        self.countLabel = UILabel()
        self.countLabel.font = UIFont(name: title.font.fontName, size: 14.0)
        self.countLabel.textColor = UIColor.lightGradientColor()
        if self.count != 0 {
            self.countLabel.text = "\(self.count)"
        } else {
            self.countLabel.text = ""
        }
        self.countLabel.sizeToFit()
        self.countLabel.frame = CGRectOffset(self.countLabel.frame, CGRectGetMaxX(title.frame), -3)
    
        self.lowestCount = self.count
        
        super.init(frame:CGRectMake(0, 0, length, length))
        self.addSubview(title)
        self.addSubview(self.countLabel)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTheCount(count: Int) {
        self.count = count
        var countToDisplay = ""
        
        if (self.count <= self.lowestCount) {
            if (self.count != 0) {
                countToDisplay = "\(self.count)"
            }
            self.countLabel.text = countToDisplay
            self.lowestCount = self.count
        }
    }

}
