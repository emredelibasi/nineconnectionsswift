//
//  UIView+GradientBackground.m
//  NineConnections
//
//  Created by Floris van der Grinten on 18-07-14.
//
//

#import "UIView+GradientBackground.h"

@implementation UIView (GradientBackground)

- (void)gradientBackgroundFrom:(UIColor *)firstColor to:(UIColor *)secondColor
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.colors = @[ (id)firstColor.CGColor, (id)secondColor.CGColor ];
//    self.gradient.startPoint = CGPointMake(0, 0);
//    self.gradient.endPoint = CGPointMake(0, 1.0);
    gradient.frame = self.frame;
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
