//
//  NSString+CorrectArticleTitle.m
//  NineConnections
//
//  Created by Floris van der Grinten on 18-06-14.
//
//

#import "NSString+CorrectArticleTitle.h"

@implementation NSString (CorrectArticleTitle)

- (NSString *)correctArticleTitle
{
    NSData *stringData = [self dataUsingEncoding:NSUTF8StringEncoding];

    NSDictionary *options = @{
                                NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                              };
    NSAttributedString *decodedString = [[NSAttributedString alloc] initWithData:stringData
                                                                         options:options
                                                              documentAttributes:nil
                                                                           error:nil];
    
    
    NSString *titleWithSeperators = decodedString.string;
    //NSLog(@"%@", titleWithSeperators);
    //NSString *titleWithTwoSeperators = [[titleWithSeperators componentsSeparatedByString:@"|"] firstObject];
    NSString *titleWithSeperatorsAndCorrectDash = [titleWithSeperators stringByReplacingOccurrencesOfString:@" — " withString:@" - "];

    NSArray *potentialSeperators = @[ @"|", @" - ",  @" • ", @" : ", @" -- ", @" | " ];
   
    NSString *correctTitle = titleWithSeperatorsAndCorrectDash;
    for (NSString *seperator in potentialSeperators) {
        correctTitle = [correctTitle longestStringWithSeperator:seperator];
    }
    
    return correctTitle;
}

- (NSString *)longestStringWithSeperator:(NSString *)seperator
{
    NSString *longestString;
    NSInteger longestStringLength = 0;

    for (NSString *str in [self componentsSeparatedByString:seperator]) {
        if (longestString == nil || str.length > longestStringLength) {
            longestString = str;
            longestStringLength = longestString.length;
        }
    }
    return longestString;
}

@end
