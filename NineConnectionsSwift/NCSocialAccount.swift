//
//  NCSocialAccount.swift
//  NineConnectionsSwift
//
//  Created by Emre Delibasi on 6/27/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit

class NCSocialAccount: NSObject, NSCoding {
    
    let id: String
    let displayName: String
    var credentials: [AnyHashable: Any]?
    let ownerId: String
    let service: String
    
    init(id: String, displayName: String, credentials: [AnyHashable: Any], ownerId: String, service: String) {
        
        self.id = id
        self.displayName = displayName
        self.credentials = credentials
        self.ownerId = ownerId
        self.service = service
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.displayName, forKey: "displayName")
        aCoder.encode(self.credentials, forKey: "credentials")
        aCoder.encode(self.ownerId, forKey: "ownerId")
        aCoder.encode(self.service, forKey: "service")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let id = aDecoder.decodeObject(forKey: "id") as? String,
            let displayName = aDecoder.decodeObject(forKey: "displayName") as? String,
            let credentials = aDecoder.decodeObject(forKey: "credentials") as? [AnyHashable: Any],
            let ownerId = aDecoder.decodeObject(forKey: "ownerId") as? String,
            let service = aDecoder.decodeObject(forKey: "service") as? String
        else { return nil }
        
        self.init(id: id, displayName: displayName, credentials: credentials, ownerId: ownerId, service: service)

    }
}
