//
//  TodayViewController.swift
//  TodayWidget
//
//  Created by Emre Delibasi on 1/12/16.
//  Copyright © 2016 Emre Delibasi. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, NCWidgetProviding {
    
    typealias FetchReturnBlock = (objects: [AnyObject], error: NSError) -> Void
    var seenHeadlinesByFeed: [NSObject: AnyObject]?
    var feedsToDisplay: [NCFeed]?
    let ArchivedSeenHeadlines = "seen-headlines.archive"
    let ArchivedFeeds = "feeds.archive"
    
    enum FeedType: Int {
        case FeedTypeChannel
        case FeedTypeNewsroom
        case FeedTypeTwitterList
        case FeedTypeAito
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let archiveContents = NSKeyedUnarchiver.unarchiveObjectWithFile(TodayViewController.sharedURLPathOfArchivedFile(ArchivedFeeds).path!)
        var feeds = [NCFeed]()
        if let contents = archiveContents as? [NCFeed] {
            for feed in contents {
                if feed.feedType == FeedType.Channel {
                    feeds.append(feed)
                }
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func sharedURLPathOfArchivedFile(archive: String) -> NSURL {
        
        let sharedContainer = NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.com.kimengi.ios.NineConnectionsSwift")
        return sharedContainer!.URLByAppendingPathComponent(archive)
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
}
